
#Can't use code 0 since it is the null termination character
PRINT = 1
INIT = 2
INIT_CONFIG = 5
START = 6
MARK_WP = 9
UPDATE_WP = 10
PING = 11
ERROR = 12
HEIGHT = 13
GRID_SIZE = 14
RADIUS = 15
ANGLE = 16
EXIT = 17
EXIT_SUCCESS = 18
FOUND = 19
GRID_QUERY = 20
VIDEO = 21
VIDEO_REPLY = 22
BORDER = 23
NOFLY = 24
HOSTIP = 25
TIMER = 26
QUIT = 27
SETUP = 28
AUTOMATA = 29
HOTSWAP = 30
FIREIP = 31
FIRESTART = 32
CODE = 33
FIRE_ELEMS = 34
BORDER_DRAW = 35
BORDER_CLEAR = 36
VIDEO_AUX = 37
NODISC = 38

#Temporary codes
CAPTURE = 100
POSITION = 101
COVER_RADIUS = 102
SORT = 103
MOTION = 104
EST_POS_LIST_START = 106
EST_POS_LIST_ELEM = 107
EST_POS_LIST_END = 108

#PORTS
PORT_FIRE = 50002

#Maximum length for messages
MAX_LENGTH = 48 #max message size is 50 - 2 (codes and underscore)

CHAR_PV = '_'
CHAR_P = ';'
CHAR_FV = '*'
CHAR_F = '?'

def send_message(module,code,message):
    #severity 6 is MAV_SEVERITY_INFO
    module.master.mav.statustext_send(6,message_to_str(module,code,message[0:MAX_LENGTH]))
    if (len(message) > MAX_LENGTH):
        module.master.mav.statustext_send(6,message_to_str(module,PRINT,"LONG MESSAGE WARNING: Message real length " + str(len(message))))
        
def code(message):
    return ord(message[1])

def message(text):
    return text[2:]

def aux_message_to_str(code,message):
    return '_' + chr(code) + message

def message_to_str(module,code,message):
    #the underscore indicates a special message
    return get_module_begin_target(module) + chr(code) + message

def get_module_begin_target(module):
    if (module.identifier == 'PLANNER_VIEWER'):
        return CHAR_P
    elif (module.identifier == 'PLANNER'):
        return CHAR_PV
    elif (module.identifier == 'FIRESIM_VIEWER'):
        return CHAR_F
    elif (module.identifier == 'FIRESIM'):
        return CHAR_FV
    
def get_module_begin_self(module):
    if (module.identifier == 'PLANNER_VIEWER'):
        return CHAR_PV
    elif (module.identifier == 'PLANNER'):
        return CHAR_P
    elif (module.identifier == 'FIRESIM_VIEWER'):
        return CHAR_FV
    elif (module.identifier == 'FIRESIM'):
        return CHAR_F

def analyze(module,begin):
    if begin == get_module_begin_self(module):
        return True
    else:
        return False

