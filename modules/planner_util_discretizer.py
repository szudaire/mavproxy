import numpy as np
import math
from shapely.geometry.polygon import Polygon
from shapely.geometry import Point
from multiprocessing import Value
from ctypes import *
from os.path import expanduser
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm

class Discretizer():
    def __init__(self,sharedmem):
        self._sharedmem = sharedmem

        path = expanduser("~/.cache/mavproxy/lib/")
        self._poly_c = cdll.LoadLibrary(path + "planner_clib_poly.so")
        self._poly_c.test_elements.argtypes = (c_int, POINTER(c_double), POINTER(c_double), POINTER(c_bool), c_double, c_double, c_int, POINTER(c_double), POINTER(c_double))
        
        self._grid = []
        self._grid_angle = Value('d',0.0)

        self._grid_size = None
        self.regions = {}
        self.regions_pol = {}
        return

    def get_loc_from_tree(self,level,coord):
        tree_depth = self._sharedmem.tree_depth.value
        tree_b = self._sharedmem.tree_b.value

        start = 0
        for i in range(tree_depth,level,-1):
            start += (tree_b**i)**2

        index = coord[1]*(tree_b**level)+coord[0]
        index += start
        print("Going to index",index)

        return index
        

    def grid_from_tree(self,center,tree,dheight):
        self._sharedmem.set_scale()
        scale = self._sharedmem.get_scale()
        
        grid_size = dheight#/tree[1]
        grid_size = self._sharedmem.meters2latlon(grid_size)[1]
        
        width = (tree[1]**tree[0])*grid_size# + grid_size/4
        dirx = np.array([np.cos(self._grid_angle.value),np.sin(self._grid_angle.value)])
        diry = np.array([-np.sin(self._grid_angle.value),np.cos(self._grid_angle.value)])

        center = center*scale
	poly_utm = []
	poly_utm.append(center - dirx*width/2 - diry*width/2)
	poly_utm.append(center + dirx*width/2 - diry*width/2)
	poly_utm.append(center + dirx*width/2 + diry*width/2)
	poly_utm.append(center - dirx*width/2 + diry*width/2)
	poly_utm.append(poly_utm[0])
	poly_utm = np.array(poly_utm)
	poly = poly_utm/scale

        grid_total = []
        for i in range(tree[0]):
            grid = self.get_grid(poly,grid_size*(tree[1]**i))
            for elem in grid:
                grid_total.append(elem)
        grid_total.append(center/scale)
        return np.array(grid_total)

    def get_matrix_grid(self):
        locations = self._grid_scaled

        dirx = np.array([np.cos(self._grid_angle.value),np.sin(self._grid_angle.value)])
        diry = np.array([-np.sin(self._grid_angle.value),np.cos(self._grid_angle.value)])

        self._grid_size = self._sharedmem.meters2latlon(self._sharedmem.get_grid_param())[1]

        aux_grid = []
        pos0 = self._grid_scaled[0,:]
        for elem in self._grid_scaled:
            vec = elem - pos0
            x = int(np.round((vec[0]*dirx[0] + vec[1]*dirx[1])/self._grid_size))
            y = int(np.round((vec[0]*diry[0] + vec[1]*diry[1])/self._grid_size))
            aux_grid.append([x,y])
        aux_grid = np.array(aux_grid,np.int)
        
        minx = aux_grid[:,0].min()
        miny = aux_grid[:,1].min()
        maxx = aux_grid[:,0].max()
        maxy = aux_grid[:,1].max()

        aux_grid[:,0] -= minx
        aux_grid[:,1] -= miny

        self._matrix_grid = aux_grid
        return aux_grid

    def get_loc_from_matrix(self,pos):
        index = np.where((self._matrix_grid[:,0] == pos[0]) & (self._matrix_grid[:,1] == pos[1]))[0]
        return index[0]

    def add_grid(self,locations):
        self._grid = locations
        self._scale = self._sharedmem.get_scale()
        self._grid_scaled = self._grid*self._scale
    
    def get_position(self,location):
        return self._grid[location]
    
    def get_locations_from_area(self,polygon):
        potencial_locations = self._select_potencial_elements(polygon,self._grid)
        ind = 0
        array_loc = [[]]*len(potencial_locations)
        polygon_obj = Polygon(polygon)
        for loc in potencial_locations:
            loc_pos = self.get_position(loc)
            if(self._test_point(loc_pos,polygon_obj)):
                array_loc[ind] = loc
                ind += 1
        return np.array(array_loc[0:ind])
    
    def _select_potencial_elements(self,polygon,grid):
        min_x = np.min(polygon[:,0])
        min_y = np.min(polygon[:,1])
        max_x = np.max(polygon[:,0])
        max_y = np.max(polygon[:,1])

        return np.flatnonzero((grid[:,0] > min_x) & (grid[:,0] < max_x) & (grid[:,1] > min_y) & (grid[:,1] < max_y))

    def get_start_position(self):
        return self._start_position

    def discretize(self,polygon,grid_size,utm=False): #Polygon is the limits for the discrete region polygon
        # Detransform scale
        if not utm:
            scale = self._sharedmem.get_scale()
            polygon = polygon*scale

        self._grid_size = grid_size
        gsize = grid_size
        
        alpha = self.get_grid_angle()
        # Transform polygon to rotated axis
        rot_array = np.array([[np.cos(alpha),np.sin(alpha)],[-np.sin(alpha),np.cos(alpha)]])
        self._rot_array = rot_array
        polygon = np.dot(rot_array,polygon.T).T
        
        min_x = np.min(polygon[:,0])
        min_y = np.min(polygon[:,1])
        max_x = np.max(polygon[:,0])
        max_y = np.max(polygon[:,1])
        
        length_x = int(math.ceil((max_x - min_x)/gsize))
        length_y = int(math.ceil((max_y - min_y)/gsize))
        self._nx = length_x
        self._ny = length_y

        print ("Length X: " + str(length_x) + " Length Y: " + str(length_y))
        
        grid = [[]]*length_x*length_y

        for i in range(length_y):
            for j in range(length_x):
                point = [min_x+(j+0.5)*gsize,min_y+(i+0.5)*gsize]
                grid[i*length_x+j] = [point[0],point[1]]

        grid = np.array(grid)
        # Specify grid in original form
        grid[:,:] = np.dot(np.linalg.inv(rot_array),grid[:,:].T).T
        if not utm:
            grid = grid/scale
        else:
            for i in range(len(grid)):
                position = self._sharedmem.get_position()
		(ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
		(lat,lon) = llutm.UTMtoLL(grid[i,0],grid[i,1],ZoneNumber,isSouthernHemisphere=True)
                grid[i,:] = [lon,lat]
        
        self._discrete_region = grid

    def save_discretization_to_file(self,disc_file):
        file = open(disc_file,'w')

        for elem in self._discrete_region:
            file.write(str(elem[0])+','+str(elem[1])+'\n')
        
        file.close()
    
    def load_discretization_from_file(self,disc_file):
        file = open(disc_file,'r')
        disc_lines = file.readlines()
        file.close()

        discrete_region = [[]]*len(disc_lines)
        for i in range(len(disc_lines)):
            line = disc_lines[i]
            values = line.split(',')
            discrete_region[i] = [float(values[0]),float(values[1][:-1])]

        self._discrete_region = np.array(discrete_region,np.float)

    def get_grid(self,polygon,grid_size,discretize=True):
        if grid_size < 0:
            locations = []
            region_names = self.regions.keys()
            region_names.sort()
            for name in region_names:
                for i in range(len(self.regions[name])-1):
                    locations.append(self.regions[name][i])
                locations.append([0,0])
            return np.array(locations,np.float)
        
        if discretize:
            self.discretize(polygon,grid_size)
        
        scale = self._sharedmem.get_scale()
        self._scale = scale
        self._start_position = self._sharedmem.get_position()

        if not discretize:
            potential_elements = self._discrete_region[self._select_potencial_elements(polygon,self._discrete_region),:]
        else:
            potential_elements = self._discrete_region
        polygon_obj = Polygon(polygon)
        test_values = np.zeros(len(potential_elements),np.uint8)
        for i in range(len(potential_elements)):
            point = potential_elements[i,:]
            test_values[i] = self._test_point(point,polygon_obj)
        
        #Filter elements
        grid = potential_elements[np.flatnonzero(test_values == 1),:]
        self._grid = grid
        self._grid_scaled = self._grid*scale

        return grid
    
    def _test_point(self,point,polygon):
        return Point(point).within(polygon)

    def set_grid_angle(self,value):
        self._grid_angle.value = value

    def get_grid_angle(self):
        return self._grid_angle.value

    def get_capture_polygon(self,location,ground_width):
        scale = self._sharedmem.get_scale()
        pos = self.get_position(location)
        #Change scale
        pos = pos*scale
        gsize = self._sharedmem.meters2latlon(ground_width)[1]
        rot_array = self._rot_array
        #Rotate axis
        pos = np.dot(rot_array,pos.T).T

        posLL = pos + np.array([-gsize/2,-gsize/2])
        posLR = pos + np.array([-gsize/2,gsize/2])
        posUL = pos + np.array([gsize/2,-gsize/2])
        posUR = pos + np.array([gsize/2,gsize/2])

        polygon = np.array([posLL,posLR,posUR,posUL])
        polygon = np.dot(np.linalg.inv(rot_array),polygon.T).T/scale

        return polygon

    def _select_elements_from_trajectory(self,trajectory):
        elements = []
        traj = trajectory*self._scale
        if (self._grid_size == None):
            self._grid_size = self._sharedmem.meters2latlon(self._sharedmem.get_grid_param())
            
        dist = self._grid_size[1]/2

        delta = traj[1:,:] - traj[0:-1,:]
        mod = np.linalg.norm(delta,axis=1)
        indices = np.flatnonzero(mod > dist)
        traj_flat = traj.flatten()

        inserted = 0
        for ind in indices:
            line = np.arange(dist,mod[ind],dist)
            middle_points = delta[ind,:] * line.reshape(len(line),1)/mod[ind] + traj[ind]
            traj_flat = np.insert(traj_flat,2*(ind+inserted+1),middle_points.flatten())
            inserted += len(middle_points)

        traj = traj_flat.reshape(len(traj_flat)/2,2)
        
        grid_param = self._grid_size[1]
        angle = math.pi/2*0
        locx = self._grid_scaled[:,0].copy()
        locy = self._grid_scaled[:,1].copy()
        inside = np.zeros(len(locx),c_bool)
        testx = traj[:,0].copy()
        testy = traj[:,1].copy()
        self._poly_c.test_elements(c_int(len(locx)),locx.ctypes.data_as(POINTER(c_double)),locy.ctypes.data_as(POINTER(c_double)),inside.ctypes.data_as(POINTER(c_bool)),c_double(grid_param),c_double(angle),c_int(len(testx)),testx.ctypes.data_as(POINTER(c_double)),testy.ctypes.data_as(POINTER(c_double)))
        
        return np.where(inside > 0)[0]
    
    def get_adjacence_matrix(self):
        self._adj_matrix = []

        grid_size = self._grid_size
        for i in range(len(self._grid_scaled)):
            indexes = np.where(np.linalg.norm(self._grid_scaled[i,:] - self._grid_scaled,axis=1) <= 1.1*grid_size)
            indexes = np.delete(indexes[0],np.where(indexes[0] == i))
            self._adj_matrix.append(indexes)

        return self._adj_matrix

    def generate_model(self,start_loc,update=False,oldcontroller=None):
        adj_matrix = self.get_adjacence_matrix()

        model_string = ''
        model_string += self._environment_text(len(adj_matrix))
        model_string += self._properties_text()
        if (update == False):
            model_string += self._specs_text()
        else:
            model_string += self._update_specs_text()
            model_string += self._controller_text(oldcontroller)
        
        model_string += '\nGrid = ('+istr('go',start_loc)+' -> '+istr('arrived',start_loc)+' -> '+istr('Elem',start_loc)+'),\n'
        for i in range(len(adj_matrix)):
            adj_elem = adj_matrix[i]
            model_string += istr('Elem',i)+' = ('
            first = True
            for elem in adj_elem:
                if not first:
                    model_string += ' | '
                else:
                    first = False
                model_string += istr('go',elem)+' -> '+istr('arrived',elem)+' -> '+istr('Elem',elem)

            if i != len(adj_matrix) - 1:
                model_string += '),\n'
            else:
                model_string += ').\n'
        
        return model_string

    def get_closest_element(self,position):
        pos_scaled = position*self._scale
        distances = np.linalg.norm(pos_scaled - self._grid_scaled,axis=1)
        return distances.argmin()

    def get_matrix_loc(self,loc):
        return self._matrix_grid[loc]

    def add_region(self,polygon,label,grid_size):
        self.regions_pol[label] = polygon
        if grid_size > 0:
            elements = self.get_locations_from_area(polygon)
        else:
            elements = []
            for elem in polygon:
                elements.append(elem)
        self.regions[label] = np.array(elements)
        return elements
    
    def _environment_text(self,length_grid):
        text = '''
const N = '''+str(length_grid-1)+'''
set GridC = {go[0..N]}
set GridU = {arrived[0..N]}
set GridA = {GridC,GridU}

set UAV_C = {takeoff, land,battery_charged, set_high_height, set_low_height}
set UAV_U = {initial_config, takeoff_ended, landed}
set UAV_A = {UAV_C,UAV_U}

UAV = (initial_config -> ConfigDone),
ConfigDone = (	takeoff -> takeoff_ended -> TakeoffEnded),
TakeoffEnded = ( {GridC} -> ({GridU} -> TakeoffEnded) |
					land -> landed -> Landed),
Landed = (battery_charged -> UAV).

FlyHeight = (set_high_height -> HeightSetHigh | set_low_height -> HeightSetLow),
HeightSetHigh = (set_low_height -> HeightSetLow),
HeightSetLow = (set_high_height -> HeightSetHigh).

set Alphabet = {GridA,UAV_A}
set Controlables = {GridC,UAV_C}
||Environment = (UAV || Grid || FlyHeight).
'''
        return text

    def _properties_text(self):
        text = '''
////////////False and True
fluent True = <Alphabet,Alphabet\Alphabet> initially 1
fluent False = <Alphabet\Alphabet,Alphabet> initially 0
'''
        for label in self.regions.keys():
            elements = self.regions[label]
            arrived_concat = ''
            if (len(elements) == 0):
                continue
            for i in range(len(elements)):
                elem = elements[i]
                if i > 0:
                    arrived_concat += ','
                arrived_concat += istr("arrived",elem)
            text += "\nfluent F_Arrived_"+label+" = <{"+arrived_concat+"},GridU\{"+arrived_concat+"}>"
            text += "\nfluent F_ArrivedOnce_"+label+" = <{"+arrived_concat+"},GridU\GridU>"
            text += "\nassert Visit_"+label+" = (F_Arrived_"+label+")"
            text += "\nassert VisitOnce_"+label+" = (F_ArrivedOnce_"+label+")"
            text += "\nltl_property VisitNever_"+label+" = [](!F_Arrived_"+label+")"
            text += "\n"
        text += '''
ltl_property L_NeverLand = [](land -> False)
fluent F_HighHeight = <set_high_height,set_low_height>
fluent F_LowHeight = <set_low_height,set_high_height>
fluent F_InFlight = <takeoff,landed>
ltl_property L_FlyHigh = [](F_InFlight -> F_HighHeight)
ltl_property L_FlyLow = [](F_InFlight -> F_LowHeight)
'''
        return text

    def _specs_text(self):
        text = '''
controllerSpec Controller_Req = {
//        failure = {Failures}
        safety = {L_NeverLand,L_FlyHigh}
        assumption = {}
        liveness = {}
        controllable = {Controlables}
}

controller ||Controller = (Environment)~{Controller_Req}.
minimal ||TestController = (Controller || Environment).
'''
        return text

    def _update_specs_text(self):
        text = '''
//Mapping Environment
BEFORE_RECONF = (reconfigure -> AFTER_RECONF | Alphabet -> BEFORE_RECONF),
AFTER_RECONF = (Alphabet -> AFTER_RECONF).

||MappingEnvironment = (Environment || BEFORE_RECONF).

//Updating controller fluents
fluent InTransition = <stopOldSpec, startNewSpec> 
fluent StopOldSpec = <stopOldSpec, beginUpdate>
fluent StartNewSpec= <startNewSpec, beginUpdate>
fluent Reconfigure = <reconfigure, beginUpdate>

ltl_property NoTransition = [](!StopOldSpec || StartNewSpec)

controllerSpec OldSpecSafe = {
        safety = {L_NeverLand,L_FlyHigh}
        controllable = {Controlables}
}

controllerSpec NewSpecSafe = {
        safety = {L_NeverLand,L_FlyHigh}
        controllable = {Controlables}
}

updatingController UpdCont = {
    oldController = OldController,
    mapping = MappingEnvironment,
    safetyOldGoal = OldSpecSafe,
    safetyNewGoal = NewSpecSafe,
    livenessNewGoal = {}
    transition = NoTransition,
    nonblocking
}

minimal ||UPDATE_CONTROLLER = (UpdCont).
'''
        return text

    def _controller_text(self,controller):
        lines = controller.splitlines()
        text = "\n||OldController = (Controller).\n\n"
        text += "\n".join(lines[5:])
        text += "\n"
        return text
        
def istr(string,i):
    return string + '[' + str(i) + ']'


    
