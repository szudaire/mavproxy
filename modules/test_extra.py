import numpy as np
import planner_util_trajectory as tj
import time

N = 10
M = 9
grilla = [[]]*(N*M)
grilla_red = []

posx = 0
posy = 2
ind_pos = posx*N+posy

for i in range(M):
	for j in range(N):
		grilla[i*N + j] = [i,j]
		if (i*N+j not in [10,11,12,13,14,15,16,17,18,19,20,0,1]):
			grilla_red.append([i,j])
	
grilla = np.array(grilla,np.float_)
grilla_red = np.array(grilla_red,np.float_)

dir_inicial = np.array([0.0,1.0])
dir_final = np.array([0.0,1.0])
pos = np.array([posx,posy],np.float_)
radio = 0.99

distancia = []
distancia_pond = []
pond_list = []

tray = tj.Trajectory()
scale = np.array([1.0,1.0])
auxwp = True
overwp = False
check_ang = True
tray.set_parameters(pos,dir_inicial,radio,scale,auxwp,overwp,check_ang)
tray.set_grid(grilla_red)

t1 = time.time()
for elem in grilla_red:
	pos_final = np.array(elem)
	wp_list,dist,dir,pond,dist_pond = tray.calcular_trayectoria_paralela_direccion_ponderada(pos_final,dir_final)
        #if (elem[0] == 1.0 and elem[1] == 7.0):
                #wp_list,dist,dir = tray.calcular_trayectoria_paralela_direccion(pos_final,dir_final)
                #print(wp_list)
	distancia.append(dist)
	distancia_pond.append(dist_pond)
	pond_list.append(pond)
t2 = time.time()
print("Delta time: " + str(t2-t1))

plot_abs = np.zeros(N*M,np.float_) - 111
plot_dist = plot_abs.copy()
plot_dist_mod = plot_abs.copy()
for i in range(len(grilla_red)):
	ind = np.flatnonzero((grilla[:,0] == grilla_red[i,0]) & (grilla[:,1] == grilla_red[i,1]))
	if (len(ind) > 0):
		plot_abs[ind[0]] = pond_list[i]
		plot_dist[ind[0]] = distancia[i]
		plot_dist_mod[ind[0]] = distancia_pond[i]

#print(plot_res[:,0].reshape(M,N).T)
#print(plot_res[:,1].reshape(M,N).T)

np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)
print(plot_abs.reshape(M,N).T)
print('')
print(plot_dist.reshape(M,N).T)
print('')
print(plot_dist_mod.reshape(M,N).T)
