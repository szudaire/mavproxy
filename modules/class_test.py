from planner_module import Module
from multiprocessing import Value
import time

class Test(Module):
	def init(self):
		self.counter = Value('i',0)
		self.init_time = 0
		
		self._suscribed_packets = ["123","456"]
	
	def _accion(self):
		self.add_to_command_queue('accion')
	
	def accion(self):
		if (self.init_time == 0):
			self.init_time = time.time()
		if (self.counter.value < 10000):
			self.counter.value += 1
			return 'evento'
			#self._add_to_event_queue('evento')
		else:
			self._add_to_print_queue(str(time.time() - self.init_time))
			self._add_to_event_queue('exit')
	