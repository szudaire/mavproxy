#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

from pymavlink import mavutil
import multiprocessing as mp
import numpy as np
import time
from os.path import expanduser
import os
import sys
import importlib
import cv2

from MAVProxy.modules.lib.mp_module import MPModule
#from MAVProxy.modules.lib import mp_util
#from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.textconsole import SimpleConsole

from MAVProxy.modules import planner_automata
from MAVProxy.modules import planner_util_sharedmem
from MAVProxy.modules import planner_iterator
from MAVProxy.modules import planner_battery
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules import planner_videocamera
from MAVProxy.modules import planner_monitor1
from MAVProxy.modules import planner_monitor2
from MAVProxy.modules import planner_monitor3
from MAVProxy.modules import planner_server

class planner(MPModule):
    def __init__(self, mpstate):
        """Initialise module"""
        super(planner, self).__init__(mpstate, "planner", "")
    
        home = expanduser("~")
        self._code_path = home + '/.cache/mavproxy/code/'
        sys.path.insert(1, self._code_path)
        
        self.path = home + '/.cache/mavproxy/logs/'
        self._remove_file(self.path + "grid1.txt")
        self._remove_file(self.path + "pos_log.txt")
        self._remove_file(self.path + "found.txt")
        self._remove_file(self.path + "markwp.txt")
        self._remove_file(self.path + "nofly.txt")
        self._remove_file(self.path + "timer.txt")
        self._log_interval = 0.5
        self._log_time = time.time()

        self._process_command("set moddebug 3")
        self._process_command("set requireexit True")
        self._txt_console = SimpleConsole()

        self._queue_message = mp.Queue()
        self._queue_print = mp.Queue()
        self._queue_mavproxy_command = mp.Queue()
        self._modules_started = False

        self._quit_on_rtl = False
        #self._mission = "PATROL_AB"
        #self._mission = "PATROL_ABC"
        #self._mission = "COVER"
        #self._mission = "BORDER"
        #self._mission = "NEMO"
        #self._mission = "PERSON"
        #self._mission = "PERSON2"
        #self._mission = "ISLAND"
        
        self.controllables = ['reconfigure','stopOldSpec','startNewSpec']
        
        self._set_sharedmem()
        self._init_config_file()
        
        self._register_modules()
        
        self.prev = time.time()
        self._config_done = False
        self._count_ack = 0
        
        self._grid_sent = False
        self._cant_ack = 0
        self._begin = 0

        self._unloaded = False

        self._grid_num = 0
        self._video_started = False
        self._waiting_for_locations = False
        self._waiting_for_automata = False
        self._waiting_for_code = False
        self._waiting_for_fire = False
        self._server_started = False

        self.identifier = 'PLANNER'
        self._module_queue = mp.Queue()

        #Only works in python 3.*
        #mp.set_start_method('spawn')

    def _init_config_file(self):
        home = expanduser("~")
        config_file = home + '/.cache/mavproxy/config_file.py'
        
        file = open(config_file,'r')
        self._config_list = file.readlines()
        file.close()

        self._uav_type = 'plane'
        self._mark_wps = True
        self._auto_takeoff = True
        self._parrot = False
        self._sort = "dist"
        self._high_height = 0
        self._low_height = 0
        self._raspi_zero = False
        
        for line in self._config_list:
            if (len(line) == 0):
                continue
            if (line[0] == '#'):
                continue
            
            if (line[-1] == '\n'):
                line = line[:-1]
                
            values = line.split(' ')
            if (values[0] == 'automata'):
                self._automata_file = values[1]
            elif (values[0] == 'sort'):
                self._sort = values[1]
            elif (values[0] == 'mission'):
                self._mission = values[1]
                self._sharedmem.mission = self._mission
            elif (values[0] == 'uav_type'):
                self._uav_type = values[1]
            elif (values[0] == 'motion'):
                if (values[1] == 'multi'):
                    self._sharedmem._control_mode.value = 1
                else:
                    self._sharedmem._control_mode.value = 0
            elif (values[0] == 'height'):
                self._sharedmem.set_flight_height(float(values[1]))
            elif (values[0] == 'dheight'):
                self._sharedmem.set_dheight(float(values[1]))
            elif (values[0] == 'high_height'):
                self._high_height = float(values[1])
            elif (values[0] == 'low_height'):
                self._low_height = float(values[1])
            elif (values[0] == 'tree_b'):
                self._sharedmem.tree_b.value = int(values[1])
            elif (values[0] == 'tree_depth'):
                self._sharedmem.tree_depth.value = int(values[1])
            elif (values[0] == 'grid_param'):
                self._sharedmem.set_grid_param(float(values[1]))
            elif (values[0] == 'grid_angle'):
                self._sharedmem.get_discretizer().set_grid_angle(np.deg2rad(float(values[1])))
            elif (values[0] == 'mark_wps'):
                if (values[1] == 'True'):
                    self._mark_wps = True
                else:
                    self._mark_wps = False
            elif (values[0] == 'auto_takeoff'):
                if (values[1] == 'True'):
                    self._auto_takeoff = True
                else:
                    self._auto_takeoff = False
            elif (values[0] == 'parrot'):
                if (values[1] == 'True'):
                    self._parrot = True
                else:
                    self._parrot = False
            elif (values[0] == 'zero'):
                if (values[1] == 'True'):
                    self._raspi_zero = True
                else:
                    self._raspi_zero = False

    def _remove_file(self,file):
        if os.path.exists(file):
            os.remove(file)
    
    def _register_modules(self):
        self._module_list = []
        args = [self._sharedmem,self._queue_print,self._queue_message,self._queue_mavproxy_command]
        self._args = args

        self._video = planner_videocamera.VideoCamera(args)
        self._server = planner_server.Server(args)

        #self._monitor = planner_monitor1.Monitor(args)
        #self._monitor = planner_monitor2.Monitor(args)
        #self._monitor = planner_monitor3.Monitor(args)

        if (self._uav_type == 'plane'):
            from MAVProxy.modules import planner_plane
            self._uav = planner_plane.Plane(args)
        elif(self._uav_type == 'quad'):
            from MAVProxy.modules import planner_quad
            self._uav = planner_quad.Quad(args)

        self._sharedmem.set_uav(self._uav)
        
        self._uav._low_height = self._low_height
        self._uav._high_height = self._high_height
        self._uav._mark_wps = self._mark_wps
        self._uav._auto_takeoff = self._auto_takeoff
        self._uav._parrot = self._parrot
        self._uav._raspi_zero = self._raspi_zero
        self._iterator = planner_iterator.Iterator(args)
        self._iterator.set_sort(self._sort)
        
        self._module_list.append(self._iterator)
        self._module_list.append(self._uav)
        self._module_list.append(planner_battery.Battery(args))
        if (self._mission == "PERSON"):
            from MAVProxy.modules import planner_personsensor
            self._personsensor = planner_personsensor.PersonSensor(args)
            self._module_list.append(self._personsensor)
        elif (self._mission == "PERSON2"):
            from MAVProxy.modules import planner_personsensor2
            self._personsensor = planner_personsensor2.PersonSensor(args)
            self._module_list.append(self._personsensor)
        elif (self._mission == "NEMO"):
            from MAVProxy.modules import planner_nemosensor
            from MAVProxy.modules import planner_sroomsensor
            self._nemosensor = planner_nemosensor.NemoSensor(args)
            self._sroomsensor = planner_sroomsensor.SRoomSensor(args)
            self._module_list.append(self._nemosensor)
            self._module_list.append(self._sroomsensor)
            self._uav._log_in_flight = True
        elif (self._mission == "ISLAND"):
            from MAVProxy.modules import planner_islandsensor
            self._islandsensor = planner_islandsensor.IslandSensor(args)
            self._module_list.append(self._islandsensor)
        elif (self._mission == "BORDER"):
            from MAVProxy.modules import planner_bordersensor
            self._bordersensor = planner_bordersensor.BorderSensor(args)
            self._module_list.append(self._bordersensor)
        elif (self._mission == "PATROL_AB"):
            self._quit_on_rtl = True
            from MAVProxy.modules import planner_asensor
            from MAVProxy.modules import planner_bsensor
            self._asensor = planner_asensor.ASensor(args)
            self._bsensor = planner_bsensor.BSensor(args)
            self._module_list.append(self._asensor)
            self._module_list.append(self._bsensor)
        elif (self._mission == "PATROL_ABC"):
            self._quit_on_rtl = True
            from MAVProxy.modules import planner_asensor
            from MAVProxy.modules import planner_bsensor
            from MAVProxy.modules import planner_csensor
            self._asensor = planner_asensor.ASensor(args)
            self._bsensor = planner_bsensor.BSensor(args)
            self._csensor = planner_csensor.CSensor(args)
            self._module_list.append(self._asensor)
            self._module_list.append(self._bsensor)
            self._module_list.append(self._csensor)
        elif (self._mission == "COVER"):
            self._quit_on_rtl = True
            from MAVProxy.modules import planner_dsensor
            self._dsensor = planner_dsensor.CSensor(args)
            self._module_list.append(self._dsensor)
            self._uav._log_in_flight = True
        elif (self._mission == "COVERALL"):
            from MAVProxy.modules import planner_camerasensor
            self._module_list.append(planner_camerasensor.Camera(args))
            self._uav._log_in_flight = True
        elif (self._mission == "FIRE"):
            from MAVProxy.modules import planner_firesensor
            self._firesensor = planner_firesensor.FireSensor(args)
            self._module_list.append(self._firesensor)
            self._uav._log_in_flight = True
        elif (self._mission == "MONITOR1"):
            from MAVProxy.modules import planner_camerasensor
            self._module_list.append(planner_camerasensor.Camera(args))
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper
            self._monitor = planner_monitor_wrapper.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)
        elif (self._mission == "MONITOR2"):
            from MAVProxy.modules import planner_camerasensor
            self._module_list.append(planner_camerasensor.Camera(args))
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper2
            self._monitor = planner_monitor_wrapper2.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)
        elif (self._mission == "MONITOR3"):
            from MAVProxy.modules import planner_camerasensor
            self._module_list.append(planner_camerasensor.Camera(args))
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper3
            self._monitor = planner_monitor_wrapper3.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)
        elif (self._mission == "MONITOR4"):
            from MAVProxy.modules import planner_camerasensor2
            self._module_list.append(planner_camerasensor2.Camera(args))
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper4
            self._monitor = planner_monitor_wrapper4.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)
        elif (self._mission == "MONITOR5"):
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper5
            self._monitor = planner_monitor_wrapper5.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)
        elif (self._mission == "MONITOR_EXAMPLE"):
            self._uav._log_in_flight = True
            from MAVProxy.modules import planner_monitor_wrapper_example
            self._monitor = planner_monitor_wrapper_example.Monitor(args)
            self._sharedmem.set_monitor(self._monitor)
            self._module_list.append(self._monitor)

        import copy
        module_list = copy.copy(self._module_list)
        module_list.append(self)
        self._automata = planner_automata.Automata(self._monitor.event_queue,module_list,self._queue_print,self._queue_message)
        
        self._packet_map = {}
        for module in self._module_list:
            #Register automata to each module
            module.set_event_queue(self._automata.get_event_queue())
            
            #Packet map construction
            packets = module.get_suscription_packets()
            for packet in packets:
                if (packet not in self._packet_map):
                    self._packet_map[packet] = [module]
                else:
                    self._packet_map[packet].append(module)

    def _add_to_message_queue(self,code,text):
	self._queue_message.put(mesg.aux_message_to_str(code,text))

    def _check_add_module(self,modulename):
        if self._modules_started: #can't load it directly into automata, wait for reconfigure
            self._module_queue.put(modulename)
            self._add_to_message_queue(mesg.PRINT, "Waiting for reconfigure to load code")
        else:
            self._add_module(modulename)

    def _add_module(self,modulename):
        modulename = modulename[:-3]
        modulelib = importlib.import_module(modulename)
        values = modulename.split('_')
        module = getattr(modulelib,values[1])(self._args)
        module.set_event_queue(self._automata.get_event_queue())
        module.start_process()
        self._module_list.append(module)
        self._automata.add_module(module)

        if (values[1] == "SafeSensor"):
            self._uav.set_nofly_regions(module.nofly_reg)
    
    def reconfigure(self):
        self._add_to_message_queue(mesg.PRINT, "Reconfiguring enrivonment")
        while self._module_queue.qsize() > 0:
            self._add_module(self._module_queue.get())
        self._add_to_message_queue(mesg.PRINT, "Reconfigure ended")

    def stopOldSpec(self):
        self._add_to_message_queue(mesg.PRINT, "Stopped Old Spec")

    def startNewSpec(self):
        self._add_to_message_queue(mesg.PRINT, "Started New Spec")

    def _start(self):
        #Set scale based on position
        self._sharedmem.set_scale()
        self._receive_locations()

    def _start_modules(self):
        #Start modules and automata
        #if (self._uav_type == 'plane'):
            #self._monitor.set_params(self.get_mav_param("NAVL1_DAMPING"),self.get_mav_param("NAVL1_PERIOD"))
        for module in self._module_list:
            module.start_process()

        ## Commented to not load automaticly a default automata
        #if not self._automata.has_automata():
            #self._automata.load_automata_from_file(self._automata_file)
        self._automata.start()
        self._modules_started = True

    def _receive_fire(self):
        self._server.start_process()
        self._server_started = True
        self._server.receive_fire()
        self._waiting_for_fire = True
        
    def _receive_code(self,filename):
        self._server.start_process()
        self._server_started = True
        code = self._server.receive_code()
        self._waiting_for_code = True
        self._code_filename = filename
        
    def _receive_locations(self):
        self._server.start_process()
        self._server_started = True
        locations = self._server.receive_locations()
        self._waiting_for_locations = True
        
    def _receive_automata(self,update=False):
        self._server.start_process()
        self._server_started = True
        locations = self._server.receive_automata()
        self._waiting_for_automata = True
        self._update_automata = update

    def _retreive_automata(self):
        automata_data = self._server.retreive_automata()
        if (len(automata_data) == 0):
            return False

        if (self._update_automata == False):
            if (self._modules_started):
                mesg.send_message(self,mesg.PRINT,"FAIL: Can't load Automata -> already started")
            else:
                self._automata.load_automata(automata_data)
                mesg.send_message(self,mesg.PRINT,"SUCCESS: Automata loaded")
        else:
            mesg.send_message(self,mesg.PRINT,"SUCCESS: Processing update Automata...")
            self._automata.load_automata(automata_data,update=True)
        return True
    
    def _retreive_code(self):
        code = self._server.retreive_code()
        if (len(code) == 0):
            return False

        file_obj = file(self._code_path + self._code_filename,'w')
        file_obj.write(code)
        file_obj.close()

        mesg.send_message(self,mesg.PRINT,"SUCCESS: "+self._code_filename+" Code Received")
        self._check_add_module(self._code_filename)
        return True
        
    def _retreive_fire(self):
        locations = self._server.retreive_fire()
        if (len(locations) == 0):
            return False

        self._firesensor.set_fire(locations)
        mesg.send_message(self,mesg.PRINT,"SUCCESS: Fire set")
        return True
        #self._iterator.add_locations(locations)
        
    def _retreive_locations(self):
        finished,locations = self._server.retreive_locations()
        if (not finished):
            return False

        if (self._mission == "MONITOR1" or self._mission == "MONITOR2"):
            if self._sharedmem.get_grid_param() < 0:
                self._monitor.set_poly(locations)
                locations = []

        if (self._mission == "MONITOR_EXAMPLE"):
            locs = []
            tmp_loc = []
            for elem in locations:
                if elem[0] != 0 and elem[1] != 0:
                    tmp_loc.append(elem)
                else:
                    tmp_loc = np.array(tmp_loc)
                    locs.append([np.average(tmp_loc[:,0]),np.average(tmp_loc[:,1])])
                    tmp_loc = []

            locations = np.array(locs)
                
        if (self._mission == "MONITOR4"):
            if self._sharedmem.get_grid_param() < 0:
                tree = [self._sharedmem.tree_depth.value,self._sharedmem.tree_b.value]
                dheight = self._sharedmem.get_dheight()
                locations = self._sharedmem.get_discretizer().grid_from_tree(locations[0],tree,dheight)
                #for loc in [locations[0],locations[(tree[1]**tree[0])**2-1]]:
                    #mesg.send_message(self,mesg.FOUND,str(loc[1]) + ' ' + str(loc[0]))
                self._monitor.set_tree(tree)
                mesg.send_message(self,mesg.PRINT,"SUCCESS: Tree of "+str(len(locations))+" nodes")

        if len(locations) == 0:
            return True
        
        self._sharedmem.get_discretizer().add_grid(locations)
        self._add_to_log("grid1.txt","header")
        for i in range(len(locations)):
            self._add_to_log("grid1.txt",str(locations[i,1]) + "\t" + str(locations[i,0]))
        
        self._iterator.add_locations(locations)
        
        if (self._mission == "PERSON" or self._mission == "PERSON2"):
            self._personsensor.add_locations(locations)
        elif (self._mission == "NEMO"):
            self._nemosensor.add_locations(locations)
            self._sroomsensor.add_locations(locations)
        elif (self._mission == "ISLAND"):
            self._islandsensor.add_locations(locations)
        elif (self._mission == "BORDER"):
            self._bordersensor.add_locations(locations)
        elif (self._mission == "MONITOR3" or self._mission == "MONITOR5"):
            matrix = self._sharedmem.get_discretizer().get_matrix_grid()
            self._monitor.set_matrix_grid(matrix)
        return True

    def _set_sharedmem(self):
        self._sharedmem = planner_util_sharedmem.SharedMem()

    def idle_task(self):
        while(self._queue_mavproxy_command.qsize() > 0):
            self._process_command(self._queue_mavproxy_command.get())
        
        while(self._queue_print.qsize() > 0):
            self._print_ln(self._queue_print.get())
            
        while(self._queue_message.qsize() > 0):
            message = self._queue_message.get()
            code = mesg.code(message)
            text = mesg.message(message)
            if (self._sharedmem.get_logging()):
                if (code == mesg.FOUND):
                    self._add_to_log("found.txt",text)
                elif (code == mesg.MARK_WP):
                    self._add_to_log("markwp.txt",text)
                elif (code == mesg.NOFLY):
                    self._add_to_log("nofly.txt",text)
                
            if (code == mesg.TIMER):
                self._add_to_log("timer.txt",text)
            mesg.send_message(self,code,text)
        
        if (self._config_done == False):
            self._config_done = self._uav.init_config()
            if (self._config_done == True):
                if (self._mission == "FIRE"):
                    self._firesensor.setup_firesim(self.mpstate.module("firesim"))
                mesg.send_message(self,mesg.SETUP,"Initial configurations done")
                if self._sharedmem.get_grid_param() < 0:
                    mesg.send_message(self,mesg.NODISC,"")

        if (self._sharedmem.get_logging() and time.time() > self._log_time + self._log_interval):
            self._log_time = time.time()
            
            altitude = self._sharedmem.get_relative_altitude()
            position = self._sharedmem.get_position()
            battery_per,battery_vol = self._sharedmem.get_battery()
            dir = self._sharedmem.get_direction()
            angle = np.arctan2(dir[1],dir[0])
            self._add_to_log("pos_log.txt",str(self._log_time - self._init_time) + "," + str(position[0]) + "," + str(position[1]) + "," + str(angle) + "," + str(altitude) + "," + str(battery_per) + "," + str(battery_vol))

        if (self._quit_on_rtl == True and self.mpstate.status.flightmode == 'RTL'):
            self._quit_on_rtl = False
            mesg.send_message(self,mesg.QUIT,"")

        if (self._waiting_for_locations):
            if (self._retreive_locations()):
                self._waiting_for_locations = False
                mesg.send_message(self,mesg.INIT_CONFIG,"Waiting for INIT command")

        if (self._waiting_for_automata):
            if (self._retreive_automata()):
                self._waiting_for_automata = False
                
        if (self._waiting_for_code):
            if (self._retreive_code()):
                self._waiting_for_code = False
                
        if (self._waiting_for_fire):
            if (self._retreive_fire()):
                self._waiting_for_fire = False

    def _add_to_log(self,log_file,text):
        log = open(self.path + log_file,"a+")
        log.write(text + "\n")
        log.close()
    
    def mavlink_packet(self, m):
        mtype = m.get_type()
        if (mtype == "STATUSTEXT"):
            if (mesg.analyze(self,m.text[0])):
                code = mesg.code(m.text)
                if (code == mesg.START):
                    self._start()
                if (code == mesg.INIT):
                    if (self._automata.has_automata()):
                        self._start_modules()
                        self._automata.add_to_event_queue('initial_config')
                        mesg.send_message(self,mesg.INIT,"SUCCESS: Started the loaded plan")
                        self._init_time = time.time()
                    else:
                        mesg.send_message(self,mesg.INIT,"FAIL: Missing a plan")
                elif (code == mesg.PING):
                    mesg.send_message(self,mesg.PING,"")
                elif (code == mesg.HEIGHT):
                    self._sharedmem.set_flight_height(float(mesg.message(m.text)))
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Flight height set to " + str("%.2f" % self._sharedmem.get_flight_height()) + " meters")
                elif (code == mesg.GRID_SIZE):
                    self._sharedmem.set_grid_param(float(mesg.message(m.text)))
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Grid size set to " + str("%.2f" % self._sharedmem.get_grid_param()) + " meters")
                elif (code == mesg.RADIUS):
                    self._sharedmem.set_turn_radius(float(mesg.message(m.text)))
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Radius set to " + str("%.2f" % float(mesg.message(m.text))) + " meters")
                elif (code == mesg.ANGLE):
                    angle = np.deg2rad(float(mesg.message(m.text)))
                    wind_dir = np.array([np.cos(angle),np.sin(angle)])
                    self._sharedmem.set_wind_dir(wind_dir)
                    self._sharedmem.get_discretizer().set_grid_angle(angle)
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Angle set to " + str("%.2f" % float(mesg.message(m.text))) + " degrees")
                elif (code == mesg.EXIT):
                    self._process_command("exit")
                    self.unload()
                elif (code == mesg.COVER_RADIUS):
                    rad = float(mesg.message(m.text))
                    self._dsensor._radius = rad
                    self._iterator.cover_radius = rad
                    self._iterator.list = self._dsensor._list
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Cover Radius set to " + str("%.5f" % rad))
                elif (code == mesg.SORT):
                    self._iterator.set_sort(mesg.message(m.text))
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Sorting set to " + mesg.message(m.text))
                elif (code == mesg.GRID_QUERY):
                    grid_size = self._sharedmem.get_grid_param()
                    grid_angle = self._sharedmem.get_discretizer().get_grid_angle()
                    mesg.send_message(self,mesg.GRID_QUERY,str(grid_size)+','+str(grid_angle))
                elif (code == mesg.FIRE_ELEMS):
                    self._receive_fire()
                elif (code == mesg.CODE):
                    self._receive_code(mesg.message(m.text))
                elif (code == mesg.AUTOMATA):
                    autotype = mesg.message(m.text)
                    if (autotype == "load"):
                        self._receive_automata()
                    if (autotype == "update"):
                        self._receive_automata(update=True)
                elif (code == mesg.MOTION):
                    self._sharedmem._control_mode.value = int(mesg.message(m.text))
                    mesg.send_message(self,mesg.PRINT,"SUCCESS: Motion planner set to " + mesg.message(m.text))
                elif (code == mesg.HOTSWAP):
                    self._automata.add_to_event_queue('beginUpdate')
                    mesg.send_message(self,mesg.PRINT,"Received HOTSWAP command")
                elif (code == mesg.VIDEO):
                    if (not self._video_started):
                        self._video.start_process()
                        self._video_started = True
                        mesg.send_message(self,mesg.VIDEO_REPLY,"Success")
                    else:
                        mesg.send_message(self,mesg.VIDEO_REPLY,"Fail")
                    
        elif (mtype == "MISSION_ACK" and m.type == 0):
            if(self._grid_sent == True):
                self._cant_ack += 1
                if (self._cant_ack == 2):
                    mesg.send_message(self,mesg.LOC_LIST,"")
                    self._grid_sent = False
                    self._cant_ack = 0
                    
        if (mtype in self._packet_map):
            for module in self._packet_map[mtype]:
                module.mavlink_packet(m)
    
    def _process_command(self,command):
        self.mpstate.functions.process_stdin(command, immediate=True)
    
    def _print_ln(self,print_str):
        self._txt_console.writeln(print_str)
        
    def unload(self):
        if (self._unloaded):
            return
        
        if (self._modules_started == True):
            self._automata.add_to_event_queue('exit')
            self._automata.join()
            
            for module in self._module_list:
                module.exit_process()
            
            for module in self._module_list:
                self._print_ln('Joining ' + str(module))
                module.join_process()

        if (self._video_started):
            self._video.exit_process()
            self._video.join()
            
        if (self._server_started):
            self._server.exit_process()
            self._server.join()

        mesg.send_message(self,mesg.EXIT_SUCCESS,"")
        self._unloaded = True
        
        #os.system("killall -9 mavproxy.py xterm")
    
def init(mpstate):
    '''initialise module'''
    return planner(mpstate)
