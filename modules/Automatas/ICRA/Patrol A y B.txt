Process:
	Controller
States:
	40
Transitions:
	Controller = Q0,
	Q0	= (initial_config -> Q20),
	Q1	= (patrolB -> Q32),
	Q2	= (remove_next -> Q32),
	Q3	= (no_A_next -> Q7
		  |yes_A_next -> Q16),
	Q4	= (remove_next -> Q17),
	Q5	= (takeoff_ended -> Q21),
	Q6	= (reset_iterator -> Q21),
	Q7	= (remove_next -> Q31),
	Q8	= (has_A_current -> Q24),
	Q9	= (patrolA -> Q33
		  |remove_next -> Q39),
	Q10	= (remove_next -> Q19),
	Q11	= (reset_iterator -> Q25),
	Q12	= (no_B_next -> Q15
		  |yes_B_next -> Q26),
	Q13	= (has_B_current -> Q34),
	Q14	= (sort_locations -> Q38),
	Q15	= (remove_next -> Q18),
	Q16	= (go_next -> Q23),
	Q17	= (has_next -> Q27),
	Q18	= (has_next -> Q36),
	Q19	= (has_next -> Q36),
	Q20	= (takeoff -> Q28),
	Q21	= (has_next -> Q27),
	Q22	= (has_A_next -> Q3),
	Q23	= (sort_locations -> Q30),
	Q24	= (no_A_current -> Q4
		  |yes_A_current -> Q29),
	Q25	= (has_next -> Q36),
	Q26	= (go_next -> Q14),
	Q27	= (no_next -> Q6
		  |yes_next -> Q22),
	Q28	= (sort_locations -> Q5),
	Q29	= (remove_next -> Q1
		  |patrolB -> Q2),
	Q30	= (arrived -> Q8),
	Q31	= (has_next -> Q27),
	Q32	= (has_next -> Q36),
	Q33	= (remove_next -> Q37),
	Q34	= (yes_B_current -> Q9
		  |no_B_current -> Q10),
	Q35	= (has_B_next -> Q12),
	Q36	= (no_next -> Q11
		  |yes_next -> Q35),
	Q37	= (has_next -> Q27),
	Q38	= (arrived -> Q13),
	Q39	= (patrolA -> Q37).
