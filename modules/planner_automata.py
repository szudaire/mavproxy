from threading import Thread
#from multiprocessing import Process
from multiprocessing import Queue,Value
import numpy as np
from os.path import expanduser
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.planner_colors import pcolors
import thread
import time
import copy
import cv2

class Automata(Thread):
    def __init__(self,monitor_queue,module_list,queue_print,queue_message,verbose=True):
        super(Automata,self).__init__()

        self._monitor_queue = monitor_queue
        
        self._queue_print = queue_print
        self._queue_message = queue_message
        self._controllable_events = []

        self._map = {}
        self._map_controllable_to_module(module_list)
        self._queue_event = Queue()
        self._sync_queue = Queue()
        self._update_states_queue = Queue()
        self._states_queue = Queue()
        self._sync_state = Value('i',-1)
        self._sync_update_state = Value('i',-1)
        self._index = 0
        self._not_exit = 1
        self._set_automata = False
        self._sync_flag = False
        self._verbose = verbose

    def has_automata(self):
        return self._set_automata

    def add_module(self,module):
        self._map_controllable_to_module([module])

        if self.has_automata():
            for state in self._states:
                for elem in state:
                    if elem[0] in self._controllable_events:
                        elem[1] = 'C'

        self._add_to_message_queue(mesg.PRINT,"Loaded " + str(module.__class__.__name__) + " successfully")
    
    def run(self):
        while (self._not_exit):
            #Stops when the only thing left to do is wait for the environment
            self._generate_controllables()
            if (not self._not_exit):
                break
            
            #Wait for the environment
            event = self._queue_event.get()
            self._process_event(event)
    
    def _exit_routine(self):
        while (self._queue_event.qsize() > 0):
            self._queue_event.get()
        while (self._states_queue.qsize() > 0):
            self._states_queue.get()
    
    def get_event_queue(self):
        return self._queue_event
   
    def add_to_event_queue(self,event):
        self._queue_event.put(event)
        
    def _map_controllable_to_module(self,module_list):
        for module in module_list:
            for controllable in module.controllables:
                self._controllable_events.append(controllable)
                self._map[controllable] = getattr(module,controllable)
    
    def _generate_controllables(self):
        controllable_found = 1
        states = self._states
        while (controllable_found):
            while(self._queue_event.qsize() > 0):
                event = self._queue_event.get()
                self._process_event(event)
            
            #Search for controllables
            controllable_found = 0
            for elem in states[self._index]:
                if (elem[1] == 'C'):
                    controllable_found = 1
                    self._process_event(elem[0],controllable=True,element=elem)
                    break
        return

    def _error_routine(self,event):
        self._add_to_print_queue("EVENT " + event + " NOT FOUND")
        self._add_to_message_queue(mesg.ERROR,"")
        self._map['rtl']()
        self._not_exit = 0
        self._exit_routine()
    
    def _add_to_print_queue(self,event):
        self._queue_print.put(pcolors.color(event,'yellow'))
        
    def _add_to_message_queue(self,code,text):
        self._queue_message.put(mesg.aux_message_to_str(code,text))
    
    def _process_event(self,event,controllable=False,element=None):
        self._monitor_queue.put(event)
        if (self._verbose):
            self._add_to_print_queue(event)
        if (event == 'exit'):
            self._not_exit = 0
            self._exit_routine()
            return
        elif (event == 'sync'):
            self._sync_flag = True
            self._sync_state.value = self._index
            return
        elif (event == 'sync_end'):
            self._sync_state.value = -1
            self._sync_flag = False
            oldindex = self._index
            self._index = self._sync_update_state.value
            self._sync_update_state.value = -1
            self._states = self._update_states_queue.get()
            self._states_queue.put(self._states)

            self._add_to_message_queue(mesg.PRINT,"Old controller state: " + str(oldindex))
            self._add_to_message_queue(mesg.PRINT,"Update controller state: " + str(self._index))
            while self._sync_queue.qsize() > 0:
                self._process_event(self._sync_queue.get())

            self._add_to_message_queue(mesg.PRINT,"SUCCESS: Controller succesfully swapped")
            self._add_to_message_queue(mesg.PRINT,"Waiting for HOTSWAP command")
            self._add_to_message_queue(mesg.HOTSWAP,"ready")
            return
                
        if (self._sync_flag): #Push events as they happen in the sync queue to later sync
            self._sync_queue.put(event)

        if not controllable:
            for elem in self._states[self._index]:
                if(elem[0] == event):
                    self._index = int(elem[2])
                    return True #Accepted
        else:
            elem = element
            map_obj = self._map
            self._index = int(elem[2])
            #Call controllable
            try:
                values = elem[0].split('[')
                if (len(values) == 1):
                    event = map_obj[elem[0]]()
                elif (len(values) == 2):
                    event = map_obj[values[0]]((values[1])[:-1])
                else:
                    self._add_to_message_queue(mesg.PRINT, elem[0] + " too many arguments")
            except KeyError:
                self._add_to_message_queue(mesg.PRINT, elem[0] + " action not callable")
                return False
            
            if (event != None):
                self._process_event(event)
            return True
        
        self._error_routine(event)
        return False #Not Accepted

    def load_automata_from_file(self,automata_file):
        home = expanduser("~")
        filename = home + '/.cache/mavproxy/automatas/' + automata_file
        
        file = open(filename,'r')
        automata_data = file.read()
        file.close()

        self.load_automata(automata_data)
    
    def load_automata(self,automata_data,update=False):
        lines = automata_data.splitlines()
        states_size = int(lines[3].strip())
        states = [[]]*states_size
        
        analysing_state = False
        num_state = 0
        for i in range(6,len(lines)):
            line = lines[i].strip()
            
            if(analysing_state == False):
                if (line[0] == 'Q'):
                    analysing_state = True
                    aux_vec = []
            if(analysing_state == True):
                    aux_str = ''
                    many_events = False
                    action_set = False
                    for char in line:
                        if (char == '='):
                            num_state = int(aux_str[1:].strip())
                            continue
                        
                        if (char in ['(','{','|']):
                            aux_str = ''
                            continue
                        
                        if (char == '}'):
                            many_events = True
                            action = aux_str
                            action_set = True
                            continue
                        
                        if (many_events == False and char == '-'):
                            action = aux_str
                            action_set = True
                            continue
                        
                        if (action_set == True):
                            if (char == 'Q'):
                                aux_str = ''
                                continue
                        
                        if (char == ')'):
                            num_next_state = int(aux_str.strip())
                            analysing_state = False
                            break
                    
                        aux_str = aux_str + char
                    
                    if(analysing_state == True):
                        num_next_state = int(aux_str.strip())
                        
                    if (many_events == True):
                        aux_lista = np.array([])
                        aux_str = ''
                        for j in range(len(action)):
                            if (action[j] == ','):
                                aux_lista = np.append(aux_lista,aux_str)
                                aux_str = ''
                                continue
                            aux_str += action[j]
                            if (j == len(action)-1):
                                aux_lista = np.append(aux_lista,aux_str)
                        action = aux_lista
                    else:
                        action = [action]
                    
                    for accion in action:
                        accion = accion.strip()
                        values = accion.split('[')
                        if (values[0] in self._controllable_events):
                            aux_var = 'C'
                        else:
                            aux_var = 'A'
                        aux_vec.append([accion,aux_var,num_next_state])
                    
                    if(analysing_state == False):
                        states[num_state] = aux_vec

        if not update:
            self._states = states
            self._set_automata = True
            self._states_queue.put(states)
            self._add_to_message_queue(mesg.AUTOMATA,"")
        else:
            update_states = states
            self._update_states_queue.put(update_states)
            self.add_to_event_queue('sync')
            oldstates = self._states_queue.get()
            self._add_to_message_queue(mesg.PRINT,"Started syncronization of automatas")
            thread.start_new_thread(sync_automatas,(self._sync_state,self._sync_queue,oldstates,update_states,self._sync_update_state,self._queue_event))
            
        '''
        self._states = np.array([
            [['accion','C',1]], #0
            [['evento','A',0]] #1
            
        ])
        '''
        return

def sync_automatas(sync_state,sync_queue,old_states,update_states,sync_update_state,event_queue):
    while (sync_state.value < 0):
        time.sleep(0.05)
    time.sleep(2) #test sleep

    curr_state = sync_state.value

    explored_states = [curr_state]
    propagate_states = [[[curr_state,'',curr_state]]]
    i = 0
    while 1:
        aux_list = []
        for elem in propagate_states[i]:
            ind = elem[0]
            found = False
            for j in range(len(old_states)):
                state = old_states[j]
                for event in state:
                    if (event[2] == ind): #Found an event that takes me to state "ind"
                        found = True
                        if (j in explored_states):
                            continue
                        else:
                            explored_states.append(j)
                            aux_list.append([j,event[0],ind])
            if not found:
                #This element is the initial state!
                #print "Found initial state: " + str(ind)
                traza = []
                index = elem[0]
                for j in range(len(propagate_states)-1,0,-1):
                    for elem in propagate_states[j]:
                        if (elem[0] == index):
                            traza.append(elem[1])
                            index = elem[2]

                automata = Automata([],[],[],verbose=False)
                automata._states = update_states

                for event in traza:
                    automata._process_event(event)
                
                #print traza
                while sync_queue.qsize() > 0:
                    automata._process_event(sync_queue.get())

                sync_update_state.value = automata._index
                event_queue.put('sync_end')
                return automata._index
            
        propagate_states.append(aux_list)
        i+=1

    return







    
