import planner_iterator
import planner_automata
import planner_util_sharedmem

sharedmem = planner_util_sharedmem.SharedMem()
iter = planner_iterator.Iterator([sharedmem,None,None,None])
iter.add_locations([[]]*1000000)
auto = planner_automata.Automata([iter],None,None)
iter.set_event_queue(auto.get_event_queue())

auto.run()
