from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.mavproxy_map import mp_tile
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import numpy as np
import time
from os.path import expanduser

class IslandSensor(Module):
	def init(self):
		home = expanduser("~")
		path = home + '/.cache/mavproxy/cache/'
		self.path = home + '/.cache/mavproxy/captures/'
		self._tile = mp_tile.MPTile(cache_path=path,debug=False)
		self._height = 300
		self._width = 300
		self._field_of_view = 150.0/100.0 #100 meters at 100 meters of altitude
		
		self._process_type = 1
		return
	
	def _started(self):
		cv.namedWindow('capture', cv.CV_WINDOW_AUTOSIZE)
		cv.namedWindow('process', cv.CV_WINDOW_AUTOSIZE)
		cv.startWindowThread()
                #This causes a memory leak, don't use namedwindow and replace it with waitKey(0)
                pass
	
	def _capture(self):
                current = self._sharedmem.get_current_location()
		image = self._image_list[current]

		cv.imshow('capture',image)

		self._process_image(image)
                #cv.imwrite(self.path + str(current) + ".png",image)
		return
		
	def _download_image(self,lat,lon,ground_width):
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
		while (self._tile.tiles_pending() > 0):
			time.sleep(0.05)
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)

                image = cv.cvtColor(image,cv.COLOR_RGB2BGR)
		
		#self.image_list.append(image)
		#self.descarga_num += 1
		#cv.imwrite(self.path + 'data_set/' + str(self.descarga_num) + ".png",image)
		
		#cv.imshow('capture',image)
		return image

        def add_locations(self,pos_locations):
                self._image_list = [[]]*len(pos_locations)
                self._add_to_command_queue('add_locations')
                
		self._location_list = Array('i',range(len(pos_locations)))
		self._array_end = Value('i',0)
                
        def _add_locations(self):
		ground_width = self._field_of_view*self._sharedmem.get_flight_height()
                
                for i in range(len(self._image_list)):
                        elem = self._sharedmem.get_discretizer().get_capture_polygon(i,ground_width)[1,:]
                        image = self._download_image(elem[1],elem[0],ground_width)
                        self._image_list[i] = image
                        #self._add_to_print_queue("Downloaded location " + str(i+1) + " of " + str(len(self._image_list)))

                self._add_to_print_queue("Finished Downloading")
                
	def _add_to_array_end(self):
                self._array_end.value += 1
                
	def _substract_to_array_end(self):
                self._array_end.value -= 1

        def _get_array_end(self):
                return self._array_end.value

        def _add_to_list(self,value):
                self._location_list[self._get_array_end()] = value
                self._add_to_array_end()

        def _get_list(self):
                return self._location_list[0:self._get_array_end()]

        def _remove_from_list(self,value):
                array = np.array(self._get_list())
                ind = np.flatnonzero(array==value)[0]
                aux = self._location_list[ind+1:self._get_array_end()]
                self._substract_to_array_end()
                self._location_list[ind:self._get_array_end()] = aux

        def _process_image(self,image):
                hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
                # define range of blue color in HSV
                lower_blue = np.array([90,100,30])
                upper_blue = np.array([110,255,255])
                mask = cv.inRange(hsv, lower_blue, upper_blue)
                
                blur = cv.blur(mask,(5,5))
		cv.imshow('process',mask)

                current = self._sharedmem.get_current_location()
                if (len(blur[blur<50]) > 3000):
                        ground_width = self._field_of_view*self._sharedmem.get_flight_height()
                        area = self._sharedmem.get_discretizer().get_capture_polygon(current,ground_width)
                        elements = self._sharedmem.get_discretizer().get_locations_from_area(area,0)
                        for elem in elements:
                                if (elem not in self._get_list()):
                                        self._add_to_list(elem)
                        loc_current_pos = self._sharedmem.get_discretizer().get_position(current)
                        self._add_to_message_queue(mesg.FOUND,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                        self._add_to_event_queue('yes_Island_current')
                else:
                        if (current in self._get_list()):
                                self._remove_from_list(current)
                        self._add_to_event_queue('no_Island_current')
                return

        def has_Island_current(self):
		self._add_to_command_queue('capture')
		return

	def has_Island_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (next_pos in self._get_list()):
                        return 'yes_Island_next'
                else:
                        return 'no_Island_next'


