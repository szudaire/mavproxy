mission MONITOR5

### DESCOMENTAR PARA SIMULAR CON QUADCOPTER
uav_type quad
sort lindist
height 10
grid_param 50
#grid_param -1
grid_angle 0
#dheight 50
#tree_b 2
#tree_depth 2

### DESCOMENTAR PARA SIMULAR CON AVION
#uav_type plane
#sort dist
#motion multi
#height 100
#height 20
#grid_param 50
#grid_param -1
#grid_angle 0

### Otros parametros
mark_wps True
auto_takeoff True
parrot False
zero False
