#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> 
#include <math.h>

__device__ double TOL = 0.000001;
__device__ double DELTA_ANG = M_PI/4; 
__device__ int CANT_MAX = 24; //Para el delta_ang = pi/4
__device__ double SENT_C1[4] = {1.0,1.0,-1.0,-1.0};
__device__ double SENT_C2[4] = {1.0,-1.0,1.0,-1.0};
__device__ double SCALE[2];
__device__ double POS_INICIAL[2];
__device__ double DIR_INICIAL[2];
__device__ double RADIO = 0.0;
__device__ bool AUXWP = false;
__device__ bool OVERWP = false;
__device__ bool CHECK_ANG = false;
__device__ double C1_H[2];
__device__ double C1_AH[2];
__device__ double OVERWP_DIST;
__device__ double SUMA_POND[2];
__device__ double POND_ACTUAL;

/*
int minIndex(double *array, int size)
{
	int min_ind = 0;
	for (int i=1; i<size; i++)
	{
		if (array[i] < array[min_ind])
		{
			min_ind = i;
		}
	}
	return min_ind;
}

int maxIndex(double *array, int size)
{
	int max_ind = 0;
	for (int i=1; i<size; i++)
	{
		if (array[i] > array[max_ind])
		{
			max_ind = i;
		}
	}
	return max_ind;
}
*/

__device__ void set_overwp_dist(double value)
{
	OVERWP_DIST = value;
}

__device__ void printArray(double *array, int n)
{
	printf("Array: [");
	for (int i=0; i<n; i++)
	{
		printf("%lf ",array[i]);
	}
	printf("]\n");
}

/*
void printArray2d(double **array, int m, int n)
{
	printf("Array2d: [\n");
	for (int i=0; i<m; i++)
	{
		printf("\t");
		printArray(array[i],n);
	}
	printf("]\n");
}
*/

__device__ double normArray(double *array, int size)
{
	double value = 0.0;
	for (int i=0; i<size; i++)
	{
		value += array[i]*array[i];
	}
	return sqrt(value);
}

__device__ double multDot(double *array1, double *array2, int size)
{
	double value = 0.0;
	for(int i=0; i<size; i++)
	{
		value += array1[i]*array2[i];
	}
	return value;
}

__device__ double multCross(double *array1, double *array2)
{
	//Assume vectors are 2d
	return (array1[0]*array2[1]) - (array1[1]*array2[0]);
}

__device__ void multElems(double *array1, double *array2, double *result, int size)
{	
	for (int i=0; i<size; i++)
	{
		result[i] = array1[i]*array2[i];
	}
}

__device__ void multScalar(double *array, double scalar, double *result, int size)
{
	for (int i=0; i<size; i++)
	{
		result[i] = scalar*array[i];
	}
}

__device__ void addElems(double *array1, double *array2, double *result, int size)
{
	for (int i=0; i<size; i++)
	{
		result[i] = array1[i] + array2[i];
	}
}

__device__ void substractElems(double *array1, double *array2, double *result, int size)
{
	for (int i=0; i<size; i++)
	{
		result[i] = array1[i] - array2[i];
	}
}

__device__ void arrayCopy(double *array, double *destination, int size)
{
	for (int i=0; i<size; i++)
	{
		destination[i] = array[i];
	}
}

__device__ void get_centros(double *pos, double *dir, double radio, double *c_h, double *c_ah)
{
	double dir_rad[2] = {dir[1],-dir[0]};
	
	double auxvec[2];
	multScalar(dir_rad,radio,auxvec,2);
	addElems(pos,auxvec,c_h,2);
	multScalar(dir_rad,-radio,auxvec,2);
	addElems(pos,auxvec,c_ah,2);
}

/*
void createArray2d(double ***array, int m, int n)
{
	*array = (double **) malloc (m * sizeof(double));
	for (int i=0; i<m; i++)
	{
		(*array)[i] = (double *) malloc(n * sizeof(double));
	}
}

void freeArray2d(double ***array, int m)
{
	for (int i=0; i<m; i++)
	{
		free((*array)[i]);
	}
	free(*array);
}
*/

__device__ void set_pond_params(double *suma_pond)
{
	arrayCopy(suma_pond,SUMA_POND,2);
	POND_ACTUAL = normArray(suma_pond,2);
}

__device__ void set_parameters(double *pos_inicial, double *dir_inicial, double radio, double *scale, bool auxwp, bool overwp, bool check_ang)
{
	multElems(pos_inicial,scale,POS_INICIAL,2);
	
	arrayCopy(scale,SCALE,2);
	arrayCopy(dir_inicial,DIR_INICIAL,2);
	RADIO = radio;
	AUXWP = auxwp;
	OVERWP = overwp;
	CHECK_ANG = check_ang;
	
	get_centros(POS_INICIAL,dir_inicial,radio,C1_H,C1_AH);
}

__device__ void fn_centros_params(double *c1, double *c2, double *centros_mod, double *centros_dif, bool *centros_flag)
{
	substractElems(c2,c1,centros_dif,2);
	*centros_mod = normArray(centros_dif,2);
	*centros_flag = (*centros_mod <= 2*RADIO || *centros_flag);
}

__device__ void fn_centros_select(double **c1_h, double **c1_ah, double **c2_h, double **c2_ah, double **c1, double **c2, int i)
{
	switch(i)
	{
		case 0:
			*c1 = *c1_h;
			*c2 = *c2_h;
			break;
		case 1:
			*c1 = *c1_h;
			*c2 = *c2_ah;
			break;
		case 2:
			*c1 = *c1_ah;
			*c2 = *c2_h;
			break;
		case 3:
			*c1 = *c1_ah;
			*c2 = *c2_ah;
			break;
	}
}

__device__ void fn_tita_select(double tita_caso1, double alpha, double beta, double *tita1, double *tita2, int j)
{
	switch(j)
	{
		case 0:
			*tita1 = tita_caso1;
			*tita2 = *tita1;
			break;
		case 1:
			*tita1 = tita_caso1 + M_PI;
			*tita2 = *tita1;
			break;
		case 2:
			*tita1 = -alpha + beta;
			*tita2 = *tita1 + M_PI;
			break;
		case 3:
			*tita1 = alpha + beta;
			*tita2 = *tita1 + M_PI;
			break;
			
	}	
}

__device__ void get_waypoint(double tita, double *centro, double *way)
{
	double auxvec[2] = {cos(tita),sin(tita)};
	multScalar(auxvec,RADIO,auxvec,2);
	addElems(centro,auxvec,way,2);
}

__device__ bool test_direction(double *vecway, double *vec1, double *vec2, int i, double *centro_dif)
{
	double sent1 = SENT_C1[i];
	double sent2 = SENT_C2[i];
	double result1,result2;
	if (normArray(vecway,2) < TOL)
	{
		result1 = -sent1*sent2;
		result2 = result1;
		if (normArray(centro_dif,2) < TOL)
		{
			result1 = -result1;
			result2 = -result2;
		}
	} else
	{
		result1 = multCross(vecway,vec1)*sent1;
		result2 = multCross(vecway,vec2)*sent2;
	}
	return (result1>0 && result2>0); 
}

__device__ double wrapAngle(double angle)
{
	return fmod(angle + 4.0*M_PI,2.0*M_PI);
}

__device__ void calculate_dist(double *c1, double *c2, int i, double tita1, double tita2, double *dir_final, double *vecway, double *dist_ang, double *dist_line)
{
	double sent1 = SENT_C1[i];
	double sent2 = SENT_C2[i];
	double tita_inicial = sent1*M_PI/2.0 + atan2(DIR_INICIAL[1],DIR_INICIAL[0]);
	double tita_final = sent2*M_PI/2.0 + atan2(dir_final[1],dir_final[0]);
	
	double delta_inicial = wrapAngle(sent1*(-tita1 + tita_inicial));
	double delta_final = wrapAngle(sent2*(-tita_final + tita2));
	
	*dist_ang = (delta_final + delta_inicial)*RADIO;
	*dist_line = normArray(vecway,2);
}

__device__ void get_aux_waypoints(double tita1, double tita2, double *c1, double *c2, int i, double *dir_final, double *way1, double *way2, double *wp_list, int *p_cant)
{
	int cant = *p_cant;
	double sent1 = SENT_C1[i];
	double sent2 = SENT_C2[i];
	double tita_inicial = sent1*M_PI/2.0 + atan2(DIR_INICIAL[1],DIR_INICIAL[0]);
	double tita_final = sent2*M_PI/2.0 + atan2(dir_final[1],dir_final[0]);
	
	double total_ang = wrapAngle(sent1*(-tita1 + tita_inicial));
	double ang = DELTA_ANG;
	
	double aux_tita, auxvec[2];
	while (ang < total_ang)
	{
		aux_tita = - sent1*ang + tita_inicial;
		get_waypoint(aux_tita,c1,auxvec);
		wp_list[2*cant] = auxvec[0]/SCALE[0];
		wp_list[2*cant + 1] = auxvec[1]/SCALE[1];
		cant++;
		ang += DELTA_ANG;
	}
	
	//way1
	wp_list[2*cant] = way1[0]/SCALE[0];
	wp_list[2*cant+1] = way1[1]/SCALE[1];
	cant++;
	
	//way2
	wp_list[2*cant] = way2[0]/SCALE[0];
	wp_list[2*cant+1] = way2[1]/SCALE[1];
	cant++;
	
	ang = DELTA_ANG;
	total_ang = wrapAngle(sent2*(-tita_final + tita2));
	while (ang < total_ang)
	{
		aux_tita = - sent2*ang + tita2;
		get_waypoint(aux_tita,c2,auxvec);
		wp_list[2*cant] = auxvec[0]/SCALE[0];
		wp_list[2*cant + 1] = auxvec[1]/SCALE[1];
		cant++;
		ang += DELTA_ANG;
	}
	
	*p_cant = cant;
}

__device__ double calc_trajectory(double *pos_inicial, double *c1_h, double *c1_ah, double *pos_final_orig, double *dir_final, bool *p_disp_center, double *wp_list, int *cant_wp_array)
{
	bool disp_center = *p_disp_center;
	
	double pos_final[2];
	multElems(pos_final_orig,SCALE,pos_final,2);
	
	double auxvec[2];
	if (OVERWP == true)
	{
		multScalar(dir_final,-OVERWP_DIST,auxvec,2);
		addElems(auxvec,pos_final,pos_final,2);
	}
	
	substractElems(pos_final,pos_inicial,auxvec,2);
	if (normArray(auxvec,2) < TOL)
	{
		multScalar(dir_final,-2.0*TOL,auxvec,2);
		addElems(pos_inicial,auxvec,pos_final,2);
	}
	
	double a_c2_h[2];
	double a_c2_ah[2];
	double *c2_h = a_c2_h;
	double *c2_ah = a_c2_ah;
	get_centros(pos_final,dir_final,RADIO,c2_h,c2_ah);
	
	bool centros_flag = false;
	double centros_mod,centros_mod_min;
	double centros_dif[2], centros_dif_min[2];
	double *c1, *c2;
	
	//Check if disp_center is required
	if (disp_center == true)
	{
		//Calculate centros_mod y centros_dif
		for (int i=0; i<4; i++)
		{
			fn_centros_select(&c1_h,&c1_ah,&c2_h,&c2_ah,&c1,&c2,i);
			fn_centros_params(c1,c2,&centros_mod,centros_dif,&centros_flag);
					
			//Min calculation
			if (i==0 || (centros_mod <= 2*RADIO && (centros_mod_min > 2*RADIO || centros_mod > centros_mod_min)))
			{
				arrayCopy(centros_dif,centros_dif_min,2);
				centros_mod_min = centros_mod;
			}
		}
		
		//Calculate dist_b
		multScalar(centros_dif_min,-1.0,auxvec,2);
		double dot_AD = multDot(auxvec,DIR_INICIAL,2);
		double value_V = 2*RADIO + TOL;
		double dist_b = - dot_AD + sqrt(dot_AD*dot_AD + value_V*value_V - centros_mod_min*centros_mod_min);
		
		//Update inicial position
		multScalar(DIR_INICIAL,dist_b,auxvec,2);
		addElems(pos_inicial,auxvec,pos_inicial,2);
		get_centros(pos_inicial,DIR_INICIAL,RADIO,c1_h,c1_ah);
		centros_flag = false;
	}
	
	//Trajectory calculation
	double min_dist, min_tita1, min_tita2, min_dist_ang;
	int min_i = -1;
	double min_way1[2],min_way2[2],min_c1[2],min_c2[2];
	for (int i=0; i<4; i++)
	{
		fn_centros_select(&c1_h,&c1_ah,&c2_h,&c2_ah,&c1,&c2,i);
		fn_centros_params(c1,c2,&centros_mod,centros_dif,&centros_flag);
		
		double tita1;
		double tita2;
		
		//Caso 1: Para j=0 y j=1
		double tita_caso1 = atan2(-centros_dif[0],centros_dif[1]);

		//Caso 2: Para j=2 y j=3
		bool valid_angle = (centros_mod > 0.0);
		double alpha,beta;
		if (valid_angle == true)
		{
			beta = atan2(centros_dif[1],centros_dif[0]);
			double aux_cos = 2*RADIO/centros_mod;
			valid_angle = (aux_cos <= 1.0);
			if (valid_angle == true)
			{
				alpha = acos(aux_cos);
			}
		}
		
		double way1[2], way2[2];
		for (int j=0; j<4; j++)
		{
			if (j<2 || valid_angle == true)
			{
				fn_tita_select(tita_caso1, alpha, beta, &tita1, &tita2, j);
				
				//printf("clib i=%d j=%d tita1=%lf tita2=%lf\n",i,j,tita1,tita2);
				//Waypoint 1
				get_waypoint(tita1,c1,way1);
				//Waypoint 2
				get_waypoint(tita2,c2,way2);
				
				double arg1[2],arg2[2],arg3[2];
				substractElems(way2,way1,arg1,2);
				substractElems(way1,c1,arg2,2);
				substractElems(way2,c2,arg3,2);
				bool right_dir = test_direction(arg1, arg2, arg3, i, centros_dif);
				
				double dist_ang, dist_line;
				if (right_dir == true)
				{
					calculate_dist(c1, c2, i, tita1, tita2, dir_final, arg1, &dist_ang, &dist_line);
					double dist = dist_ang + dist_line;
					//printf("distance: %lf\n",dist);
					
					//Min calculation
					if (min_i == -1 || dist < min_dist)
					{
						min_dist = dist;
						min_dist_ang = dist_ang;
						min_i = i;
						
						if (AUXWP == true)
						{
							min_tita1 = tita1;
							min_tita2 = tita2;
							arrayCopy(way1,min_way1,2);
							arrayCopy(way2,min_way2,2);
							arrayCopy(c1,min_c1,2);
							arrayCopy(c2,min_c2,2);
						}
					}
				}
			}
		}
	}
	
	*p_disp_center = false;
	if (CHECK_ANG == true && centros_flag == true)
	{
		if (min_dist_ang > 2.0*M_PI*RADIO + TOL)
		{
			*p_disp_center = true;
			return -1.0;
		}
	}
	
	if (AUXWP == true)
	{
		int cant = 0;
		if (disp_center == true)
		{
			wp_list[0] = pos_inicial[0]/SCALE[0];
			wp_list[1] = pos_inicial[1]/SCALE[1];
			cant++;
		}
		get_aux_waypoints(min_tita1, min_tita2, min_c1, min_c2, min_i, dir_final, min_way1, min_way2, wp_list, &cant);
		
		//destination point
		wp_list[2*cant] = pos_final[0]/SCALE[0];
		wp_list[2*cant+1] = pos_final[1]/SCALE[1];
		cant++;
		
		if (OVERWP == true)
		{
			//No hace falta afectar con scale a pos_final_orig
			wp_list[2*cant] = pos_final_orig[0];
			wp_list[2*cant+1] = pos_final_orig[1];
			cant++;
		}
		
		cant_wp_array[0] = cant;
	}
	
	return min_dist;
}

__device__ double ang_check_trajectory(double *pos_final, double *dir_final, double *wp_list, int *cant_wp_array)
{
	bool disp_center = false;
	double dist;
	double pos_inicial[2];
	double c1_h[2];
	double c1_ah[2];
	arrayCopy(C1_H,c1_h,2);
	arrayCopy(C1_AH,c1_ah,2);
	arrayCopy(POS_INICIAL,pos_inicial,2);
	
	do
	{
		dist = calc_trajectory(pos_inicial, c1_h, c1_ah, pos_final, dir_final, &disp_center, wp_list, cant_wp_array);
	} while (disp_center == true);
	
	return dist;
}

__device__ double parallel_dir_trajectory(double *pos_final, double *dir_final, double *wp_list, int *cant_wp_array, double *sign_dir)
{
	double wp_list1[24], wp_list2[24];
	int cant1 = 0, cant2 = 0;
	double dist1, dist2;
	double neg_dir_final[2];
	
	multScalar(dir_final,-1.0,neg_dir_final,2);	
	dist1 = ang_check_trajectory(pos_final,dir_final,wp_list1,&cant1);
	dist2 = ang_check_trajectory(pos_final,neg_dir_final,wp_list2,&cant2);
	
	if (dist1 > dist2)
	{
		if (AUXWP == true)
		{
			*cant_wp_array = cant2;
			arrayCopy(wp_list2,wp_list,2*cant2);
		}
		*sign_dir = -1.0;
		return dist2;
	}
	
	if (AUXWP == true)
	{
		*cant_wp_array = cant1;
		arrayCopy(wp_list1,wp_list,2*cant1);
	}
	*sign_dir = 1.0;
	return dist1;
}

__device__ double saturate(double value, double limit)
{
	if (value > limit)
	{
		return limit;
	} else if (value < -limit)
	{
		return -limit;
	}
	return value;
}

__device__ double calc_pond(double *pos_final)
{
	double suma[2];
	multElems(pos_final,SCALE,suma,2);
	substractElems(suma,POS_INICIAL,suma,2);
	substractElems(SUMA_POND,suma,suma,2);
	double pond = normArray(suma,2) - POND_ACTUAL;
	
	return saturate(0.9*pond,M_PI*RADIO);
} 

__device__ void select_dir(int ind, double *dir)
{
	switch (ind)
	{
		case 0: //North
			dir[0] = 0.0;
			dir[1] = 1.0;
			break;
		case 1: //East
			dir[0] = 1.0;
			dir[1] = 0.0;
			break;
		case 2: //North-East
			dir[0] = cos(M_PI/4);
			dir[1] = sin(M_PI/4);
			break;
		case 3: //South-East
			dir[0] = cos(-M_PI/4);
			dir[1] = sin(-M_PI/4);
			break;
	}
}

__device__ double multi_dir_trajectory(double *pos_final, double *wp_list, int *cant_wp_array, double *dir_min)
{
	double dist[4], sign_dir[4];
	double dir_final[2];
	
	bool orig_AUXWP = AUXWP;
	AUXWP = false;
	
	for (int i=0; i<4; i++)
	{
		select_dir(i,dir_final);
		dist[i] = parallel_dir_trajectory(pos_final,dir_final,NULL,NULL, &sign_dir[i]);
	}
	
	int min_ind = 0;
	for (int i=1; i<4; i++)
	{
		if(dist[i] < dist[min_ind])
		{
			min_ind = i;
		}
	}
	
	AUXWP = orig_AUXWP;
	
	double dist_tmp = dist[min_ind];
	select_dir(min_ind,dir_final);
	multScalar(dir_final,sign_dir[min_ind],dir_min,2);
	if (AUXWP == true)
	{
		dist_tmp = ang_check_trajectory(pos_final,dir_min,wp_list, cant_wp_array);
	}
	
	return dist_tmp;
}


__global__ void pond_trajectory(double *pos_final_array, double *dir_final, int cant_pos, double *dist_array, double *pos_inicial, double *dir_inicial, double radio, double *scale, bool auxwp, bool overwp, bool check_ang, double *c1_h, double *c1_ah, double overwp_dist, double *suma_pond, int mode)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (i < cant_pos)
	{
		multElems(pos_inicial,scale,POS_INICIAL,2);
		arrayCopy(scale,SCALE,2);
		arrayCopy(dir_inicial,DIR_INICIAL,2);
		RADIO = radio;
		AUXWP = auxwp;
		OVERWP = overwp;
		CHECK_ANG = check_ang;
		arrayCopy(c1_h,C1_H,2);
		arrayCopy(c1_ah,C1_AH,2);
		OVERWP_DIST = overwp_dist;
		arrayCopy(suma_pond,SUMA_POND,2);
		POND_ACTUAL = normArray(suma_pond,2);

		//Set wp variable to false, just in case
		AUXWP = false;
		double pos_final[2];
		double sign_dir = 0.0;
		double dir_aux[2];
		
		pos_final[0] = pos_final_array[2*i];
		pos_final[1] = pos_final_array[2*i+1];
		double pond = calc_pond(pos_final);
		double dist;
		
		if (mode == 0)
		{
			dist = parallel_dir_trajectory(pos_final, dir_final, NULL, NULL, &sign_dir);
		} else if (mode == 1)
		{
			dist = multi_dir_trajectory(pos_final, NULL, NULL, dir_aux);
		}
		
		dist_array[i] = dist - pond;
	}
}














