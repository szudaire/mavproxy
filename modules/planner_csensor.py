from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from os.path import expanduser
import numpy as np

class CSensor(Module):
	def init(self):
		self._process_type = 0
		self._list = [573]
		
		self._start_time = -1.0
		self._started = False
		self._ended = False
		self._counter = 0
		return

        def patrolA(self):
                self._counter += 1
                if (self._counter == 2):
                        self._start_time = time.time()
                        self._sharedmem.set_logging(True)
                elif (self._counter == 3):
                        self._sharedmem.set_logging(False)

                if (self._counter >= 3):
                        self._add_to_message_queue(mesg.TIMER,str(time.time() - self._start_time))
                        self._start_time = time.time()

                if (self._counter >= 33):
                        self._add_to_mavproxy_command_queue("rtl")
                                

	def has_C_current(self):
                current = self._sharedmem.get_current_location()
                if (current in self._list):
                        return 'yes_C_current'
                else:
                        return 'no_C_current'

	def has_C_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (next_pos in self._list):
                        return 'yes_C_next'
                else:
                        return 'no_C_next'
