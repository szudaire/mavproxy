from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from os.path import expanduser
import numpy as np

class ASensor(Module):
	def init(self):
		self._process_type = 0
		self._list = [129]
		return

	def has_A_current(self):
                current = self._sharedmem.get_current_location()
                if (current in self._list):
                        return 'yes_A_current'
                else:
                        return 'no_A_current'

	def has_A_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (next_pos in self._list):
                        return 'yes_A_next'
                else:
                        return 'no_A_next'
