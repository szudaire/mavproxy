from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.planner_module import Module
import numpy as np
import time
import subprocess as sp
import cv2
from os.path import expanduser,getctime

varA = 0

class Camera(Module):
    def init(self):
        self._process_type = 1
        self.controllables = ['capture']
        home = expanduser("~")
        self.path = home + '/.cache/mavproxy/captures/'
        self.count = 0

        command = "v4l2-ctl -d 0 -c exposure_auto=3" # -c exposure_absolute=2"
        output = sp.call(command, shell=True)
        return

    def capture(self):
        loc_current_pos = self._sharedmem.get_position()
        self._add_to_message_queue(mesg.FOUND,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
        return "interesting"
    
    def capture_old(self):
        roll,pitch,yaw = self._sharedmem.get_attitude()
        roll = -roll
        #pitch = np.deg2rad(-30)
        #yaw = np.deg2rad(30)
        H = self._sharedmem.get_relative_altitude()
        width = 800.0
        height = 600.0
        relx = height/width
        relz = 2.29 #73.5/32 cm/cm
        param = 1.0/np.sqrt(1.0 + relx**2 + relz**2)

        vA = param*np.array([-relx,1,relz],np.float)
        vB = param*np.array([relx,1,relz],np.float)
        vC = param*np.array([-relx,-1,relz],np.float)
        vD = param*np.array([relx,-1,relz],np.float)
        
        v_list = [vA,vB,vD,vC,vA]
        v_trans = []
        for v in v_list:
            v_trans.append(euler_transform(pitch,roll,yaw,v))
        #for p in p_list:
            #p_trans.append(euler_transform(pitch,roll,yaw,p))

        c_list = []
        for i in range(len(v_trans)):
            v = v_trans[i]
            p_prima = - np.array([0,0,H],np.float)

            fac = p_prima[2]/v[2]
            x = p_prima[0] - v[0]*fac
            y = p_prima[1] - v[1]*fac

            c_list.append([x,y])

        #print(c_list)
        pos_curr = self._sharedmem.get_position()
        zone,posx,posy = llutm.LLtoUTM(pos_curr[1],pos_curr[0])
        self._add_to_message_queue(mesg.EST_POS_LIST_START,str(len(c_list)))
        for elem in c_list:
            pos_lat,pos_lon = llutm.UTMtoLL(-elem[0]+posy,elem[1]+posx,zone)
            self._add_to_message_queue(mesg.EST_POS_LIST_ELEM,str(pos_lat) + " " + str(pos_lon))    
        self._add_to_message_queue(mesg.EST_POS_LIST_END,"camera")

        ind_curr = self._sharedmem.get_current_location()
        pos_curr = self._sharedmem.get_discretizer().get_position(ind_curr)
        posx = pos_curr[0]
        posy = pos_curr[1]
        delta = self._sharedmem.get_grid_param()/2
        delta = self._sharedmem.meters2latlon(delta)
        vertex_list = []
        vertex_list.append([posx-delta[0],posy-delta[1]])
        vertex_list.append([posx-delta[0],posy+delta[1]])
        vertex_list.append([posx+delta[0],posy+delta[1]])
        vertex_list.append([posx+delta[0],posy-delta[1]])
        vertex_list.append(vertex_list[0])

        self._add_to_message_queue(mesg.EST_POS_LIST_START,str(len(vertex_list)))
        for elem in vertex_list:
            self._add_to_message_queue(mesg.EST_POS_LIST_ELEM,str(elem[1]) + " " + str(elem[0]))    
        self._add_to_message_queue(mesg.EST_POS_LIST_END,"location")
        
        self._add_to_command_queue('capture')

    def _capture(self):
        self._add_to_event_queue('no_person')
        return
        
        t1 = time.time()
        
        cam = cv2.VideoCapture(0)
        for i in range(15):
            next, frame = cam.read()
        while not next:
            next, frame = cam.read()
        self.count += 1
        cv2.imwrite(self.path + str(self.count) + ".png", frame)
        cam.release()
        
        self._add_to_message_queue(mesg.PRINT,"Captured " + str(self.count) + " in " + str(time.time() - t1))
        
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        lower_red1 = np.array([0,45,60])
        upper_red1 = np.array([15,255,240])
        lower_red2 = np.array([160,45,60])
        upper_red2 = np.array([255,255,240])
        mask = cv2.inRange(hsv, lower_red1, upper_red1) + cv2.inRange(hsv, lower_red2, upper_red2)
        
        num = len(mask[mask>200])
        self._add_to_message_queue(mesg.PRINT,"NUM red: " + str(num))
        if (num > 5000):
            self._add_to_event_queue('yes_person')
            position = self._sharedmem.get_position()
            self._add_to_message_queue(mesg.FOUND,str(position[1]) + " " + str(position[0]))
        else:
            self._add_to_event_queue('no_person')


def euler_transform(pitch,roll,yaw,vec):
    Myaw = np.array([[np.cos(yaw),np.sin(yaw),0],[-np.sin(yaw),np.cos(yaw),0],[0,0,1]],np.float)
    Mpitch = np.array([[np.cos(pitch),0,-np.sin(pitch)],[0,1,0],[np.sin(pitch),0,np.cos(pitch)]],np.float)
    Mroll = np.array([[1,0,0],[0,np.cos(roll),np.sin(roll)],[0,-np.sin(roll),np.cos(roll)]],np.float)
    return Myaw.dot(Mpitch.dot(Mroll.dot(vec)))
