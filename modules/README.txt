------------------------------------------------------------------------------------------------------------------------
CORRER EL SOFTWARE

Para seleccionar entre el QUADCOPTER y el AVION hay que modificar el config_file.py segun lo que indican los comentarios

1) Abrir dos terminales en la carpeta
AVION:
~/Desktop/ardupilot/ArduPlane/
QUADCOPTER:
~/Desktop/ardupilot/ArduCopter/

	--> Terminal 1:

	a) Conseguir permisos Root (la pass es 'uav'): 
	su

	b) Correr el simulador SITL + el visor de la mision: 
	AVION:
	sim_vehicle.py -N -m '--load-module planner_viewer' -L 'TestPatrol' --console --map
	QUADCOPTER:
	sim_vehicle.py -N -m '--load-module planner_viewer --load-module parrot' -L 'TestPatrol' --console --map

	(en el caso que diga que "Vehicle binary (/home/uav/Desktop/ardupilot/build/sitl/bin/arducopter) does not exist", correr lo mismo por unica vez sin la opcion "-N")

	--> Terminal 2:

	a) Conseguir permisos Root (la pass es 'uav'): 
	su

	b) Correr el planner abordo del UAV: 
	AVION
	mavproxy.py --master=tcp:0.0.0.0:5762 --console --load-module planner
	QUADCOPTER
	mavproxy.py --master=tcp:0.0.0.0:5762 --console --load-module planner  --load-module parrot

2) Entre la Terminal 1 y el mapa se define la mision

	a) En el Mapa hacer click derecho y seleccionar: PFence -> Draw
	b) Ir haciendo click en el mapa en los lugares que definiran el poligono de la region de vuelo
	c) Para terminar hacer click derecho
	d) Si se quiere cambiar el poligono volver a hacer a)
	e) Cuando la region este definida, correr este comando para discretizar la region en la terminal: pstart
	f) Para elegir una region de no vuelo repetir la secuencia a), b), c) y luego correr en la terminal: pregion NF
	(en este caso llame la region de no vuelo NF, pero se le puede poner otro nombre)
	g) Hacer f) por cada region de no vuelo deseada
	h) Cuando esten definidas todas las regiones de no vuelo correr: pnofly NF
	(donde hay que numerar separado por un espacio todas las regiones de no vuelo, por ejemplo: pnofly NF1 NF2 noflynombrequeinvente NF4)

Para el caso de la misión 1.a) del trabajo de RV-UAV:
	i) Cargar el plan de vuelo con el comando "pplan plans/Findsafe.txt". Es necesario para esta mision haber definido una region de no-fly con el comando: "pnofly" (ver inciso h)). Esta mision genera un "capture" cada vez que visita una nueva locacion y responde a los 0.5s con un "no_person".
Caso contrario:
	i) Cargar el plan de vuelo con el comando "pplan plans/Cover_All.txt"

	j) Cuando diga "Automata loaded" se puede dar comienzo a la mision con el comando: "pinit"
	k) Si no pasa nada por varios segundos, reintentar el takeoff. escribiendo en cualquiera de las dos terminales el comando "TKOF". (esto solo es necesario muy de vez en cuando).
	l) Cuando termine la mision por defecto ingresara al modo RTL y volvera a base.
	m) Para cortar la simulacion en cualquier momento correr el comando: pexit
	n) Se puede acelerar la simulacion con el comando "param set SIM_SPEEDUP 3" donde 3 es 3x de aceleracion

3) Si se quiere reusar las mismas regiones que antes se puede hacer "plreg REGION" al principio para cargar la misma region discreta que la mision anterior.
Se puede hacer lo mismo con las regiones de no vuelo: "plreg NF1", "plreg NF2", etc y despues correr de vuelta "pnofly NF1 NF2"

------------------------------------------------------------------------------------------------------------------------
HACER CAMBIOS AL SOFTWARE

1) La base del repositorio esta en ~/Desktop/MAVProxy/MAVProxy/* y la direccion es https://bitbucket.org/szudaire/MAVProxy

2) Modificar los archivos que se desee de ~/Desktop/MAVProxy/MAVProxy/modules/. 
Los modulos de MAVProxy que cargan el planner y el visor de mision son:
--> mavproxy_planner.py
--> mavproxy_planner_viewer.py

El resto de los archivos y modulos auxiliares siguen la siguiente notacion:
--> planner_*

3) Correr como root el archivo: './install.sh'

------------------------------------------------------------------------------------------------------------------------
PARA REPLICAR INSTALACION EN OTRA PC:

Pasos para instalar SITL:
apt-get install git
git clone https://github.com/ArduPilot/ardupilot
cd ardupilot
git submodule update --init --recursive --progress
Copiar el archivo install-prereqs-debian.sh a la carpeta ardupilot/Tools/scripts/ (probablemente se tenga que hacer en sudo)
chmod -R 755 ../ardupilot/
Tools/scripts/install-prereqs-debian.sh -y
Agregar al path la ubicacion de sim_vehicle.py
Agregar al path la ubicacion /root/.local/bin <-- el mavproxy va a parar aca
Quitar del path la ubicacion /usr/local/bin

Paquetes para que funcione el mavproxy
# pip install future
apt-get install python-dev python-opencv python-wxgtk3.0 python-pip python-matplotlib python-pygame python-lxml python-yaml

Pasos para que funcione el planner
apt-get install libgeos-dev
pip install shapely

Copiar la carpeta MAVProxy a la nueva PC y volver a correr ./isntall.sh

------------------------------------------------------------------------------------------------------------------------
HLola:

Junto al repositorio de MAVProxy, clonar el branch mavproxy del repositorio de HLola:
$ git clone -b mavproxy https://gitlab.software.imdea.org/streamrv/hlola.git

La especificación a monitorizar es spec::Specification en el archivo hlola/app/Spec.hs

Via CABAL:
apt-get install cabal
En el directorio con el .cabal:
cabal update -v3
cabal configure
cabal install --only-dependencies
cabal build

Si no alcanza la RAM para hacer install de "aeson", correr asi:
cabal install aeson -f fast
