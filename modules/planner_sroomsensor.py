from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.mavproxy_map import mp_tile
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import numpy as np
import time
from os.path import expanduser

class SRoomSensor(Module):
    def init(self):
        home = expanduser("~")
        path = home + '/.cache/mavproxy/cache/'
        self.path = home + '/.cache/mavproxy/captures/'
        self._tile = mp_tile.MPTile(cache_path=path,debug=False)
        self._height = 300
        self._width = 300
        self._field_of_view = 50.0/150.0 #100 meters at 100 meters of altitude
        self._aux_radius = 40.0 #Radius for intermediate waypoints
        
        self._process_type = 1
        return
    
    def _started(self):
        pass
    
    def _download_image(self,lat,lon,ground_width):
        image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
        while (self._tile.tiles_pending() > 0):
            time.sleep(0.01)
        image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)

        image = cv.cvtColor(image,cv.COLOR_RGB2BGR)
        return image

    def add_locations(self,pos_locations):
        if (len(pos_locations) < 2000):
            self._image_list = [[]]*len(pos_locations)
        else:
            self._image_list = [[]]*2000 #len(pos_locations)
        self._border_list = Array('i',range(len(pos_locations)))
        self._nofly_list = Array('i',range(len(pos_locations)))
        self._add_to_command_queue('add_locations')
        
        self._location_list = Array('i',range(len(pos_locations)))
        self._array_end = Value('i',0)
        
    def _add_locations(self):
        ground_width = self._field_of_view*self._sharedmem.get_flight_height()

        cant = 0
        for i in range(len(self._image_list)):
            elem = self._sharedmem.get_discretizer().get_capture_polygon(i,ground_width)[1,:]
            image = self._download_image(elem[1],elem[0],ground_width)
            self._image_list[i] = image

            cant += 1
            if (cant == 10): #Print a download message every X locations
                self._add_to_print_queue("Downloaded location " + str(i+1) + " of " + str(len(self._image_list)))
                cant = 0

        self._add_to_print_queue("Finished Downloading")
        
        fire_vec = np.array([0]*len(self._image_list))
        for i in range(len(self._image_list)):
            ret = self._process_image(self._image_list[i])
            if (ret):
                fire_vec[i] = 1

        locations = self._sharedmem.get_discretizer().get_position(range(len(self._image_list)))
        locations = locations * self._sharedmem.get_scale()
        fire_loc = locations[np.flatnonzero(fire_vec == 1)]
        nofly_dist = self._sharedmem.meters2latlon(60)[1]
        for i in range(len(self._image_list)):
            min_dist = np.min(np.linalg.norm(fire_loc - locations[i],axis=1))
            
            if (min_dist < nofly_dist):
                self._nofly_list[i] = 1
                loc_pos = self._sharedmem.get_discretizer().get_position(i)
                self._add_to_message_queue(mesg.NOFLY,str(loc_pos[1]) + ' ' + str(loc_pos[0]))
            else:
                self._nofly_list[i] = 0

        self._add_to_print_queue("Finished Calculating SRooms")

    def _process_image(self,image):
        hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        # define range of blue color in HSV
        lower_blue = np.array([90,100,30])
        upper_blue = np.array([110,255,255])
        mask = cv.inRange(hsv, lower_blue, upper_blue)
        
        blur = cv.blur(mask,(5,5))
        #cv.imshow('process',mask)

        cant = len(blur[blur<50])
        if (cant > 30000):
            return True
        else:
            return False

    def has_SRoom_next(self):
        next_pos = self._sharedmem.get_next_location()
        if (self._nofly_list[next_pos] == 1):
            return "yes_SRoom_next"
        else:
            return "no_SRoom_next"
