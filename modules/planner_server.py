from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Queue
from MAVProxy.modules.planner_colors import pcolors
import numpy as np
import time
import socket

class Server(Module):
    def init(self):
        self._queue_locations = Queue()
        self._queue_automata = Queue()
        self._queue_code = Queue()
        self._queue_fire = Queue()
        self._process_type = 1
        return
    
    def _started(self):
        pass

    def receive_fire(self):
        self._add_to_command_queue('receive_fire')
        
    def receive_code(self):
        self._add_to_command_queue('receive_code')
        
    def receive_locations(self):
        self._add_to_command_queue('receive_locations')
        
    def receive_automata(self):
        self._add_to_command_queue('receive_automata')

    def retreive_fire(self):
        if self._queue_fire.qsize() > 0:
            return self._queue_fire.get()
        else:
            return []
        
    def retreive_code(self):
        if self._queue_code.qsize() > 0:
            return self._queue_code.get()
        else:
            return []
        
    def retreive_locations(self):
        if self._queue_locations.qsize() > 0:
            return True,self._queue_locations.get()
        else:
            return False,[]
        
    def retreive_automata(self):
        if self._queue_automata.qsize() > 0:
            return self._queue_automata.get()
        else:
            return []
        
    def _receive_fire(self):
        message,delta = self._get_data_from_socket()
        locations = np.fromstring(message,np.int)
        self._add_to_print_queue(pcolors.color("Received " + str(len(locations)) + " fire elements in " + str(delta) + " secs",'green'))
        self._queue_fire.put(locations)
        return
    
    def _receive_code(self):
        message,delta = self._get_data_from_socket()
        
        self._add_to_print_queue(pcolors.color("Received code of size " + str(len(message)) + " in " + str(delta) + " secs",'green'))
        self._queue_code.put(message)
        return
    
    def _receive_locations(self):
        message,delta = self._get_data_from_socket()
        
        locations = np.fromstring(message,np.float_)
        locations = locations.reshape(len(locations)/2,2)
        
        self._add_to_print_queue(pcolors.color("Received " + str(len(locations)) + " locations in " + str(delta) + " secs",'green'))
        
        self._queue_locations.put(locations)
        return

    def _get_data_from_socket(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try: 
            host_name = socket.gethostname() 
            host_ip = socket.gethostbyname(host_name + ".local")
        except:
            self._add_to_message_queue(mesg.ERROR,"Unable to get Host IP")
            return
        
        s.bind((host_ip, 50000))
        s.listen(1)
        self._add_to_message_queue(mesg.HOSTIP,host_ip)
        
        conn, addr = s.accept()
        
        t1 = time.time()
        #Connection established, send locations
        message = ''
        while 1:
            data = conn.recv(1024)
            message = message + data
            if not data:
                break
        conn.close()
        s.close()
        
        t2 = time.time()

        return message, t2-t1

    def _receive_automata(self):
        message,delta = self._get_data_from_socket()

        self._add_to_print_queue(pcolors.color("Received automata of size " + str(len(message)) + " in " + str(delta) + " secs",'green'))
        
        self._queue_automata.put(message)
        return 
