from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.planner_module import Module
from ctypes import c_bool
from multiprocessing import Value,Queue
import numpy as np
from MAVProxy.modules import planner_util_message as mesg
import math
import time
import thread

varA = 0

class Quad(Module):
    def init(self):
        self._set_suscription_packets(["ATTITUDE","MISSION_ACK","GLOBAL_POSITION_INT","MISSION_ITEM_REACHED"])
        
        # Initial time
        self._init_time = time.time()
        self._config_time = 5
        self._config_init = False
        
        # Waypoint data
        self._MIN_PITCH = 15.0
        self._CMD_TAKEOFF = 22
        self._CMD_LAND = 21
        self._CMD_WP = 16
        
        # Waypoint list extra data
        self._header = "QGC WPL 110"
        self._filename = "wp_temp.txt"
        
        # Waypoint list
        self._wp_default = np.array([0.000000,0.000000,0.000000,0.000000,-35.363262,149.165237,100.0])
        
        self._wp_count = Value('i',0)
        self._wp_len = Value('i',0)
        self._wait_ack = Value(c_bool,False)
        self._first_wp = Value(c_bool,True)
        self._takeoff_ended = False
        
        self._in_flight = Value(c_bool,False)
        self._takeoff_flag = Value(c_bool,False)

        self._do_spin = Value(c_bool,False)

        #Time the flight
        self._start_time = Value('d',0.0)
        self._started_time = False

        #Acceptance radius
        self._loc_radius = 1.0 #Radius for locations to visit
        self._loc_radius_scaled = 0.0
        self._aux_radius = 2.0 #Radius for intermediate waypoints
        self._default_radius = 2.0 #Default radius
        self._straight_dist = 5.0 #350.0 #Straight wp distance
        self._parrot_delay = 1
	self._delay = 9.0

        self._log_in_flight = False
        self._using_iterator = Value(c_bool,False)

        self._mark_wps = False
        self._auto_takeoff = True
        self._parrot = False
        self._count_ack = 0

        self.spin_time = 20.0

        self._nofly_set = Value(c_bool,False)
        self._nofly_queue = Queue()
        self._nofly_reg = None
        self._use_motion_planner = False

	self.controllables = ['takeoff', 'go_next', 'land', 'abort_go','go','rtl','do_spin','set_high_height','set_low_height']

    def set_nofly_regions(self,nofly):
        self._nofly_queue.put(nofly)
        self._nofly_set.value = True
    
    def set_high_height(self):
        self._sharedmem.set_flight_height(self._high_height)
    
    def set_low_height(self):
        self._sharedmem.set_flight_height(self._low_height)
    
    def init_config(self):
        time_aux = time.time()
        
        if (self._raspi_zero):
        	self._config_time = 20
        
        	
        # Initial conditions
        if (time_aux > self._init_time + self._config_time):
            if (self._config_init == False):
                self._add_to_mavproxy_command_queue("set moddebug 3")
                self._add_to_mavproxy_command_queue("param set SIM_SPEEDUP 15")

                #Set initial turn radius
                if (self._sharedmem.get_turn_radius() == 0.0):
                    radius = 85.0
                    self._sharedmem.set_turn_radius(radius)
                    self._add_to_message_queue(mesg.PRINT,"DEFAULT: Radius set to " + str("%.2f" % radius) + " meters")
                
                self.set_home()
                self._config_init = True

                self._loc_radius_scaled = self._sharedmem.meters2latlon(self._loc_radius)[1]
                return True
        return False
    
    def _set_waypoint(self):
        if (self._parrot):
            self._add_to_mavproxy_command_queue("wp set 0")
        else:
            self._add_to_mavproxy_command_queue("wp set 1")

        if (self._do_spin.value):
            thread.start_new_thread(spin_process,(self._queue_mavproxy_command,self.spin_time))
            
        #Update waypoints on ground station
        self._add_to_message_queue(mesg.UPDATE_WP,"")
    
    def _wp_along_direction(self,position,distance,direction):
        return position + direction*np.flip(self._sharedmem.meters2latlon(distance),0)
        
    def _load_waypoints(self, command, list_wp, altitude,dir,radius,delay=0.0,load=True):
        file_object = open(self._filename,'w')
        file_object.write(self._header + "\n")
        if (self._parrot):
            delay = self._parrot_delay

        #Waypoint home
        if (not self._parrot):
            file_object.write(self._create_wp_string(self._CMD_WP,0,self._wp_default,altitude,self._default_radius,delay))
            i = 0
        else:
            i = -1

        #Waypoints in the middle
        for j in range(len(list_wp)-1):
            i += 1
            file_object.write(self._create_wp_string(command,i,list_wp[i-1],altitude,self._aux_radius,delay))

        i += 1
        #Waypoint of interest (location)
        file_object.write(self._create_wp_string(command,i,list_wp[i-1],altitude,self._loc_radius,self._delay))
        
        i += 1
        #Waypoint extra along direction
        file_object.write(self._create_wp_string(self._CMD_WP,i,self._wp_along_direction(list_wp[-1],self._straight_dist,dir),altitude,self._default_radius,self._delay))

	i += 1
        #Waypoint extra along direction
        file_object.write(self._create_wp_string(self._CMD_WP,i,self._wp_along_direction(list_wp[-1],self._straight_dist*2,dir),altitude,self._default_radius,delay))

        file_object.close()

        if load:
            self._set_wait_ack(True)
            self._set_wp_len(len(list_wp))
            self._set_wp_count(0)
            self._add_to_mavproxy_command_queue("wp load " + self._filename)
    
    def _create_wp_string(self,command,ind,pos,alt,radius,delay):
        #Default intermediate waypoint radius is radius
        
        aux_string = str(ind) + "\t" + str(0)
        
        if (ind == 0 and self._parrot == False): #HOME
            axis = 0
            wp_params = np.array([0.000000,0.000000,0.000000,0.000000,self._wp_home[0],self._wp_home[1],self._wp_home_alt])
        else: #WAYPOINT
            axis = 3 #Relative altitudes
            wp_params = np.copy(self._wp_default)
            wp_params[0] = delay

            wp_params[-1] = alt #Altitude
            wp_params[4:6] = pos 
            wp_params[1] = radius #Radius hit waypoint
            
            if (command == self._CMD_TAKEOFF):
                wp_params[0] = self._MIN_PITCH
            
        aux_string = aux_string + "\t" + str(axis) + "\t" + str(command)
        
        for param in wp_params:
            aux_string = aux_string + "\t" + str(param)
        aux_string = aux_string + "\t" + str(1) + "\n" # Autocontinue
        return aux_string
    
    def rtl(self):
        self._add_to_mavproxy_command_queue("rtl")
    
    def takeoff(self):
        if (self._log_in_flight):
            self._sharedmem.set_logging(True)
        
        if (not self._started_time):
            self._started_time = True
            self._start_time.value = time.time()
            
        #Initial waypoints for automatic takeoff
        direction = np.flip(self._sharedmem.get_direction(),0)
        position = np.flip(self._sharedmem.get_position(),0)
        self._load_waypoints(self._CMD_WP,[position],self._sharedmem.get_flight_height(),direction,self._default_radius)

        #Flag used to put the autopilot in AUTO when it finishes loading the waypoints
        self._set_takeoff_flag(True)
        return

    def _loiter_waypoints(self):
        loc_next_pos = self._sharedmem.get_position()
        loc_current_pos = self._sharedmem.get_position()
        
        self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=False,check_ang=False,pond=False)
        wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=True)
        
        wp_list = np.flip(wp_list,1)

        altitude = self._sharedmem.get_flight_height()
        self._load_waypoints(self._CMD_WP,wp_list,altitude,np.flip(dir,0),self._loc_radius)
        return
    
    def land(self):
        if (self._log_in_flight):
            self._sharedmem.set_logging(False)
        total_flight_time = str(time.time() - self._start_time.value)
        self._add_to_print_queue("TOTAL FLIGHT TIME: " + total_flight_time)
        self._add_to_message_queue(mesg.TIMER,total_flight_time)
        
        self._sharedmem.set_iteration_status(False)
        if (self._auto_takeoff == False or 1==1):
            #Return to launch instead of automatic landing
            self.rtl()
        else:
            if (self._parrot):
                self._add_to_mavproxy_command_queue("land")
            else:
                self._add_to_mavproxy_command_queue("stabilize")
            '''
            #Waypoints for automatic landing
            direction = np.flip(self._sharedmem.get_direction(),0)
            position = np.flip(self._sharedmem.get_position(),0)
            self._load_waypoints(self._CMD_LAND,[self._wp_along_direction(position,800.0,direction)]*self._get_wp_len(),0.0,direction,self._default_radius)
            '''
        return

    def do_spin(self):
        self._set_in_flight(True)
        altitude = self._sharedmem.get_flight_height()
        self._load_waypoints(self._CMD_WP,self._wp_list,altitude,np.flip(self._dir,0),self._loc_radius,delay=self.spin_time)
        self._do_spin.value = True
    
    def _get_using_iterator(self):
        return self._using_iterator.value

    def _set_using_iterator(self,value):
        self._using_iterator.value = value
    
    def go(self,loc_next,using_iterator=False,noevents=False):
        loc_next = int(loc_next)
        self._set_using_iterator(using_iterator)
        
        loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)
        self._sharedmem.set_next_location(loc_next)

        self._go_to_loc(loc_next_pos,noevents=noevents)
        
    def go_next(self):        
        self._set_using_iterator(True)
        self._sharedmem.set_iteration_status(False)
        
        loc_next = self._sharedmem.get_next_location()
        loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)

        self._go_to_loc(loc_next_pos)

    def _go_to_loc(self,loc_next_pos,noevents=False):
        if noevents == False:
            self._set_in_flight(True)
            
        loc_current = self._sharedmem.get_current_location()
        if(loc_current == -1):
            loc_current_pos = self._sharedmem.get_position()
        else:
            loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)

        if (self._nofly_set.value == True):
            self._nofly_reg = self._nofly_queue.get()
            self._nofly_set.value = False
            self._use_motion_planner = True
            self._sharedmem.add_obstacles(self._nofly_reg)
            self._add_to_message_queue(mesg.PRINT,"Using RRT motion planner")
        
        #Esta condicion era necesaria antes cuando podia diferir un poco de donde arrancaba el avion
        '''
        position = self._sharedmem.get_position()
        if (self._compare_distance(position,loc_current_pos,self._aux_radius)): #Quizas esto es poco
            loc_current_pos = position'''

        if (self._use_motion_planner):
            wp_list = self._sharedmem.rrt_solver(loc_current_pos,loc_next_pos)
            if (len(wp_list) > 0):
                dir = (wp_list[-1] - wp_list[-2])*self._sharedmem.get_scale()
                wp_list = wp_list[1:]
            else:
                return 'no_path' #No flight path
        else:
            wp_list = np.array([loc_next_pos])
            dir = (loc_next_pos - loc_current_pos)*self._sharedmem.get_scale()
            
        mod = np.linalg.norm(dir)
        dir = dir / mod

        #Update monitor
	radius_list = [self._aux_radius]*(len(wp_list) - 1)
	radius_list.append(self._loc_radius)
	radius_list = np.array(radius_list,np.float)
        self._sharedmem.monitor.set_target(loc_next_pos,loc_current_pos,wp_list,radius_list)

        wp_list = np.flip(wp_list,1)
        self._wp_list = wp_list
        self._dir = dir
        
        self._sharedmem.set_arrived_dir(dir)

        altitude = self._sharedmem.get_flight_height()
        self._load_waypoints(self._CMD_WP,wp_list,altitude,np.flip(dir,0),self._loc_radius)

    def _compare_distance(self,pos1,pos2,distance):
        dist_latlon = self._sharedmem.meters2latlon(distance)[1]

        vec = pos1 - pos2
        scale = self._sharedmem.get_scale()
        vec = vec*scale

        if (np.linalg.norm(vec) > dist_latlon):
            return True
        else:
            return False
    
    def abort_go(self):
        self._set_in_flight(False)
        self._sharedmem.set_current_location(-1)
        self._sharedmem.set_arrived_dir_flag(False)

    def set_home(self):
        self._wp_home = np.flip(self._sharedmem.get_position(),0)
        self._wp_home_alt = self._sharedmem.get_altitude()
        self._add_to_message_queue(mesg.PRINT,"Home set to " + str("%.5f" % self._wp_home[0]) + " " + str("%.5f" % self._wp_home[1]) + " alt: " + str("%.2f" % self._wp_home_alt))
    
    def _set_in_flight(self,value):
        self._in_flight.value = value
    
    def _get_in_flight(self):
        return self._in_flight.value
        
    def _set_wait_ack(self,value):
        self._wait_ack.value = value
    
    def _get_wait_ack(self):
        return self._wait_ack.value
    
    def _set_wp_len(self,value):
        self._wp_len.value = value
    
    def _get_wp_len(self):
        return self._wp_len.value
        
    def _set_wp_count(self,value):
        self._wp_count.value = value
        
    def _add_wp_count(self):
        self._wp_count.value += 1
    
    def _get_wp_count(self):
        return self._wp_count.value
    
    def _set_first_wp(self,value):
        self._first_wp.value = value
    
    def _get_first_wp(self):
        return self._first_wp.value

    def _set_takeoff_flag(self,value):
        self._takeoff_flag.value = value

    def _get_takeoff_flag(self):
        return self._takeoff_flag.value
    
    def _position_update(self, GLOBAL_POSITION_INT):
        '''update gps position'''
        lat = GLOBAL_POSITION_INT.lat*0.0000001
        lon = GLOBAL_POSITION_INT.lon*0.0000001
        relative_alt = GLOBAL_POSITION_INT.relative_alt*0.001 # mm2meter conversion
        alt = GLOBAL_POSITION_INT.alt*0.001
        
        self._sharedmem.set_position(np.array([lon,lat]))
        self._sharedmem.set_altitude(alt)
        self._sharedmem.set_relative_altitude(relative_alt)
        
        '''update ground velocity'''
        vx = GLOBAL_POSITION_INT.vx*0.01 # cm2metre
        vy = GLOBAL_POSITION_INT.vy*0.01 # cm2metre
        vz = GLOBAL_POSITION_INT.vz*0.01 # cm2metre
        
        '''update heading angle'''
        hdg = GLOBAL_POSITION_INT.hdg*0.01 # cdeg2deg
        
        direction = np.array([vy,vx])
        mod = np.linalg.norm(direction,axis=0)

        '''
        if(mod > 2): # m/s
            self._sharedmem.set_direction(direction/mod)
        else:
            direction = np.array([math.sin(math.radians(hdg)),math.cos(math.radians(hdg))])
            self._sharedmem.set_direction(direction)'''
        

    def _attitude_update(self, ATTITUDE):
        self._sharedmem.set_attitude(ATTITUDE.roll,ATTITUDE.pitch,ATTITUDE.yaw)
        
        #for the quad always set direction with hdg <--- revisar, cambiar por yaw
        direction = np.array([math.sin(ATTITUDE.yaw),math.cos(ATTITUDE.yaw)])
        self._sharedmem.set_direction(direction)
    
    def mavlink_packet(self,m):
        mtype = m.get_type()

        if mtype == "ATTITUDE":
            self._attitude_update(m)
        
        elif mtype == "MISSION_ACK":
            if (self._get_wait_ack()):
                self._count_ack += 1
                if (self._count_ack == 2):
                    self._count_ack = 0
                    self._set_wait_ack(False)
                    self._set_waypoint()

                    if (self._get_takeoff_flag()):
                        self._set_in_flight(True)
                        self._set_takeoff_flag(False)
                        
                        if (self._auto_takeoff):
                            if (self._parrot):
                                self._add_to_mavproxy_command_queue("takeoff 0")
                            else:
                                self._add_to_mavproxy_command_queue("TKOF")
                        
        elif mtype == "GLOBAL_POSITION_INT":
            self._position_update(m)
            if(self._get_in_flight()):
                pos_curr = self._sharedmem.get_position()
                ind_next = self._sharedmem.get_next_location()
                if ind_next >= 0:
                    pos_next = self._sharedmem.get_discretizer().get_position(ind_next)
                    scale = self._sharedmem.get_scale()

                    if (np.linalg.norm((pos_next - pos_curr)*scale) < self._loc_radius_scaled):
                        self.generate_events()
            
        elif mtype == "MISSION_ITEM_REACHED":
            self._add_wp_count()
            if (self._get_wp_count() >= self._get_wp_len()):  
                if(self._get_in_flight()):
                    self.generate_events()

    def generate_events(self):
        self._set_in_flight(False)
        if (not self._takeoff_ended):
            self._add_to_event_queue('takeoff_ended')
            self._takeoff_ended = True
        else:
            if (self._do_spin.value):
                self._do_spin.value = False
                print "FINISHED"
                self._add_to_event_queue('spin_finished')
            else:
                loc_next = self._sharedmem.get_next_location()
                self._sharedmem.set_current_location(loc_next)
                if (self._get_using_iterator()):
                    self._add_to_event_queue('arrived')
                else:
                    self._add_to_event_queue('arrived[' + str(loc_next) + "]")
                
                #Send location reached to ground station
                loc_current = self._sharedmem.get_current_location()
                loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)
                if (self._mark_wps):
                    self._add_to_message_queue(mesg.MARK_WP,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                    time.sleep(0.01)
                    self._add_to_message_queue(mesg.MARK_WP,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))


def spin_process(mavproxy_command_queue,spin_time):
    steps = 8
    dt = spin_time/steps
    dyaw = 360.0/steps
    for i in range(steps):
        mavproxy_command_queue.put("setyaw "+str(dyaw)+" 0 1")
        time.sleep(dt)
