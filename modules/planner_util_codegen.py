def create_sensor_from_points(points,sensor_name):
    concat_points = ''
    for i in range(len(points)):
        if i > 0:
            concat_points += ','
        concat_points += str(points[i])       
    code = '''
from MAVProxy.modules.planner_module import Module

class '''+sensor_name+'''Sensor(Module):
    def init(self):
	self._process_type = 0
	self._list = ['''+concat_points+''']
        self.controllables = ["patrol'''+sensor_name+'''","has_'''+sensor_name+'''_current","has_'''+sensor_name+'''_next"]
	return

    def patrol'''+sensor_name+'''(self):
        return

    def has_'''+sensor_name+'''_current(self):
        current = self._sharedmem.get_current_location()
        if (current in self._list):
            return 'yes_'''+sensor_name+'''_current'
        else:
            return 'no_'''+sensor_name+'''_current'

    def has_'''+sensor_name+'''_next(self):
        next_pos = self._sharedmem.get_next_location()
        if (next_pos in self._list):
            return 'yes_'''+sensor_name+'''_next'
        else:
            return 'no_'''+sensor_name+'''_next'
    '''
    return code

def create_safe_sensor_from_points(points,polygons):
    concat_points = ''
    for i in range(len(points)):
        if i > 0:
            concat_points += ','
        concat_points += str(points[i])

    polygon_str = '['
    for i in range(len(polygons)):
        poly = polygons[i]
        polygon_str += '['
        for j in range(len(poly)):
            elem = poly[j]
            polygon_str += '[' + str(elem[0]) + ',' + str(elem[1]) + ']'
            if (j<len(poly)-1):
                polygon_str += ','
        polygon_str += ']'
        if i<len(polygons)-1:
            polygon_str += ',\n'
        else:
            polygon_str += ']\n'
    
    code = '''
from MAVProxy.modules.planner_module import Module

class SafeSensor(Module):
    def init(self):
	self._process_type = 0
	self._list = ['''+concat_points+''']
        self.controllables = ['is_safe_next']
        self.nofly_reg = ''' + str(polygon_str) + '''
        self._sharedmem.monitor.set_nofly(self.nofly_reg)
	return

    def is_safe_next(self):
        next_pos = self._sharedmem.get_next_location()
        if (next_pos not in self._list):
            return 'yes_safe_next'
        else:
            return 'no_safe_next'
    '''
    return code

