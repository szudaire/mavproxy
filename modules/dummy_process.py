import time
import sys

#Write to output stream, we can also use: print "START"
sys.stdout.write("START\n")
sys.stdout.flush() # <-- IMPORTANT: Doesn't work without flushing

while 1:
    #Read from input stream -> BLOCKS until '\n' is found
    string = sys.stdin.readline()

    #Write to output stream, equivalent to: print string[:-1]
    sys.stdout.write(string)
    sys.stdout.flush() # <-- IMPORTANT: Doesn't work without flushing
