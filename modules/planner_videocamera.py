from multiprocessing import Process
from multiprocessing import Value
from ctypes import c_bool
import time
#import cv2 as cv
import subprocess as sp
import time

class VideoCamera(Process):
        def __init__(self,file_name):
		super(VideoCamera,self).__init__()
                self.exit_code = Value(c_bool,False)
                self._file_name = file_name
                return

        def run(self):
                count = 0
                while (not self.exit_code.value):
                        count += 1
                        file_name = "output" + str(count) + ".avi"
                        time = 60 #seconds
                        sp.call(["python","/home/pi/Desktop/Planner/Run MAVProxy/camera.py",str(time),"/home/pi/Desktop/Planner/Run MAVProxy/" + file_name])
                        print("CAPTURED VIDEO " + str(count))

        def run2(self):
                video = cv.VideoCapture(0)
                fourcc = cv.cv.CV_FOURCC(*'XVID')
                out = cv.VideoWriter(self._file_name,fourcc, 5.0, (640,480))
                delta = 0.2
                
                ret, frame = video.read()
                out.write(frame)
                t1 = time.time()
                while(not self.exit_code.value):
                        t_actual = time.time()
                        if (t_actual > t1 + delta):
                                t1 = t1 + delta
                        else:
                                time.sleep(0.02)
                                continue

                        ret, frame = video.read()
                        out.write(frame)

                video.release()
                out.release()
                
        def exit_process(self):
                self.exit_code.value = True
                return
