from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.planner_module import Module
from multiprocessing import Value
from ctypes import c_bool
import subprocess as sp
import psutil as psu
import time
from os.path import expanduser
import numpy as np
import json
import time
from multiprocessing import Process,Value,Queue
from ctypes import c_bool
from MAVProxy.modules import planner_util_message as mesg

class SharedVariable:
    def __init__(self,name_list):
        self._vars = {}
        self._name_list = name_list
        for name in name_list:
            self._vars[name] = Value('d',0.0)
        self._reported = Value(c_bool,False)

    def reported(self):
        return self._reported.value

    def get_var(self,name):
        return self._vars[name].value

    def set_var(self,name,value):
        if (not self.reported()):
            self._reported.value = True
        self._vars[name].value = value

    def get_dictionary(self):
        variables = {}
        for name in self._name_list:
            variables[name] = self._vars[name].value
        return variables

class Monitor(Module):
    _wind = None
    _position = None
    def init(self):
        home = expanduser("~")
        path = home + '/.cache/mavproxy/bin/'
        self._log_path = home + '/.cache/mavproxy/logs/'

        self.hlolacounter = 0
        self.event_queue = Queue()
        #Config Module
        self._set_suscription_packets(["WIND", "GLOBAL_POSITION_INT", "ATTITUDE","VFR_HUD"])
        # uncomment for wind-only test
        # self._set_suscription_packets(["WIND"])
        self._process_type = 1
        #Start monitor process
        #Uses standard input and output pipes -> Equivalent to running in terminal: python PATH/dummy_process.py
        hlolastderr0 = open("/root/host/HLolaerr0.txt","w+")
        self.mon0 = sp.Popen([path + "HLola","tree","DFS"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr0)
        self.monitors = [self.mon0]
        self.process0 = psu.Process(self.mon0.pid)

        # comment for event-triggered reports
        #Threading report
        #t = threading.Timer(1.0, self._reportEvent)
        #t.start()

        self._tau_pred = None
        self._roll_pred = None
        self._wp_queue = Queue()
        self._radius_queue = Queue()
        self._num_queue = Queue()
        self._exit_bool = Value(c_bool,False)
        self._going_flag = Value(c_bool,False)
        self._wp_reached_queue = Queue()

        self._integral = 0
        self.target_vel = 21.0
        self._count_not_arrive = 0

        #Periodic reporting
        self._proc_report = Process(target=self._periodicReports)
        #Set shared memory with reporting process
        self._wind = SharedVariable(["direction","speed","speed_z"]) #Wind
        self._pos = SharedVariable(["lat","lon","x","y","zone","alt"]) #Pos in UTM of UAV
        self._att = SharedVariable(["pitch","roll","yaw"]) #Attitude in radians of UAV
        self._vel = SharedVariable(["x","y"]) #GPS speed in m/s of UAV
        self._vel_data = SharedVariable(["vel_a"]) #In m/s
        self._target = SharedVariable(["x","y","num_wp","mutex"]) #Target location in UTM
        self._tdir = SharedVariable(["x","y"]) #Target finish line direction
        self._poly_queue = Queue()
        self._matrix_queue = Queue()
        self._poly_queue_mon = Queue()
        self._wp_list_queue = Queue()
        self._radius_list_queue = Queue()
        self._tree_queue = Queue()
        self._interesting_queue = Queue()
        #Start periodic reporting
        self._period_report = 0.1

        #Auxiliary temporal
        self._takeoff_ended = Value("i",0)

        self.controllables = ["goto_loc"]

    def goto_loc(self):
        return
        
    def _started(self):
        #Start reading from output
        self._log_output = open(self._log_path + "mon_out.txt",'w+')
        self._count_out = 0
        self._add_to_command_queue("read_from_output")
        self._proc_report.start()

    def _exit(self):
        #Kill monitor process
        for mon in self.monitors:
            mon.kill()
            mon.wait()
        self._proc_report.terminate()
        self._proc_report.join()

    def _read_from_output(self):
        #Read from output of monitor -> BLOCKS until '\n' is reached
        output0 = self.mon0.stdout.readline()[:-1] #[:-1] removes '\n' at the end

        #Print output to console
        self._count_out += 1
        memory0 = self.process0.memory_info().rss
        self._log_output.write(str(self._count_out) + ',' + str(memory0) + ',' + str(output0) + '\n')

        output0 = json.loads(output0)

        outev = output0["goto"]
        if (outev != None):
            print("0",output0)
            if outev[0] < 0:
                self._add_to_event_queue('finished')
                self._add_to_event_queue('arrived')
            else:
                index = self._sharedmem.get_discretizer().get_loc_from_tree(outev[0],outev[1])
                dheight = self._sharedmem.get_dheight()
                tree_depth = self._sharedmem.tree_depth.value
                self._sharedmem.set_flight_height(dheight*(1+tree_depth-outev[0]))
            
                self._sharedmem.uav.go(index,using_iterator=True)

        #print("HLola memory: " + str(self.process0.memory_info().rss))  # in bytes 

        #Continue reading from output
        if not self._exit_bool.value:
            self._add_to_command_queue("read_from_output")
        
    def _write_to_input(self,data,mon):
        mon.stdin.write(data + "\n")
        mon.stdin.flush()

    def set_matrix_grid(self,matrix):
        strmatrix = ""
        w = matrix[:,0].max()+1
        h = matrix[:,1].max()+1

        for i in range(w):
            for j in range(h):
                if len(np.where((matrix[:,0] == i) & (matrix[:,1] == j))[0]) > 0:
                    strmatrix += "1"
                else:
                    strmatrix += "0"

        inimatrix = [[w,h],strmatrix]
        self._matrix_queue.put(inimatrix)

    def set_tree(self,tree):
        self._tree_queue.put(tree)

    def set_interesting(self,value):
        self._interesting_queue.put(value)

    def set_poly(self,poly):
        poly_tmp = []
        for elem in poly:
            if elem[0] != 0 and elem[1] != 0:
                zone,posx,posy = llutm.LLtoUTM(elem[1],elem[0])                
                poly_tmp.append([posx,posy])
            else:
                self._poly_queue.put(np.array(poly_tmp))
                poly_tmp = []

    def set_target(self,target,prev_target,wp_list,radius_list):
        zone,posx,posy = llutm.LLtoUTM(target[1],target[0])
        self._target.set_var("mutex",0) #Loading data
        self._target.set_var("x",posx)
        self._target.set_var("y",posy)
        self._target.set_var("num_wp",0)

        zone,prev_posx,prev_posy = llutm.LLtoUTM(prev_target[1],prev_target[0])

        #Define tdir to be perpendicular to the line connecting prev_target with target
        self._tdir.set_var("x",- (posy - prev_posy))
        self._tdir.set_var("y",posx - prev_posx)

        #Push into queues the lists
        self._wp_list_queue.put(wp_list)
        self._radius_list_queue.put(radius_list)
        self._target.set_var("mutex",1) #Finished loading data

        self._going_flag.value = True

    def set_wp_num(self,seq):
        self._target.set_var("num_wp",seq)

    def mavlink_packet(self,m):
        if (not self._started_flag):
            return
        mtype = m.get_type()
        if (mtype == "WIND"):
            self._wind.set_var("direction",30*np.pi/180)#np.pi/2 - np.round(m.direction/30)*30*np.pi/180)#30*np.pi/180)
            self._wind.set_var("speed",m.speed)
            self._wind.set_var("speed_z",m.speed_z)
          # uncomment for wind-only test, event-triggered reports
          # ev = {
          #     "wind": self._wind,
          #     "timestamp": int(time.time())
          # }
          # self._write_to_input(json.dumps(ev))
          # return
        elif (mtype == "GLOBAL_POSITION_INT"):
            zone,posx,posy = llutm.LLtoUTM(m.lat*0.0000001,m.lon*0.0000001)
            self._pos.set_var("lat",m.lat*0.0000001)
            self._pos.set_var("lon",m.lon*0.0000001)
            self._pos.set_var("x",posx)
            self._pos.set_var("y",posy)
	    self._pos.set_var("alt",m.relative_alt*0.001) # mm2meter conversion
            self._pos.set_var("zone",zone)
            
            self._vel.set_var("x",m.vy*0.01) #El vx de GLOBAL_POSITION_INT apunta en sentido norte y el vy en sentido este
            self._vel.set_var("y",m.vx*0.01)
        elif (mtype == "ATTITUDE"):
            self._att.set_var("roll",m.roll)
            self._att.set_var("pitch",m.pitch)
            self._att.set_var("yaw",m.yaw)
        elif (mtype == "VFR_HUD"):
            self._vel_data.set_var("vel_a",m.airspeed)
        # uncomment for event-triggered reports
        # self._reportEvent()
        #

    def _periodicReports(self):
        i = 0
        dt = self._period_report #seconds
        init_time = time.time()
        self._count_in = 0
        self._log_input = open(self._log_path + "mon_in.txt",'w+')
        while not self._exit_bool.value:
            try:
                self._reportEvent()
            except Exception:
                import traceback
                traceback.print_exc()
                break
            i = i + 1
            time.sleep(max(0.01,init_time + i*dt - time.time()))
        self._log_input.close()

    def _retrieve_lists(self):
        while (self._wp_list_queue.qsize() > 0):
            self._wp_list = self._wp_list_queue.get()
            for i in range(len(self._wp_list)):
                zone,x,y = llutm.LLtoUTM(self._wp_list[i,1],self._wp_list[i,0])
                self._wp_list[i,0] = x
                self._wp_list[i,1] = y
        while (self._radius_list_queue.qsize() > 0):
            self._radius_list = self._radius_list_queue.get()
    
    def _reportEvent(self):
        if (self._pos.reported()):
            
            events = [] #Eventos que ocurrieron desde el ultimo reporte
            while (self.event_queue.qsize() > 0):
                event = self.event_queue.get()
                events.append(event)

            if self._takeoff_ended.value == 0:
                if ("takeoff_ended" not in events):
                    return
                else:
                    self._takeoff_ended.value = 1

            if ("capture" in events):
                self._interesting_queue.put("Interesting")
            #while not (self._target.get_var("mutex") > 0):
                #time.sleep(0.01)
            
            #self._retrieve_lists()
            if self._tree_queue.qsize() > 0:
                tree = self._tree_queue.get()
            else:
                tree = [0,0]

            if self._interesting_queue.qsize() > 0:
                interesting = self._interesting_queue.get()
            else:
                interesting = "IDK"
            

            ev0 = {
                "initree": tree,
                "interestingness": interesting
            }
            
            self._count_in += 1
            self._log_input.write(str(self._count_in) + ',' + str(json.dumps(ev0)) + '\n')

            self._write_to_input(json.dumps(ev0),self.mon0)

            
            #print "Written to HLola ev number: " + str(self.hlolacounter)
            self.hlolacounter += 1

            return
