from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.planner_module import Module
from multiprocessing import Value,Queue
from ctypes import c_bool
import subprocess as sp
import time
from os.path import expanduser
import numpy as np
import json
import time
import psutil as psu
from multiprocessing import Process,Value,Queue
from ctypes import c_bool
from MAVProxy.modules import planner_util_message as mesg

class SharedVariable:
    def __init__(self,name_list):
        self._vars = {}
        self._name_list = name_list
        for name in name_list:
            self._vars[name] = Value('d',0.0)
        self._reported = Value(c_bool,False)

    def reported(self):
        return self._reported.value

    def get_var(self,name):
        return self._vars[name].value

    def set_var(self,name,value):
        if (not self.reported()):
            self._reported.value = True
        self._vars[name].value = value

    def get_dictionary(self):
        variables = {}
        for name in self._name_list:
            variables[name] = self._vars[name].value
        return variables
    
class Monitor(Module):
    _wind = None
    _position = None
    def init(self):
        home = expanduser("~")
        path = home + '/.cache/mavproxy/bin/'
        self._log_path = home + '/.cache/mavproxy/logs/'

        self.event_queue = Queue()
        self._nofly_queue = Queue()

        #Config Module
        self._set_suscription_packets(["WIND", "GLOBAL_POSITION_INT", "ATTITUDE","VFR_HUD"])
        # uncomment for wind-only test
        # self._set_suscription_packets(["WIND"])
        self._process_type = 1
        #Start monitor process
        #Uses standard input and output pipes -> Equivalent to running in terminal: python PATH/dummy_process.py
        hlolastderr = open("/root/host/HLolaerr.txt","w+")
        self.proc = sp.Popen([path + "HLola"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr)
        self.process = psu.Process(self.proc.pid)

        # comment for event-triggered reports
        #Threading report
        #t = threading.Timer(1.0, self._reportEvent)
        #t.start()

        self._tau_pred = None
        self._roll_pred = None

        #Periodic reporting
        self._proc_report = Process(target=self._periodicReports)
        #Set shared memory with reporting process
        self._param = SharedVariable(["NAVL1_D","NAVL1_P"])
        self._wind = SharedVariable(["direction","speed","speed_z"]) #Wind
        self._pos = SharedVariable(["x","y","zone","alt"]) #Pos in UTM of UAV
        self._att = SharedVariable(["pitch","roll","yaw"]) #Attitude in radians of UAV
        self._vel = SharedVariable(["x","y"]) #GPS speed in m/s of UAV
        self._vel_data = SharedVariable(["vel_a"]) #In m/s
        self._target = SharedVariable(["x","y","num_wp","mutex"]) #Target location in UTM
        self._tdir = SharedVariable(["x","y"]) #Target finish line direction
        self._wp_list_queue = Queue()
        self._radius_list_queue = Queue()
        #Start periodic reporting
        self._period_report = 0.2
        self._proc_report.start()

        self.controllables = []

    def set_params(self,NAVL1_D,NAVL1_P):
        self._param.set_var("NAVL1_D",NAVL1_D)
        self._param.set_var("NAVL1_P",NAVL1_P)

    def set_nofly(self,obstacles):
        nofly = []
        for i in range(len(obstacles)):
            auxvec = []
            for j in range(len(obstacles[i])):
                pos_gps = obstacles[i][j]
                zone,posx,posy = llutm.LLtoUTM(pos_gps[1],pos_gps[0])
                auxvec.append([posx,posy])
            nofly.append(auxvec)
	#print(nofly)
        self._nofly_queue.put(nofly)
        
    def _started(self):
        #Start reading from output
        self._log_output = open(self._log_path + "mon_out.txt",'w+')
        self._count_out = 0
        self._add_to_command_queue("read_from_output")

    def _exit(self):
        #Kill monitor process
        self.proc.kill()
        self.proc.wait()
        self._proc_report.terminate()
        self._proc_report.join()

    def _read_from_output(self):
        #Read from output of monitor -> BLOCKS until '\n' is reached
        output = self.proc.stdout.readline()[:-1] #[:-1] removes '\n' at the end

        #Print output to console
        self._count_out += 1
        memory = self.process.memory_info().rss
        self._log_output.write(str(self._count_out) + ',' + str(memory) + ',' + str(output) + '\n')

        output = json.loads(output)

        #Continue reading from output
        self._add_to_command_queue("read_from_output")

    def _send_list_to_ground_station(self,pos_list):
        zone = self._pos.get_var("zone")
        self._add_to_message_queue(mesg.EST_POS_LIST_START,str(len(pos_list)))
        for pos in pos_list:
            pos_lat,pos_lon = llutm.UTMtoLL(pos[1],pos[0],zone)
            self._add_to_message_queue(mesg.EST_POS_LIST_ELEM,str(pos_lat) + " " + str(pos_lon))
        self._add_to_message_queue(mesg.EST_POS_LIST_END,"")
        

    def _write_to_input(self,data):
        self.proc.stdin.write(data + "\n")
        self.proc.stdin.flush()

    def set_target(self,target,prev_target,wp_list,radius_list):
        zone,posx,posy = llutm.LLtoUTM(target[1],target[0])
        self._target.set_var("mutex",0) #Loading data
        self._target.set_var("x",posx)
        self._target.set_var("y",posy)
        self._target.set_var("num_wp",0)

        zone,prev_posx,prev_posy = llutm.LLtoUTM(prev_target[1],prev_target[0])

        #Define tdir to be perpendicular to the line connecting prev_target with target
        self._tdir.set_var("x",- (posy - prev_posy))
        self._tdir.set_var("y",posx - prev_posx)

        #Push into queues the lists
        self._wp_list_queue.put(wp_list)
        self._radius_list_queue.put(radius_list)
        self._target.set_var("mutex",1) #Finished loading data

    def set_wp_num(self,seq):
        self._target.set_var("num_wp",seq)

    def mavlink_packet(self,m):
        if (not self._started_flag):
            return
        mtype = m.get_type()
        if (mtype == "WIND"):
            self._wind.set_var("direction",m.direction)
            self._wind.set_var("speed",m.speed)
            self._wind.set_var("speed_z",m.speed_z)
        elif (mtype == "GLOBAL_POSITION_INT"):
            zone,posx,posy = llutm.LLtoUTM(m.lat*0.0000001,m.lon*0.0000001)
            self._pos.set_var("x",posx)
            self._pos.set_var("y",posy)
	    self._pos.set_var("alt",m.relative_alt*0.001) # mm2meter conversion
            self._pos.set_var("zone",zone)
            
            self._vel.set_var("x",m.vy*0.01) #El vx de GLOBAL_POSITION_INT apunta en sentido norte y el vy en sentido este
            self._vel.set_var("y",m.vx*0.01)
        elif (mtype == "ATTITUDE"):
            self._att.set_var("roll",m.roll)
            self._att.set_var("pitch",m.pitch)
            self._att.set_var("yaw",m.yaw)
        elif (mtype == "VFR_HUD"):
            self._vel_data.set_var("vel_a",m.airspeed)

    def _periodicReports(self):
        i = 0
        dt = self._period_report #seconds
        init_time = time.time()
        self._count_in = 0
        self._log_input = open(self._log_path + "mon_in.txt",'w+')
        while 1:
            self._reportEvent(init_time)
            i = i + 1
            if (init_time + i*dt - time.time() > 0):
                time.sleep(init_time + i*dt - time.time())

    def _retrieve_lists(self):
        while (self._wp_list_queue.qsize() > 0):
            self._wp_list = self._wp_list_queue.get()
            for i in range(len(self._wp_list)):
                zone,x,y = llutm.LLtoUTM(self._wp_list[i,1],self._wp_list[i,0])
                self._wp_list[i,0] = x
                self._wp_list[i,1] = y
        while (self._radius_list_queue.qsize() > 0):
            self._radius_list = self._radius_list_queue.get()
    
    def _reportEvent(self,init_time):
        while not (self._target.get_var("mutex") > 0):
            time.sleep(0.01)
            
        self._retrieve_lists()
        
        if (self._nofly_queue.qsize() > 0):
            self._nofly = self._nofly_queue.get() #poligonos de las regiones prohibidas
        
        #print(self._nofly)

        events = [] #Eventos que ocurrieron desde el ultimo reporte
        while (self.event_queue.qsize() > 0):
            events.append(self.event_queue.get())

        if len(events) > 0:
            pass
            #print(events)

        ev = {
            "wind": self._wind.get_dictionary(),
            "position": self._pos.get_dictionary(),
            "velocity": self._vel.get_dictionary(),
            "attitude": self._att.get_dictionary(),
            "target": self._target.get_dictionary(),
            "altitude": self._pos.get_var("alt"),
            "target_dir": self._tdir.get_dictionary(),
            "timestamp": int(time.time()),
            "events_within": events,
            "nofly": self._nofly
        }
        self._count_in += 1
        self._log_input.write(str(self._count_in) + ',' + str(json.dumps(ev)) + '\n')
        #print ev
        self._write_to_input(json.dumps(ev))
            
