from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from os.path import expanduser
import numpy as np

class NemoSensor(Module):
	def init(self):
		home = expanduser("~")
		self.path = home + '/.cache/mavproxy/captures/'

		self._process_type = 0

		self._prob_find = 0.05
		self._prob_lose = 0.8
		self._prob = self._prob_find
		return
	
	def add_locations(self,pos_locations):
		self._location_list = Array('i',range(len(pos_locations)))
		self._array_end = Value('i',0)

	def _add_to_array_end(self):
                self._array_end.value += 1
                
	def _substract_to_array_end(self):
                self._array_end.value -= 1

        def _get_array_end(self):
                return self._array_end.value

        def _add_to_list(self,value):
                self._location_list[self._get_array_end()] = value
                self._add_to_array_end()

        def _get_list(self):
                return self._location_list[0:self._get_array_end()]

        def _remove_from_list(self,value):
                array = np.array(self._get_list())
                ind = np.flatnonzero(array==value)[0]
                aux = self._location_list[ind+1:self._get_array_end()]
                self._substract_to_array_end()
                self._location_list[ind:self._get_array_end()] = aux
	
	def has_Nemo_current(self):
                current = self._sharedmem.get_current_location()
                if (np.random.rand() < self._prob):
                        self._prob = self._prob_lose
                        if (current not in self._get_list()):
                                self._add_to_list(current)
                        loc_current_pos = self._sharedmem.get_discretizer().get_position(current)
                        self._add_to_message_queue(mesg.FOUND,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                        return 'yes_Nemo_current'
                else:
                        if (current in self._get_list()):
                                self._remove_from_list(current)
                        self._prob = self._prob_find
                        return 'no_Nemo_current'

	def has_Nemo_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (next_pos in self._get_list()):
                        return 'yes_Nemo_next'
                else:
                        return 'no_Nemo_next'
