#!/bin/bash
# set -e # exit on error
modulepath="$(pwd)"
cd ~/.cache/mavproxy/bin
HLolaPath="$(pwd)"
HLolaBin="${HLolaPath}/HLola"

rm -rf ~/.cache/mavproxy
mkdir ~/.cache/mavproxy/{,cache,captures,lib,logs,automatas,bin,code}

cd "$modulepath"
cd ../../../hlola || exit 1
#cd ../../../hlola/
### STACK
#stack build --copy-bins --local-bin-path "${HLolaPath}" || exit 1

### CABAL
#cd ../../../hlola2 || exit 1
#cabal configure
#cabal build
cp dist/build/HLola/HLola "${HLolaBin}"
#cp dist/build/HLola_mon1/* "${HLolaPath}"
#${HLolaBin} --analyse || exit 1

#exit 1

cd "${modulepath}"
cp ./config_file.py ~/.cache/mavproxy/config_file.py
cp -r ./Automatas/* ~/.cache/mavproxy/automatas/
gcc -fpic -shared -o planner_clib_trajectory.so planner_clib_trajectory.c
gcc -fpic -shared -o planner_clib_poly.so planner_clib_poly.c
g++ -fpic -shared -o planner_clib_rrtsolver.so planner_clib_rrtsolver.cpp
cp ./planner_clib_rrtsolver.so ~/.cache/mavproxy/lib/planner_clib_rrtsolver.so
cp ./planner_clib_trajectory.so ~/.cache/mavproxy/lib/planner_clib_trajectory.so
cp ./planner_gpu_trajectory.cu ~/.cache/mavproxy/lib/planner_gpu_trajectory.cu
cp ./planner_clib_poly.so ~/.cache/mavproxy/lib/planner_clib_poly.so

# cp ./dummy_process.py ~/.cache/mavproxy/bin/dummy_process.py

cd ../../
python setup.py build install --user

#${HLolaBin} --analyse # print dotfile
