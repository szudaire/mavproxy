import numpy as np
import math

class Trajectory():
        def __init__(self):
                self.tol = 0.000001
                self.delta_ang = math.pi/4
                self.sentidos_C1_dir = np.array([1.0,1.0,-1.0,-1.0]) #1 es horario, 0 es antihorario
                self.sentidos_C2_dir = np.array([1.0,-1.0,1.0,-1.0])
                self.sentidos_C1_min = np.array([1.0,-1.0]) #1 es horario, -1 es antihorario
                return
        
        def set_parameters(self, pos_inicial, dir_inicial,radio,scale,auxwp,overwp,check_ang):
                self._scale = scale

                #Adjust pos_inicial with scale
                pos_inicial = pos_inicial*scale
                self.pos_inicial = pos_inicial
                #Direction doesn't depend on lat-lon so no need to scale
                self.dir_inicial = dir_inicial
                
                self.radio = radio
                C1_H,C1_AH = get_centros(pos_inicial,dir_inicial,radio)

                #Configure flags
                self.auxwp = auxwp
                self.overwp = overwp
                self.check_ang = check_ang

                #Direccionada
                self.centros_C1_dir = np.array([C1_H,C1_H,C1_AH,C1_AH])
                self.centros_C1_min = np.array([C1_H,C1_AH])
        
        def set_overwp_dist(self,value):
                self._overwp_dist = value
        
        def set_grid(self,grilla):
                #Scale the grid first
                self.grilla = grilla*self._scale
                self.suma_actual,self.pond_actual = self.calc_ponderacion_total(self.pos_inicial)
                
        def calcular_trayectoria_paralela_direccion_ponderada(self, pos_final, dir_final):
                suma,pond = self.calc_ponderacion(pos_final.copy())

                wp_list,dist,dir = self.calcular_trayectoria_paralela_direccion(pos_final, dir_final)
                
                pond = pond - self.pond_actual
                pond = self.saturate(0.9*pond,0.5*2*math.pi*self.radio)
                dist_pond = dist - pond
                #return wp_list,dist,dir,pond,dist_pond #For tests
                return wp_list,dist_pond,dir

        def saturate(self,value,limit):
                if (value > limit):
                        value = limit
                elif (value < -limit):
                        value = -limit
                return value
                        
        def calc_ponderacion(self,elem):
                elem = elem*self._scale
                suma = self.suma_actual - (elem - self.pos_inicial)
                pond = np.linalg.norm(suma,axis=0)
                return suma,pond

        def calc_ponderacion_total(self,elem):
                suma = np.sum(self.grilla-elem,axis=0)/len(self.grilla)
                pond = np.linalg.norm(suma,axis=0)
                return suma,pond
        
        def calcular_trayectoria_paralela_direccion(self,pos_final, dir_final):
                wp_list1,dist1 = self.calcular_trayectoria_direccionada_ang_check(pos_final, dir_final)
                wp_list2,dist2 = self.calcular_trayectoria_direccionada_ang_check(pos_final, -dir_final)
                
                if (dist1 > dist2):
                        return wp_list2,dist2,-dir_final
                
                return wp_list1,dist1,dir_final

        def calcular_trayectoria_direccionada_ang_check(self, pos_final, dir_final):
                disp_center = False
                self.pos_inicial_orig = self.pos_inicial.copy()
                self.centros_C1_dir_orig = self.centros_C1_dir.copy()
                
                wp_list,dist,disp_center = self.calcular_trayectoria_direccionada(pos_final, dir_final, disp_center)
                while (disp_center):
                        wp_list,dist,disp_center = self.calcular_trayectoria_direccionada(pos_final, dir_final, disp_center)
                
                #Return to original pos_inicial
                self.pos_inicial = self.pos_inicial_orig
                self.centros_C1_dir = self.centros_C1_dir_orig
                
                return wp_list,dist
        
        def calcular_trayectoria_direccionada(self, pos_final, dir_final, disp_center):
                #El eje latitud es el eje Y
                #El eje longitud es el eje X
                #Invertir los ejes de las ecuaciones

                #Scale end position
                pos_final = pos_final*self._scale
                radio = self.radio

                if (self.overwp):
                        pos_final_orig = pos_final.copy()
                        pos_final -= dir_final*self._overwp_dist
                
                #Correction when trying to fly to the same waypoint, so distance isn't zero
                if (np.linalg.norm(pos_final-self.pos_inicial) < self.tol):
                        pos_final = self.pos_inicial - dir_final*2*self.tol

                C2_H, C2_AH = get_centros(pos_final,dir_final,radio)

                #Predefined variables
                sentidos_C1 = self.sentidos_C1_dir
                sentidos_C2 = self.sentidos_C2_dir
                dir_inicial = self.dir_inicial

                #Variables that can change depending on disp_center
                centros_C1 = self.centros_C1_dir
                centros_C2 = np.array([C2_H,C2_AH,C2_H,C2_AH])
                centros = centros_C2 - centros_C1
                centros_mod = np.linalg.norm(centros,axis=1)
                centros_flag = (len(np.flatnonzero(centros_mod <= 2*radio)) > 0)

                if (disp_center == True):
                        ind_centros = np.flatnonzero(centros_mod <= 2*radio)
                        min_ind = ind_centros[np.argmax(centros_mod[ind_centros])]
                        
                        vec_A = -centros[min_ind]
                        vec_D = self.dir_inicial
                        dot_AD = np.dot(vec_A,vec_D)
                        value_V = 2*radio + self.tol
                        dist_b = - dot_AD + math.sqrt(dot_AD**2 + value_V**2 - centros_mod[min_ind]**2)
                        
                        #Modify variables according to new pos_inicial
                        self.pos_inicial += dir_inicial*dist_b
                        C1_H,C1_AH = get_centros(self.pos_inicial,dir_inicial,radio)
                        self.centros_C1_dir = np.array([C1_H,C1_H,C1_AH,C1_AH])
                        centros_C1 = self.centros_C1_dir
                        centros = centros_C2 - centros_C1
                        centros_mod = np.linalg.norm(centros,axis=1)
                        centros_flag = (len(np.flatnonzero(centros_mod <= 2*radio)) > 0)
                        #After this new pos_inicial there is no chance pos_final = pos_inicial because circles overlap
                
                pos_inicial = self.pos_inicial

                way1 = [[]]*4
                way2 = [[]]*4
                way_i = [[]]*4
                way_ind = [[]]*4
                distancia = [[]]*4
                if (self.check_ang):
                        distancia_ang = [[]]*4
                
                tita_1_list = [[]]*4
                tita_2_list = [[]]*4
                
                #Angulos caso 1
                tita = np.arctan2(-centros[:,0],centros[:,1])
                tita_1_list[0] = tita.copy()
                tita_2_list[0] = tita.copy()
                tita += math.pi
                tita_1_list[1] = tita.copy()
                tita_2_list[1] = tita.copy()
                
                #Angulos caso 2
                beta = np.arctan2(centros[:,1],centros[:,0])
                nonzero_ind = np.flatnonzero(centros_mod > 0.0)
                aux_cos = np.zeros(len(centros_mod)) + 2.0
                aux_cos[nonzero_ind] = 2*radio/centros_mod[nonzero_ind]
                alpha = np.array([20.0]*len(centros))
                valid_ind = np.flatnonzero(aux_cos <= 1.0)
                alpha[valid_ind] = np.arccos(aux_cos[valid_ind])
                tita_1 = -alpha[valid_ind] + beta[valid_ind]
                tita_2 = math.pi + tita_1
                tita_1_list[2] = tita_1.copy()
                tita_2_list[2] = tita_2.copy()
                alpha[valid_ind] = -alpha[valid_ind]
                tita_1 = -alpha[valid_ind] + beta[valid_ind]
                tita_2 = math.pi + tita_1
                tita_1_list[3] = tita_1.copy()
                tita_2_list[3] = tita_2.copy()

                counter = 0
                
                for i in range(len(tita_1_list)):
                        tita_1 = tita_1_list[i]
                        tita_2 = tita_2_list[i]

                        #After first two tita come the ones which can have invalid ind
                        if(i == 2):
                                centros_C1 = centros_C1[valid_ind]
                                centros_C2 = centros_C2[valid_ind]
                                sentidos_C1 = sentidos_C1[valid_ind]
                                sentidos_C2 = sentidos_C2[valid_ind]
                                
                        aux_way_1,aux_way_2 = get_waypoints_direccionada(tita_1,tita_2,centros_C1,centros_C2,radio)
                        indices = self.test_direccion_direccionada(aux_way_2 - aux_way_1, aux_way_1 - centros_C1, aux_way_2 - centros_C2,sentidos_C1,sentidos_C2,centros_C2-centros_C1)
                        if (len(indices) > 0):
                                dist_ang,dist_line = calcular_distancia_direccionada(centros_C1[indices],sentidos_C1[indices],dir_inicial,tita_1[indices],centros_C2[indices],sentidos_C2[indices],dir_final,tita_2[indices],aux_way_2[indices]-aux_way_1[indices],radio)
                                distancia[counter] = dist_ang + dist_line
                                if (self.check_ang):
                                        distancia_ang[counter] = dist_ang
                                if (self.auxwp == True):
                                        way1[counter] = aux_way_1[indices]
                                        way2[counter] = aux_way_2[indices]
                                        way_i[counter] = i
                                        way_ind[counter] = indices                   
                                counter += 1
                                
                        #Temp -> Eliminar cuando termine el testing
                        for j in range(len(tita_1)):
                                if (i<2):
                                        ind = j;
                                else:
                                        ind = valid_ind[j]
                                '''
                                print("python i="+str(ind)+" j="+str(i)+" tita1="+str(tita_1[j])+" tita2="+str(tita_2[j]))
                                if (j in indices):
                                        print(distancia[counter-1])
                                '''
                                
                                


                for i in range(counter):
                        min_1 = np.argmin(distancia[i])
                        distancia[i] = distancia[i][min_1]
                        if (self.check_ang):
                                distancia_ang[i] = distancia_ang[i][min_1]
                        if (self.auxwp == True):                        
                                way1[i] = way1[i][min_1]
                                way2[i] = way2[i][min_1]
                                way_ind[i] = way_ind[i][min_1]

                min_index = np.argmin(distancia[0:counter])
                
                if (self.check_ang and centros_flag):
                        if (distancia_ang[min_index] > 2*math.pi*radio + self.tol): #Check if self.tol is not too much
                                return None,None,True #Return True meaning the angular check came positive
                
                if (self.auxwp):        
                        i = way_i[min_index]
                        ind = way_ind[min_index]
                        tita1 = tita_1_list[i][ind]
                        tita2 = tita_2_list[i][ind]
                        if (i < 2):
                                centros_C1 = self.centros_C1_dir
                                sentidos_C1 = self.sentidos_C1_dir
                                centros_C2 = np.array([C2_H,C2_AH,C2_H,C2_AH])
                                sentidos_C2 = self.sentidos_C2_dir
                        centro1 = centros_C1[ind]
                        centro2 = centros_C2[ind]
                        sentido1 = sentidos_C1[ind]
                        sentido2 = sentidos_C2[ind]
                        
                        wps_initial,wps_end = self.get_aux_waypoints_direccionada(tita1,tita2,centro1,centro2,radio,sentido1,sentido2,dir_inicial,dir_final)
                        wp_list = [[]]*(len(wps_initial) + len(wps_end) + 3)
                        ind = 0
                        wp_list[ind:ind+len(wps_initial)] = wps_initial
                        ind += len(wps_initial)
                        wp_list[ind] = way1[min_index]
                        ind += 1
                        wp_list[ind] = way2[min_index]
                        ind += 1
                        wp_list[ind:ind + len(wps_end)] = wps_end
                        ind += len(wps_end)
                        wp_list[ind] = pos_final

                        if (self.overwp):
                                wp_list.append(pos_final_orig)
                        if (disp_center):
                                wp_list.insert(0,pos_inicial)
                        
                        wp_list = np.array(wp_list)
                        #Scale back wp list
                        wp_list = wp_list/self._scale
                        return wp_list,distancia[min_index],False
                
                return None,distancia[min_index],False
        
        def calcular_trayectoria_minima(self,pos_final):
                pos_final = pos_final*self._scale
                
                pos_inicial = self.pos_inicial
                dir_inicial = self.dir_inicial
                radio = self.radio

                if (np.linalg.norm(pos_final-self.pos_inicial) < self.tol):
                        pos_final = self.pos_inicial - dir_inicial*2*self.tol
                
                centros = self.centros_C1_min
                sentidos = self.sentidos_C1_min
                
                PC = pos_final - centros
                PC_mod = np.linalg.norm(PC,axis=1)
                
                way1 = [[]]*2
                way_ind = [[]]*2
                way_tita= [[]]*2
                distancia = [[]]*2
                dir = [[]]*2
                tita_list = [[]]*2
                
                beta = np.arctan2(PC[:,1],PC[:,0])
                nonzero_ind = np.flatnonzero(PC_mod > 0.0)
                aux_cos = np.zeros(len(PC_mod)) + 2.0
                aux_cos[nonzero_ind] = radio/PC_mod[nonzero_ind]
                alpha = np.array([20.0]*len(PC))
                valid_ind = np.flatnonzero(aux_cos <= 1.0)
                alpha[valid_ind] = np.arccos(aux_cos[valid_ind])
                tita = -alpha[valid_ind] + beta[valid_ind]
                tita_list[0] = tita.copy()
                alpha[valid_ind] = -alpha[valid_ind]
                tita = -alpha[valid_ind] + beta[valid_ind]
                tita_list[1] = tita.copy()
                
                centros = centros[valid_ind]
                sentidos = sentidos[valid_ind]
                
                counter = 0
                
                for tita in tita_list:
                        aux_way_1 = get_waypoints_minima(tita,centros,radio)

                        #Get end direction
                        dir_final = pos_final - aux_way_1
                        mod_dir = np.linalg.norm(dir_final,axis=1)
                        indices = np.flatnonzero(mod_dir < self.tol)
                        if (len(indices) > 0):
                                mod_dir[indices] = 1.0
                        dir_final = (dir_final.T/mod_dir).T
                        if (len(indices) > 0):
                                dir_final[indices] = np.array([np.sin(tita[indices]),-np.cos(tita[indices])]).T*sentidos[indices]

                        #Test correct direction
                        indices = self.test_direccion_minima(dir_final, aux_way_1 - centros,sentidos)
                        
                        if (len(indices) > 0):
                                distancia[counter] = calcular_distancia_minima(centros[indices],sentidos[indices],dir_inicial,tita[indices],pos_final - aux_way_1[indices],radio)
                                if (self.auxwp == True):
                                        way1[counter] = aux_way_1[indices]
                                        dir[counter] = dir_final[indices]
                                        way_ind[counter] = indices
                                        way_tita[counter] = tita
                                counter += 1

                for i in range(counter):
                        min_1 = np.argmin(distancia[i])
                        distancia[i] = distancia[i][min_1]
                        if (self.auxwp == True):
                                way1[i] = way1[i][min_1]
                                dir[i] = dir[i][min_1]
                                way_ind[i] = way_ind[i][min_1]
                
                min_index = np.argmin(distancia[0:counter])

                if (self.auxwp == True):
                        ind = way_ind[min_index]
                        tita = way_tita[min_index]
                        wp_list = self.get_aux_waypoints_minima(tita[ind],centros[ind],radio,sentidos[ind],dir_inicial)
                        wp_list.append(way1[min_index])
                        wp_list.append(pos_final)

                        wp_list = np.array(wp_list)
                        wp_list = wp_list/self._scale
                        return wp_list,distancia[min_index],dir[min_index]
                
                return None,distancia[min_index],None

        def test_direccion_direccionada(self,vec_way,vec_1,vec_2,sentido_1,sentido_2,centro_dif):
                #Con direccion de llegada
                resultado1 = np.cross(vec_way,vec_1)*sentido_1
                resultado2 = np.cross(vec_way,vec_2)*sentido_2
                
                indices = np.flatnonzero(np.linalg.norm(vec_way,axis=1) < self.tol)
                if (len(indices) > 0):
                        resultado1[indices] = -sentido_1[indices]*sentido_2[indices]
                        resultado2[indices] = -sentido_1[indices]*sentido_2[indices]
                        ind_centro = np.flatnonzero(np.linalg.norm(centro_dif[indices],axis=1) < self.tol)
                        if(len(ind_centro) > 0):
                                resultado1[indices[ind_centro]] *= -1
                                resultado2[indices[ind_centro]] *= -1
                
                return np.flatnonzero((resultado1 > 0) & (resultado2 > 0))

        def test_direccion_minima(self,vec_way,vec,sentido):
                #Sin direccion de llegada
                resultado = np.cross(vec_way,vec)*sentido
                return np.flatnonzero(resultado > 0)

        def get_aux_waypoints_direccionada(self,tita_1, tita_2, centros_C1, centros_C2, radio, sentidos_C1, sentidos_C2, dir_1, dir_2):
                #Con direccion de llegada
                tita_inicial = sentidos_C1*math.pi/2 + np.arctan2(dir_1[1],dir_1[0])
                tita_final = sentidos_C2*math.pi/2 + np.arctan2(dir_2[1],dir_2[0])

                wps_initial = []
                wps_end = []
                ang = self.delta_ang
                total_ang = wrap_angle(sentidos_C1*(- tita_1 + tita_inicial))
                while (ang < total_ang):
                        aux_tita = - sentidos_C1*ang + tita_inicial
                        wps_initial.append(centros_C1 + radio*np.array([np.cos(aux_tita),np.sin(aux_tita)]).T) 
                        ang += self.delta_ang

                ang = self.delta_ang
                total_ang = wrap_angle(sentidos_C2*(- tita_final + tita_2))
                while (ang < total_ang):
                        aux_tita = - sentidos_C2*ang + tita_2
                        wps_end.append(centros_C2 + radio*np.array([np.cos(aux_tita),np.sin(aux_tita)]).T) 
                        ang += self.delta_ang
                
                return wps_initial,wps_end
                
        def get_aux_waypoints_minima(self,tita, centros, radio, sentidos,dir):
                #Sin direccion de llegada
                tita_inicial = sentidos*math.pi/2 + np.arctan2(dir[1],dir[0])

                wp_list = []
                ang = self.delta_ang
                total_ang = wrap_angle(sentidos*(- tita + tita_inicial))
                
                while (ang < total_ang):
                        aux_tita = - sentidos*ang + tita_inicial
                        wp_list.append(centros + radio*np.array([np.cos(aux_tita),np.sin(aux_tita)]).T)
                        ang += self.delta_ang
                return wp_list

def get_centros(pos,dir,radio):
        dir_rad = np.array([dir[1],-dir[0]])
        
        C_H = pos + radio*dir_rad #Horario
        C_AH = pos - radio*dir_rad#Antihorario
        
        return C_H, C_AH

def calcular_distancia_direccionada(centros_C1,sentidos_C1,dir_1,tita_1,centros_C2,sentidos_C2,dir_2,tita_2,vec_way,radio):
        #Con direccion de llegada
        tita_inicial = sentidos_C1*math.pi/2 + np.arctan2(dir_1[1],dir_1[0])
        tita_final = sentidos_C2*math.pi/2 + np.arctan2(dir_2[1],dir_2[0])
        
        delta_inicial = wrap_angle(sentidos_C1*(- tita_1 + tita_inicial))
        delta_final = wrap_angle(sentidos_C2*(- tita_final + tita_2))
        
        return (delta_final + delta_inicial)*radio,np.linalg.norm(vec_way,axis=1)
        
def calcular_distancia_minima(centros,sentidos,dir,tita,vec_way,radio):
        #Sin direccion de llegada
        tita_inicial = sentidos*math.pi/2 + np.arctan2(dir[1],dir[0])
        delta_inicial = wrap_angle(sentidos*(- tita + tita_inicial))
        
        return delta_inicial*radio + np.linalg.norm(vec_way,axis=1)
        
def wrap_angle(angle):
        return (angle + 4*np.pi)%(2*np.pi)

def get_waypoints_direccionada(tita_1, tita_2, centros_C1, centros_C2, radio):
        #Con direccion de llegada
        way1 = centros_C1 + radio*np.array([np.cos(tita_1),np.sin(tita_1)]).T
        way2 = centros_C2 + radio*np.array([np.cos(tita_2),np.sin(tita_2)]).T
        return way1,way2
        
def get_waypoints_minima(tita,centros,radio):
        #Sin direccion de llegada
        way = centros + radio*np.array([np.cos(tita),np.sin(tita)]).T
        return way

