import numpy as np

def get_adjacence_matrix(grid):
    adj_matrix = []
    for i in range(len(grid)):
        indexes = np.where(np.linalg.norm(grid[i,:] - grid,axis=1) <= 1.5)
        indexes = np.delete(indexes[0],np.where(indexes[0] == i))
        adj_matrix.append(indexes)

    return adj_matrix

def wavefront_propagation(adj_mat,initial_pos):
    wave = np.zeros(len(adj_mat),np.int)
    num = 1
    wave[initial_pos] = num
    horizon = adj_mat[initial_pos]

    for i in range(1,len(adj_mat)):
        num += 1
        new_hor = []
        for elem in horizon:
            wave[elem] = num

        for elem in horizon:
            for adj in adj_mat[elem]:
                if (adj not in new_hor) and wave[adj] == 0:
                    new_hor.append(adj)

        horizon = new_hor

    return wave

def priorities(elems,wave):
    return elems[wave[elems].argsort()]

def search_path(adj_mat,initial_pos,goal_pos): #DFS
    wave = wavefront_propagation(adj_mat,goal_pos)
    tree = [priorities(adj_mat[initial_pos],wave)]
    index = 0
    path = [initial_pos]

    while index >= 0:
        #print(index)
        #print(tree,index)
        if len(tree[index]) == 0:
            index -= 1
            path = path[:-1]
            continue
        
        elem = tree[index][-1]
        tree[index] = tree[index][:-1]

        adj_elems = adj_mat[elem]
        filtered_adj = []
        for adj in adj_elems:
            if adj not in path:
                filtered_adj.append(adj)

        if len(filtered_adj) == 0: #Deadlock
            if len(path) == len(adj_mat) - 1:
                path.append(elem)
                #Finished
                break
            else:
                continue

        path.append(elem)
        filtered_adj = np.array(filtered_adj,np.int)
        tree.append(priorities(filtered_adj,wave))
        index += 1

    path = np.array(path,np.int)
    return path
    
        
        
