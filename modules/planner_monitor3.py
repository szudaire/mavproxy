from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.planner_module import Module
from multiprocessing import Value
from ctypes import c_bool
import subprocess as sp
import psutil as psu
import time
from os.path import expanduser
import numpy as np
import json
import time
from multiprocessing import Process,Value,Queue
from ctypes import c_bool
from MAVProxy.modules import planner_util_message as mesg

class SharedVariable:
    def __init__(self,name_list):
        self._vars = {}
        self._name_list = name_list
        for name in name_list:
            self._vars[name] = Value('d',0.0)
        self._reported = Value(c_bool,False)

    def reported(self):
        return self._reported.value

    def get_var(self,name):
        return self._vars[name].value

    def set_var(self,name,value):
        if (not self.reported()):
            self._reported.value = True
        self._vars[name].value = value

    def get_dictionary(self):
        variables = {}
        for name in self._name_list:
            variables[name] = self._vars[name].value
        return variables

class Monitor(Module):
    _wind = None
    _position = None
    def init(self):
        home = expanduser("~")
        path = home + '/.cache/mavproxy/bin/'
        self._log_path = home + '/.cache/mavproxy/logs/'

        self.hlolacounter = 0
        self.event_queue = Queue()
        #Config Module
        self._set_suscription_packets(["WIND", "GLOBAL_POSITION_INT", "ATTITUDE","VFR_HUD"])
        # uncomment for wind-only test
        # self._set_suscription_packets(["WIND"])
        self._process_type = 1
        #Start monitor process
        #Uses standard input and output pipes -> Equivalent to running in terminal: python PATH/dummy_process.py
        hlolastderr = open("/root/host/HLolaerr.txt","w+")
        self.proc = sp.Popen([path + "HLola"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr)
        self.process = psu.Process(self.proc.pid)

        # comment for event-triggered reports
        #Threading report
        #t = threading.Timer(1.0, self._reportEvent)
        #t.start()

        self._tau_pred = None
        self._roll_pred = None
        self._wp_queue = Queue()
        self._radius_queue = Queue()
        self._num_queue = Queue()
        self._exit_bool = Value(c_bool,False)
        self._going_flag = Value(c_bool,False)

        self._integral = 0
        self.target_vel = 21.0
        self._count_not_arrive = 0

        #Periodic reporting
        self._proc_report = Process(target=self._periodicReports)
        #Set shared memory with reporting process
        self._param = SharedVariable(["NAVL1_D","NAVL1_P"])
        self._wind = SharedVariable(["direction","speed","speed_z"]) #Wind
        self._pos = SharedVariable(["x","y","zone","alt"]) #Pos in UTM of UAV
        self._att = SharedVariable(["pitch","roll","yaw"]) #Attitude in radians of UAV
        self._vel = SharedVariable(["x","y"]) #GPS speed in m/s of UAV
        self._vel_data = SharedVariable(["vel_a"]) #In m/s
        self._target = SharedVariable(["x","y","num_wp","mutex"]) #Target location in UTM
        self._tdir = SharedVariable(["x","y"]) #Target finish line direction
        self._wp_list_queue = Queue()
        self._radius_list_queue = Queue()
        #Start periodic reporting
        self._period_report = 1
        self._proc_report.start()

        self.controllables = []

    def set_params(self,NAVL1_D,NAVL1_P):
        self._param.set_var("NAVL1_D",NAVL1_D)
        self._param.set_var("NAVL1_P",NAVL1_P)
        
    def _started(self):
        #Start reading from output
        self._log_output = open(self._log_path + "mon_out.txt",'w+')
        self._count_out = 0
        self._add_to_command_queue("read_from_output")

    def _exit(self):
        #Kill monitor process
        self.proc.kill()
        self.proc.wait()
        self._proc_report.terminate()
        self._proc_report.join()

    def _read_from_output(self):
        #Read from output of monitor -> BLOCKS until '\n' is reached
        output = self.proc.stdout.readline()[:-1] #[:-1] removes '\n' at the end

        #Print output to console
        self._count_out += 1
        memory = self.process.memory_info().rss
        self._log_output.write(str(self._count_out) + ',' + str(memory) + ',' + str(output) + '\n')

        output = json.loads(output)

        '''
        intersDist = output["intersDistance"]
        #self._add_to_message_queue(mesg.PRINT,"Estimated Distance: " + str(intersDist))
        if (intersDist >= 0):
            zone = self._pos.get_var("zone")
            
            posx = output["debug"]["Right"]["targInters"]["x"]
            posy = output["debug"]["Right"]["targInters"]["y"]
            pos_lat,pos_lon = llutm.UTMtoLL(posy,posx,zone)
            
            #Send estimated location to planner_viewer
            loc_list = []
            loc_list.append([posx,posy])
            loc_list.append([posx,posy])
            #loc_list.append([self._target.get_var("x"),self._target.get_var("y")])
            #self._send_list_to_ground_station(loc_list)
        else:
            #self._add_to_message_queue(mesg.EST_POS_LIST_START,"")
            #self._add_to_message_queue(mesg.EST_POS_LIST_END,"")
            pass
            '''

        pos_dic = output["simulated_guidance_wps"]
        positions = []
        for elem in pos_dic:
            positions.append([elem["x"],elem["y"]])

        wp_list = self._wp_queue.get()
        radius_list = self._radius_queue.get()
        num_wp = self._num_queue.get()

        if (self._going_flag.value):
            if (len(positions) == len(wp_list) - num_wp):
                self._count_not_arrive = 0
                dist = []
                for i in range(len(positions)):
                    dist.append(np.linalg.norm(positions[i] - wp_list[num_wp+i]))

                print("Max distance from ideal: " + str(max(dist)))
                if (max(dist) > 100):
                    self._add_to_event_queue("dangerous_drift")
                
                if (num_wp >= len(wp_list)/2): #Voy por la mitad de la trayectoria
                    if np.linalg.norm(positions[-1]-wp_list[-1]) < 1.5*radius_list[-1]:
                        print("Going to arrive close")
                    else:
                        print("Going to arrive far")
                        self._going_flag.value = False
                        self._sharedmem.set_arrived_dir_flag(False)
                        self._add_to_event_queue("arrived_far")
                        #self._sharedmem.uav.go_next()
                else:
                    #print("Todavia no se")
                    pass
                self._send_list_to_ground_station(positions)
                
            elif(num_wp < len(wp_list)):
                self._count_not_arrive += 1
                if (self._count_not_arrive == 3):
                    print("Not going to arrive")
                    self._add_to_event_queue("not_going_to_arrive")
                    self._going_flag.value = False
                    #self._add_to_message_queue(mesg.PRINT,"Not going to arrive: Entering RTL")
                    #self._sharedmem.uav.rtl()
                    #self._exit_bool.value = True
            else:
                #print("Border case")
                pass

        kal_vec = output["kalman_filter_update"]
        roll_filt = kal_vec[1]
        tau_filt = kal_vec[2]
        #print "HLola Values: Roll: " + str(np.rad2deg(roll_filt)) + " Tau: " + str(tau_filt)

        #SET SPEED en funcion del valor de target_vel calculado
        target_vel = output["set_speed"]

        print("HLola memory: " + str(self.process.memory_info().rss))  # in bytes 

        #Continue reading from output
        if not self._exit_bool.value:
            self._add_to_mavproxy_command_queue("setspeed " + str(target_vel))
            self._add_to_command_queue("read_from_output")
        
    def _send_list_to_ground_station(self,pos_list):
        zone = self._pos.get_var("zone")
        self._add_to_message_queue(mesg.EST_POS_LIST_START,str(len(pos_list)))
        for pos in pos_list:
            pos_lat,pos_lon = llutm.UTMtoLL(pos[1],pos[0],zone)
            self._add_to_message_queue(mesg.EST_POS_LIST_ELEM,str(pos_lat) + " " + str(pos_lon))
        self._add_to_message_queue(mesg.EST_POS_LIST_END,"")
        
    def _write_to_input(self,data):
        self.proc.stdin.write(data + "\n")
        self.proc.stdin.flush()

    def set_target(self,target,prev_target,wp_list,radius_list):
        zone,posx,posy = llutm.LLtoUTM(target[1],target[0])
        self._target.set_var("mutex",0) #Loading data
        self._target.set_var("x",posx)
        self._target.set_var("y",posy)
        self._target.set_var("num_wp",0)

        zone,prev_posx,prev_posy = llutm.LLtoUTM(prev_target[1],prev_target[0])

        #Define tdir to be perpendicular to the line connecting prev_target with target
        self._tdir.set_var("x",- (posy - prev_posy))
        self._tdir.set_var("y",posx - prev_posx)

        #Push into queues the lists
        self._wp_list_queue.put(wp_list)
        self._radius_list_queue.put(radius_list)
        self._target.set_var("mutex",1) #Finished loading data

        self._going_flag.value = True

    def set_wp_num(self,seq):
        self._target.set_var("num_wp",seq)

    def mavlink_packet(self,m):
        if (not self._started_flag):
            return
        mtype = m.get_type()
        if (mtype == "WIND"):
            self._wind.set_var("direction",m.direction)
            self._wind.set_var("speed",m.speed)
            self._wind.set_var("speed_z",m.speed_z)
          # uncomment for wind-only test, event-triggered reports
          # ev = {
          #     "wind": self._wind,
          #     "timestamp": int(time.time())
          # }
          # self._write_to_input(json.dumps(ev))
          # return
        elif (mtype == "GLOBAL_POSITION_INT"):
            zone,posx,posy = llutm.LLtoUTM(m.lat*0.0000001,m.lon*0.0000001)
            self._pos.set_var("x",posx)
            self._pos.set_var("y",posy)
	    self._pos.set_var("alt",m.relative_alt*0.001) # mm2meter conversion
            self._pos.set_var("zone",zone)
            
            self._vel.set_var("x",m.vy*0.01) #El vx de GLOBAL_POSITION_INT apunta en sentido norte y el vy en sentido este
            self._vel.set_var("y",m.vx*0.01)
        elif (mtype == "ATTITUDE"):
            self._att.set_var("roll",m.roll)
            self._att.set_var("pitch",m.pitch)
            self._att.set_var("yaw",m.yaw)
        elif (mtype == "VFR_HUD"):
            self._vel_data.set_var("vel_a",m.airspeed)
        # uncomment for event-triggered reports
        # self._reportEvent()
        #

    def _periodicReports(self):
        i = 0
        dt = self._period_report #seconds
        init_time = time.time()
        self._count_in = 0
        self._log_input = open(self._log_path + "mon_in.txt",'w+')
        while not self._exit_bool.value:
            self._reportEvent()
            i = i + 1
            time.sleep(max(0.01,init_time + i*dt - time.time()))

    def _retrieve_lists(self):
        while (self._wp_list_queue.qsize() > 0):
            self._wp_list = self._wp_list_queue.get()
            for i in range(len(self._wp_list)):
                zone,x,y = llutm.LLtoUTM(self._wp_list[i,1],self._wp_list[i,0])
                self._wp_list[i,0] = x
                self._wp_list[i,1] = y
        while (self._radius_list_queue.qsize() > 0):
            self._radius_list = self._radius_list_queue.get()
    
    def _reportEvent(self):
        if (self._wind.reported() and self._pos.reported() and self._vel.reported() and self._target.reported() and self._tdir.reported() and self._att.reported() and self._param.reported() and self._vel_data.reported()):
            while not (self._target.get_var("mutex") > 0):
                time.sleep(0.01)
            
            self._retrieve_lists()
            
            events = [] #Eventos que ocurrieron desde el ultimo reporte
            while (self.event_queue.qsize() > 0):
                events.append(self.event_queue.get())

            def listToPoint(ls):
              return {"x": ls[0], "y": ls[1]}

            wp_list = self._wp_list
            initial_pos = np.array([self._pos.get_var("x"),self._pos.get_var("y")],np.float)
            initial_yaw = self._att.get_var("yaw")
            initial_roll = self._att.get_var("roll")
            radius_list = self._radius_list
            NAVL1_D = self._param.get_var("NAVL1_D")
            NAVL1_P = self._param.get_var("NAVL1_P")
            num_wp = int(self._target.get_var("num_wp"))

            if (num_wp >= len(wp_list)-1):
                num_wp = len(wp_list) - 1 

            vel_a = self._vel_data.get_var("vel_a") #around 21.0 #m/s
            vel_w = self._wind.get_var("speed")
            gamma = np.deg2rad(self._wind.get_var("direction")) + np.pi #Para que mire en el sentido contrario

            ev = {
                "wind": self._wind.get_dictionary(),
                "position": self._pos.get_dictionary(),
                "velocity": self._vel.get_dictionary(),
                "target": self._target.get_dictionary(),
                "target_dir": self._tdir.get_dictionary(),
                "timestamp": int(time.time()),
                "nonlineardata": {
                    "wp_list": map(listToPoint,wp_list)
                    ,"radius_list": radius_list.tolist()
                    ,"initial_pos": {
                        "x": self._pos.get_var("x")
                        ,"y": self._pos.get_var("y")
                    }
                    ,"initial_yaw": initial_yaw
                    ,"navl1_d" : NAVL1_D
                    ,"navl1_p" : NAVL1_P
                    ,"num_wp" : num_wp
                    ,"vel_a" : vel_a
                    ,"vel_w" : vel_w
                    ,"gamma" : gamma
                    ,"initial_roll" : initial_roll
                }
            }
            # self._add_to_print_queue(json.dumps(ev))
            self._wp_queue.put(wp_list)
            self._radius_queue.put(radius_list)
            self._num_queue.put(num_wp)

            target_vel = 0.0
            ev["taginfo"] = {"target_vel": target_vel, "evcount": self.hlolacounter}
            self._count_in += 1
            self._log_input.write(str(self._count_in) + ',' + str(json.dumps(ev)) + '\n')
            self._write_to_input(json.dumps(ev))
            # print "THEEV: " + json.dumps(ev)
            print "Written to HLola ev number: " + str(self.hlolacounter)
            self.hlolacounter += 1

            return
                
            ##### ACA COMIENZA LA PARTE DE ESTIMACION CON MODELO NO LINEAL #####
            ### Los waypoints estan almacenados en : self._wp_list
            ### Los radios de aceptacion estan en  : self._radius_list
            #self._add_to_print_queue("wp_list: " + str(wp_list))
            #self._add_to_print_queue("radius_list: " + str(self._radius_list))
            dt = 0.05 #Paso del tiempo

            print "Vel_w: " + str(vel_w) + " Gamma: " + str(np.rad2deg(gamma))

            if (self._tau_pred == None):
                self._tau_pred = 0.5
                self._roll_pred = initial_roll
                self._prev_roll = initial_roll
                self._prev_tau = self._tau_pred
                self._pmat = np.array([[0.01,0],[0,0.01]],np.float)
                
            self._pmat,roll_filt,tau_filt = self._kalman_filter_update(self._pmat,self._prev_roll,self._prev_tau,initial_roll,self._roll_pred,self._tau_pred,dt)

            #Predict states for the next kalman filter update
            self._prev_tau = tau_filt
            self._prev_roll = initial_roll
            self._roll_pred,self._tau_pred = self._kalman_filter_predict(initial_pos,initial_yaw,initial_roll,wp_list,radius_list,NAVL1_D,NAVL1_P,vel_w,gamma,vel_a,num_wp,dt,tau_filt)

            maxtime = 100 #seconds --> 1 min 40 sec
            if (num_wp < len(wp_list)-1):
                position_list,roll,reached_maxtime,total_time,total_dist = simulate_guidance_logic(initial_pos,initial_yaw,initial_roll,wp_list,radius_list,NAVL1_D,NAVL1_P,vel_w,gamma,vel_a,num_wp,dt,maxtime,tau_filt)
                print "Reached maxtime: " + str(reached_maxtime)
                if (len(position_list) > 0):
                    position_list.append(position_list[-1])
                #self._send_list_to_ground_station(position_list)


                ########## AGREGADO PARA CONTROL DE VELOCIDAD ##########
                if (total_time <> 0):
                    vel_a = 21.0
                    vel_min = 12
                    vel_max = 30
                    prop_gain = 0.5
                    int_gain = 0.03
                    int_max = 2.0

                    error = vel_a - total_dist/total_time

                    if not (self.target_vel == vel_min or self.target_vel == vel_max): #Solo integrar si saturo el target_vel, se conoce este tipo de tecnicas como anti-windup
                        self._integral += int_gain*error
                        self._integral = np.clip(self._integral,-int_max,int_max) #Saturar el valor de la accion integral

                    target_vel = error*prop_gain + vel_a + self._integral
                    target_vel = np.clip(target_vel,vel_min,vel_max)
                    self.target_vel = target_vel

                #SET SPEED en funcion del valor de target_vel calculado
                self._add_to_mavproxy_command_queue("setspeed " + str(self.target_vel))
                ev["taginfo"] = {"target_vel": target_vel, "evcount": self.hlolacounter}
                self._write_to_input(json.dumps(ev))
                # print "THEEV: " + json.dumps(ev)
                print "Written to HLola ev number: " + str(self.hlolacounter)
                self.hlolacounter += 1
                
                #print total_dist/total_time,target_vel,self._integral
            
    

    def _kalman_filter_update(self,pmat,prev_roll,prev_tau,initial_roll,roll_pred,tau_pred,dt):
        #print "Roll value: " + str(np.rad2deg(initial_roll)) + " Roll pred: " + str(np.rad2deg(roll_pred))

        F = np.array([[prev_tau/(prev_tau + dt),dt/((prev_tau+dt)**2)],[0,1]],np.float)
        q_roll = 0.01
        q_tau = 0.00001
        r_roll = 0.001
        Q = np.array([[q_roll,0],[0,q_tau]],np.float)
        H = np.array([[1,0]],np.float)
        R = np.array([[r_roll]],np.float)
        I = np.array([[1,0],[0,1]],np.float)

        x = np.array([[roll_pred],[tau_pred]],np.float)
        z = np.array([[initial_roll]],np.float)
        pmat_aux = F.dot(pmat).dot(F.T) + Q
        y = z - H.dot(x)
        S = H.dot(pmat_aux).dot(H.T) + R
        K = pmat_aux.dot(H.T).dot(np.linalg.inv(S))
        x_new = x + K.dot(y)
        pmat = (I - K.dot(H)).dot(pmat_aux)

        roll_filt = x_new[0,0]
        tau_filt = x_new[1,0]
        tau_min = 0.01
        if tau_filt < tau_min:
            tau_filt = tau_min
        #print "Roll filt: " + str(np.rad2deg(roll_filt)) + " Tau filt: " + str(tau_filt)
        return pmat,roll_filt,tau_filt
    
    def _kalman_filter_predict(self,initial_pos,initial_yaw,initial_roll,wp_list,radius_list,NAVL1_D,NAVL1_P,vel_w,gamma,vel_a,num_wp,dt,tau):
        maxtime = self._period_report*0.999
        if (num_wp < len(wp_list)-1):
            position_list,roll,reached_maxtime,total_time,total_dist = simulate_guidance_logic(initial_pos,initial_yaw,initial_roll,wp_list,radius_list,NAVL1_D,NAVL1_P,vel_w,gamma,vel_a,num_wp,dt,maxtime,tau)
            if (reached_maxtime):
                return roll,tau

        #Unable to make prediction <-- no update will happen
        return initial_roll,tau

        

###### ACA COMIENZA LA FUNCION DE ESTIMACION CON MODELO NO LINEAL DEL UAV ######

#Variables for the waypoint guidance logic
REACHED = 0
PASSED = 1
INTRAVEL = 2

#Non lineal model for the UAV
#### Arguments ####
# vel_g : Groundspeed in m/s
# vel_w : Wind speed in m/s
# gamma : Wind angle in radians
# vel_a : Airspeed of the UAV in m/s
# yaw   : UAV current yaw angle in radians
# posx  : UAV current position in X (UTM)
# posy  : UAV current position in Y (UTM)
# L1    : L1 length from the guidance controller
# K_L1  : K_L1 gain from the guidance controller
# eta   : eta angle from the guidance controller
# dt    : Time step
#### Returns ####
# yaw_next  : Next value for the yaw angle in radians
# posx_next : Next value for the UAV's x position (UTM)
# posy_next : Next value for the UAV's y position (UTM)
def advance_one_step(vel_g,vel_w,gamma,vel_a,yaw,roll,posx,posy,L1,K_L1,eta,dt,tau):
    g = 9.8
    LIM_ROLL_CD = np.deg2rad(65)
    limit = np.sin(LIM_ROLL_CD)
    
    #Non lineal model derivatives
    demanded_roll = np.arcsin(np.clip(( K_L1 * vel_g * vel_g * np.sin(eta) ) / (L1 * g),-limit,limit))
    roll_next = (dt*demanded_roll + tau*roll) / (tau + dt)
    yaw_dot = g * np.sin(roll) / vel_a 
    
    posx_dot = vel_a * np.sin(yaw) + vel_w * np.sin(gamma)
    posy_dot = vel_a * np.cos(yaw) + vel_w * np.cos(gamma)

    #Explicit Euler numerical solution
    yaw_next = yaw + yaw_dot*dt
    posx_next = posx + posx_dot*dt
    posy_next = posy + posy_dot*dt

    #print "Demanded: " + str(np.rad2deg(demanded_roll)) + " Actual: " + str(np.rad2deg(roll))
    return yaw_next,roll_next,posx_next,posy_next

#Get guidance control parameters
#### Arguments ####
# NAVL1_D : Guidance parameter (damping)
# NAVL1_P : Guidance parameter (period)
# vel_g   : Groundspeed of the UAV in m/s
# hdg     : UAV current heading angle in radians
# posx    : UAV current position in X (UTM)
# posy    : UAV current position in Y (UTM)
# prev_wp : UAV current previous waypoint (UTM) (2d vec)
# next_wp : UAV current next waypoint (UTM) (2d vec)
#### Returns ####
# L1   : L1 length from the guidance controller
# K_L1   : K_L1 gain from the guidance controller
# eta   : eta angle from the guidance controller
def guidance_control_parameters(NAVL1_D,NAVL1_P,vel_g,hdg,posx,posy,prev_wp,next_wp,verbose = False):
    L1 = NAVL1_D * NAVL1_P * vel_g / np.pi
    K_L1 = 4.0 * NAVL1_D * NAVL1_D
    
    #Waypoint A: prev_wp
    #Waypoint B: next_wp
    #Position U: current UAV position
    pos_A = prev_wp
    pos_B = next_wp
    pos_U = np.array([posx,posy],np.float)
    #Vector representing the groundspeed
    vec_gs = np.array([vel_g*np.sin(hdg),vel_g*np.cos(hdg)],np.float)
    
    vec_UA = pos_A - pos_U
    vec_AB = pos_B - pos_A
    mod_UA = np.linalg.norm(vec_UA)
    ang_prev = angle_between_vectors(vec_AB,vec_UA)

    region_I = (mod_UA >= L1 and ang_prev >= -np.pi/4 and ang_prev <= np.pi/4)

    if region_I:
        #eta will be the angle between vec_UA and the groundspeed
        eta = - angle_between_vectors(vec_gs,vec_UA) #El signo menos es para que la aceleracion lateral (a_s) despues de con el signo correcto
        if verbose:
            print "REGION I -> Eta: " + str(np.rad2deg(eta))
    else:
        #Normal L1 guidance logic
        crosstrack = np.cross(-vec_UA,vec_AB) / np.linalg.norm(vec_AB)
        sin_eta1 = crosstrack/L1

        sin_45 = np.sin(np.pi/4)
        eta1 = np.arcsin(np.clip(sin_eta1,-sin_45,sin_45)) #Saturate between -45 and 45 degrees
        eta2 = angle_between_vectors(vec_gs,vec_AB)
        if verbose:
            print "CROSSTRACK: " + str(crosstrack)
            print "Eta1: " + str(np.rad2deg(eta1))
            print "Eta2: " + str(np.rad2deg(eta2))
        
        eta = - (eta1 + eta2) #El signo menos es para que la aceleracion lateral (a_s) despues de con el signo correcto

    return L1,K_L1,eta

# Verify if we reached or passed the waypoint
#### Arguments ####
# posx      : UAV current position in X (UTM)
# posy      : UAV current position in Y (UTM)
# prev_wp   : UAV current previous waypoint (UTM) (2d vec)
# next_wp   : UAV current next waypoint (UTM) (2d vec)
# wp_radius : Acceptance radius for the next waypoint
#### Returns ####
# REACHED, PASSED or INTRAVEL to determine if a next_wp must be selected
def verify_next_waypoint(posx,posy,prev_wp,next_wp,wp_radius):
    pos_A = prev_wp
    pos_B = next_wp
    pos_U = np.array([posx,posy],np.float)
    
    vec_UB = pos_B - pos_U

    if (np.linalg.norm(vec_UB) <= wp_radius):
        return REACHED #Reached waypoint correctly
    
    vec_AB = next_wp - prev_wp
    mod_AB = np.linalg.norm(vec_AB)

    vec_UA = pos_A - pos_U
    alongtrack = np.dot(-vec_UA,vec_AB) / mod_AB

    if (alongtrack >= mod_AB):
        return PASSED #Entered region III, where the UAV passed the Finish Line

    return INTRAVEL

def get_ground_speed(vel_w,gamma,vel_a,yaw):
    vec_w = np.array([vel_w*np.sin(gamma),vel_w*np.cos(gamma)],np.float)
    vec_a = np.array([vel_a*np.sin(yaw),vel_a*np.cos(yaw)],np.float)

    vec_g = vec_a + vec_w
    vel_g = np.linalg.norm(vec_g)
    hdg = np.arctan2(vec_g[0],vec_g[1])

    return vel_g,hdg

def simulate_guidance_logic(initial_pos,initial_yaw,initial_roll,wp_list,radius_list,NAVL1_D,NAVL1_P,vel_w,gamma,vel_a,num_wp,dt,maxtime,tau,verbose = False):
    position_list = []

    posx = initial_pos[0]
    posy = initial_pos[1]
    yaw = initial_yaw
    roll = initial_roll
    next_wp = wp_list[num_wp]
    wp_radius = radius_list[num_wp]

    total_time = 0
    total_dist = 0
    reached_maxtime = False
    if (num_wp == 0):
        while (verify_next_waypoint(posx,posy,initial_pos,next_wp,wp_radius) == INTRAVEL):
            #No crosstrack algorithm (suppose current position as previous waypoint)
            prev_wp = np.array([posx,posy],np.float)
            
            vel_g,hdg = get_ground_speed(vel_w,gamma,vel_a,yaw)
            L1,K_L1,eta = guidance_control_parameters(NAVL1_D,NAVL1_P,vel_g,hdg,posx,posy,prev_wp,next_wp)

            #Update yaw and position
            prev_pos = np.array([posx,posy],np.float)
            yaw,roll,posx,posy = advance_one_step(vel_g,vel_w,gamma,vel_a,yaw,roll,posx,posy,L1,K_L1,eta,dt,tau)
            total_time += dt
            total_dist += np.linalg.norm(prev_pos - np.array([posx,posy],np.float))

            if (maxtime > 0 and total_time >= maxtime):
                position_list.append([posx,posy])
                reached_maxtime = True
                return position_list,roll,reached_maxtime,total_time,total_dist
        position_list.append([posx,posy])

        if verbose:
            print "Waypoint Number: 0"
            print verify_next_waypoint(posx,posy,initial_pos,next_wp,wp_radius)
            print np.rad2deg(initial_yaw), initial_pos
            print np.rad2deg(yaw),posx,posy
    
    for i in range(num_wp+1,len(wp_list)):
        prev_wp = wp_list[i-1]
        next_wp = wp_list[i]
        if (np.linalg.norm(next_wp - prev_wp) < 0.01):
            continue #Skip pairs of equal waypoints
        wp_radius = radius_list[i]

        iter = 0
        while (verify_next_waypoint(posx,posy,prev_wp,next_wp,wp_radius) == INTRAVEL):
            vel_g,hdg = get_ground_speed(vel_w,gamma,vel_a,yaw)
            L1,K_L1,eta = guidance_control_parameters(NAVL1_D,NAVL1_P,vel_g,hdg,posx,posy,prev_wp,next_wp)

            #Update yaw and position
            prev_pos = np.array([posx,posy],np.float)
            yaw,roll,posx,posy = advance_one_step(vel_g,vel_w,gamma,vel_a,yaw,roll,posx,posy,L1,K_L1,eta,dt,tau)
            #print np.rad2deg(yaw),posx,posy
            total_time += dt
            total_dist += np.linalg.norm(prev_pos - np.array([posx,posy],np.float))

            if (maxtime > 0 and total_time >= maxtime):
                position_list.append([posx,posy])
                reached_maxtime = True
                return position_list,roll,reached_maxtime,total_time,total_dist

        if verbose:
            print "Waypoint Number: " + str(i)
            print verify_next_waypoint(posx,posy,prev_wp,next_wp,wp_radius)
            print np.rad2deg(yaw),posx,posy
        position_list.append([posx,posy])

    return position_list,roll,reached_maxtime,total_time,total_dist

#Function to get angle between vectors in radians
def angle_between_vectors(vec1,vec2):
    cosang = np.dot(vec1, vec2)
    sinang = np.cross(vec1, vec2)
    return np.arctan2(sinang, cosang)

























        
