from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.planner_module import Module
from multiprocessing import Value
from ctypes import c_bool
import subprocess as sp
import psutil as psu
import time
from os.path import expanduser
import numpy as np
import json
import time
from multiprocessing import Process,Value,Queue
from ctypes import c_bool
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules import planner_util_wavefront as lwave

class SharedVariable:
    def __init__(self,name_list):
        self._vars = {}
        self._name_list = name_list
        for name in name_list:
            self._vars[name] = Value('d',0.0)
        self._reported = Value(c_bool,False)

    def reported(self):
        return self._reported.value

    def get_var(self,name):
        return self._vars[name].value

    def set_var(self,name,value):
        if (not self.reported()):
            self._reported.value = True
        self._vars[name].value = value

    def get_dictionary(self):
        variables = {}
        for name in self._name_list:
            variables[name] = self._vars[name].value
        return variables

class Monitor(Module):
    _wind = None
    _position = None
    def init(self):
        home = expanduser("~")
        path = home + '/.cache/mavproxy/bin/'
        self._log_path = home + '/.cache/mavproxy/logs/'

        self.hlolacounter = 0
        self.event_queue = Queue()
        #Config Module
        self._set_suscription_packets(["WIND", "GLOBAL_POSITION_INT", "ATTITUDE","VFR_HUD"])
        # uncomment for wind-only test
        # self._set_suscription_packets(["WIND"])
        self._process_type = 1
        #Start monitor process
        #Uses standard input and output pipes -> Equivalent to running in terminal: python PATH/dummy_process.py
        hlolastderr1 = open("/root/host/HLolaerr1.txt","w+")
        hlolastderr2 = open("/root/host/HLolaerr2.txt","w+")
        hlolastderr3 = open("/root/host/HLolaerr3.txt","w+")
        self.mon1 = sp.Popen([path + "HLola","patrolgive","mon1"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr1)
        self.mon2 = sp.Popen([path + "HLola","patrolgive","mon2"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr2)
        self.mon3 = sp.Popen([path + "HLola","patrolgive","mon3"],stdin=sp.PIPE,stdout=sp.PIPE,stderr=hlolastderr3)
        self.monitors = [self.mon1,self.mon2,self.mon3]
        self.process1 = psu.Process(self.mon1.pid)
        self.process2 = psu.Process(self.mon2.pid)
        self.process3 = psu.Process(self.mon3.pid)

        # comment for event-triggered reports
        #Threading report
        #t = threading.Timer(1.0, self._reportEvent)
        #t.start()

        self._tau_pred = None
        self._roll_pred = None
        self._wp_queue = Queue()
        self._radius_queue = Queue()
        self._num_queue = Queue()
        self._exit_bool = Value(c_bool,False)
        self._going_flag = Value(c_bool,False)
        self._wp_reached_queue = Queue()

        self._integral = 0
        self.target_vel = 21.0
        self._count_not_arrive = 0

        #Periodic reporting
        self._proc_report = Process(target=self._periodicReports)
        #Set shared memory with reporting process
        self._wind = SharedVariable(["direction","speed","speed_z"]) #Wind
        self._pos = SharedVariable(["lat","lon","x","y","zone","alt"]) #Pos in UTM of UAV
        self._att = SharedVariable(["pitch","roll","yaw"]) #Attitude in radians of UAV
        self._vel = SharedVariable(["x","y"]) #GPS speed in m/s of UAV
        self._vel_data = SharedVariable(["vel_a"]) #In m/s
        self._target = SharedVariable(["x","y","num_wp","mutex"]) #Target location in UTM
        self._tdir = SharedVariable(["x","y"]) #Target finish line direction
        self._poly_queue = Queue()
        self._matrix_queue = Queue()
        self._poly_queue_mon = Queue()
        self._wp_list_queue = Queue()
        self._radius_list_queue = Queue()
        self._uav_stable = Value(c_bool,False)
        #Start periodic reporting
        self._period_report = 0.1

        self.controllables = ["sense_person","drop_packet"]

    def sense_person(self):
        pass

    def drop_packet(self):
        pass
        
    def _started(self):
        #Start reading from output
        self._log_output = open(self._log_path + "mon_out.txt",'w+')
        self._count_out = 0
        self._add_to_command_queue("read_from_output")
        self._proc_report.start()

    def _exit(self):
        #Kill monitor process
        for mon in self.monitors:
            mon.kill()
            mon.wait()
        self._proc_report.terminate()
        self._proc_report.join()

    def _read_from_output(self):
        #Read from output of monitor -> BLOCKS until '\n' is reached
        output1 = self.mon1.stdout.readline()[:-1] #[:-1] removes '\n' at the end
        output2 = self.mon2.stdout.readline()[:-1] #[:-1] removes '\n' at the end
        output3 = self.mon3.stdout.readline()[:-1] #[:-1] removes '\n' at the end

        #Print output to console
        self._count_out += 1

        output1 = json.loads(output1)
        print("1",output1)
        output2 = json.loads(output2)
        print("2",output2)
        output3 = json.loads(output3)
        print("3",output3)

        outev = output1["senseanswer"]
        if outev == "NA":
            pass
        elif outev == "Found":
            self._add_to_event_queue('yes_person')
        elif outev == "NotFound":
            self._add_to_event_queue('no_person')

        self._uav_stable.value = output2["uav_stable"]

        if output3["cmd_resumeflight"]:
            self._add_to_event_queue('packet_dropped')
        if output3["cmd_droppacket"]:
            self._add_to_message_queue(mesg.PRINT,"Dropping packet")
        
        alt = output3["altitude_goto"]
        if np.abs(alt - self._pos.get_var("alt")) > 5:
            if np.abs(alt - self._sharedmem.get_flight_height()) > 5:
                loc_current = self._sharedmem.get_current_location()
                self._sharedmem.set_flight_height(alt)
                self._sharedmem.uav.go(loc_current,noevents=True)
                self._add_to_message_queue(mesg.PRINT,"Changed height to "+str(alt))
        

        #Continue reading from output
        if not self._exit_bool.value:
            self._add_to_command_queue("read_from_output")
        
    def _write_to_input(self,data,mon):
        mon.stdin.write(data + "\n")
        mon.stdin.flush()

    def set_matrix_grid(self,matrix):
        adj_matrix = lwave.get_adjacence_matrix(matrix)
        init = 0
        goal = len(matrix)-1
        path = []
        for i in range(len(matrix)-2):
            path = lwave.search_path(adj_matrix,init,goal)
            if len(path) > 0:
                break
            else:
                print("Try: "+str(i+1)+" to find path")
                goal -= 1
                
        print(path)
        inilist = []
        for i in range(len(path)):
            inilist.append([matrix[path[i],0],matrix[path[i],1]])
        
        self._matrix_queue.put(inilist)

    def set_poly(self,poly):
        poly_tmp = []
        for elem in poly:
            if elem[0] != 0 and elem[1] != 0:
                zone,posx,posy = llutm.LLtoUTM(elem[1],elem[0])                
                poly_tmp.append([posx,posy])
            else:
                self._poly_queue.put(np.array(poly_tmp))
                poly_tmp = []

    def set_target(self,target,prev_target,wp_list,radius_list):
        zone,posx,posy = llutm.LLtoUTM(target[1],target[0])
        self._target.set_var("mutex",0) #Loading data
        self._target.set_var("x",posx)
        self._target.set_var("y",posy)
        self._target.set_var("num_wp",0)

        zone,prev_posx,prev_posy = llutm.LLtoUTM(prev_target[1],prev_target[0])

        #Define tdir to be perpendicular to the line connecting prev_target with target
        self._tdir.set_var("x",- (posy - prev_posy))
        self._tdir.set_var("y",posx - prev_posx)

        #Push into queues the lists
        self._wp_list_queue.put(wp_list)
        self._radius_list_queue.put(radius_list)
        self._target.set_var("mutex",1) #Finished loading data

        self._going_flag.value = True

    def set_wp_num(self,seq):
        self._target.set_var("num_wp",seq)

    def mavlink_packet(self,m):
        if (not self._started_flag):
            return
        mtype = m.get_type()
        if (mtype == "WIND"):
            self._wind.set_var("direction",30*np.pi/180)#np.pi/2 - np.round(m.direction/30)*30*np.pi/180)#30*np.pi/180)
            self._wind.set_var("speed",m.speed)
            self._wind.set_var("speed_z",m.speed_z)
          # uncomment for wind-only test, event-triggered reports
          # ev = {
          #     "wind": self._wind,
          #     "timestamp": int(time.time())
          # }
          # self._write_to_input(json.dumps(ev))
          # return
        elif (mtype == "GLOBAL_POSITION_INT"):
            zone,posx,posy = llutm.LLtoUTM(m.lat*0.0000001,m.lon*0.0000001)
            self._pos.set_var("lat",m.lat*0.0000001)
            self._pos.set_var("lon",m.lon*0.0000001)
            self._pos.set_var("x",posx)
            self._pos.set_var("y",posy)
	    self._pos.set_var("alt",m.relative_alt*0.001) # mm2meter conversion
            self._pos.set_var("zone",zone)
            
            self._vel.set_var("x",m.vy*0.01) #El vx de GLOBAL_POSITION_INT apunta en sentido norte y el vy en sentido este
            self._vel.set_var("y",m.vx*0.01)
        elif (mtype == "ATTITUDE"):
            self._att.set_var("roll",m.roll)
            self._att.set_var("pitch",m.pitch)
            self._att.set_var("yaw",m.yaw)
        elif (mtype == "VFR_HUD"):
            self._vel_data.set_var("vel_a",m.airspeed)
        # uncomment for event-triggered reports
        # self._reportEvent()
        #

    def _periodicReports(self):
        i = 0
        dt = self._period_report #seconds
        init_time = time.time()
        self._count_in = 0
        self._log_input = open(self._log_path + "mon_in.txt",'w+')
        while not self._exit_bool.value:
            try:
                self._reportEvent()
            except Exception:
                import traceback
                traceback.print_exc()
                break
            i = i + 1
            time.sleep(max(0.01,init_time + i*dt - time.time()))
        self._log_input.close()

    def _retrieve_lists(self):
        while (self._wp_list_queue.qsize() > 0):
            self._wp_list = self._wp_list_queue.get()
            for i in range(len(self._wp_list)):
                zone,x,y = llutm.LLtoUTM(self._wp_list[i,1],self._wp_list[i,0])
                self._wp_list[i,0] = x
                self._wp_list[i,1] = y
        while (self._radius_list_queue.qsize() > 0):
            self._radius_list = self._radius_list_queue.get()
    
    def _reportEvent(self):
        if (self._pos.reported()):
            #while not (self._target.get_var("mutex") > 0):
                #time.sleep(0.01)
            
            events = [] #Eventos que ocurrieron desde el ultimo reporte
            while (self.event_queue.qsize() > 0):
                event = self.event_queue.get()
                if event == "goto_loc":
                    event = "go.next"
                if event == "sense_person":
                    event = "sense.person"
                if event == "drop_packet":
                    event = "drop.packet"
                events.append(event)

            loc_current = self._sharedmem.get_current_location()
            if loc_current == 2:
                person_detected = True
            else:
                person_detected = False

            ev1 = {
                "events_within": events,
                "person_detected": person_detected
            }

            ev2 = {
                "altitude": self._pos.get_var("alt"),
                "angle": self._att.get_var("pitch")*180.0/np.pi
            }
            
            ev3 = {
                "events_within": events,
                "altitude": np.round(self._pos.get_var("alt")),
                "uav_stable": self._uav_stable.value
            }
            
            self._count_in += 1
            self._log_input.write(str(self._count_in) + ',' + str(json.dumps(ev1)) + '\n')

            self._write_to_input(json.dumps(ev1),self.mon1)
            self._write_to_input(json.dumps(ev2),self.mon2)
            self._write_to_input(json.dumps(ev3),self.mon3)

            
            print "Written to HLola ev number: " + str(self.hlolacounter)
            self.hlolacounter += 1

            return
