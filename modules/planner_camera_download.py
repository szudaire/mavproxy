from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.mavproxy_map import mp_tile
import cv2 as cv
import time
from os.path import expanduser

class Camera(Module):
	def init(self):
		home = expanduser("~")
		path = home + '/.cache/mavproxy/cache/'
		self._tile = mp_tile.MPTile(cache_path=path,debug=False)
		self._height = 300
		self._width = 300
		self._field_of_view = 150.0/100.0 #150 meters at 100 meters of altitude
		
		self._process_type = 1
		return
	
	def _started(self):
                #This causes a memory leak, don't use namedwindow and replace it with waitKey(0)
		cv.namedWindow('capture', cv.CV_WINDOW_AUTOSIZE)
		cv.moveWindow('capture',300,0)
		cv.startWindowThread()
	
	def _capture(self):
		altitude = self._sharedmem.get_relative_altitude()
		position = self._sharedmem.get_position()
		image = self._download_image(position[1],position[0],altitude)
		return
	
	def capture(self):
		self._add_to_command_queue('capture')
		self._add_to_print_queue("Roll value: " + str(self._sharedmem.get_roll()))
		
	def _download_image(self,lat,lon,altitude):
		ground_width = self._field_of_view*altitude

                time.sleep(0.5)
                self._add_to_event_queue('camera_ready')
                return
		
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
		while (self._tile.tiles_pending() > 0):
			time.sleep(0.05)
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
		
		#self.image_list.append(image)
		#self.descarga_num += 1
		#cv.imwrite(self.path + 'data_set/' + str(self.descarga_num) + ".png",image)
		
		cv.imshow('capture',image)
		#self._add_to_event_queue('camera_ready')
		return image
