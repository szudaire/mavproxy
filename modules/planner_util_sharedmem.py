from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from ctypes import c_bool
from multiprocessing import Value
import numpy as np
from MAVProxy.modules import planner_util_discretizer
from MAVProxy.modules import planner_util_trajectory
from os.path import expanduser
from ctypes import *
import math
import time

CUDA = False
if (CUDA):
    import pycuda.driver as cuda
    from pycuda.compiler import SourceModule

class SharedMem():
    def __init__(self):
        #Position
        self._global_pos_lat = Value('d',0.0) #Y axis
        self._global_pos_lon = Value('d',0.0) #X axis
        self._pos_lat = Value('d',0.0) #Y axis
        self._pos_lon = Value('d',0.0) #X axis
        self._altitude = Value('d',0.0)
        self._relative_altitude = Value('d',0.0)
        #Direction
        self._dir_lat = Value('d',0.0)
        self._dir_lon = Value('d',0.0)
        #Roll
        self._roll = Value('d',0.0)
        self._pitch = Value('d',0.0)
        self._yaw = Value('d',0.0)
        #Battery
        self._battery_per = Value('d',0.0)
        self._battery_vol = Value('d',0.0)
        #Iterator
        self._next_loc = Value('i',-1)
        self._current_loc = Value('i',-1)
        #Iteration_status is set to false with "land" and "go_next"
        self._iteration_status = Value(c_bool,False) #True iterating, False not iterating

        #Grid configuration
        self._flight_height = Value('d',150.0) #meters
        self._dheight = Value('d',150.0) #meters
        self._grid_param = Value('d',50.0) #meters
        
        #Discretizer
        self._discretizer = planner_util_discretizer.Discretizer(self)
        
        #Plane trajectory
        self._control_mode = Value('i',0)
        # Parallel to wind: 0
        # Minimal: 1
        self._wind_dir_lat = Value('d',np.sin(np.pi/2))
        self._wind_dir_lon = Value('d',np.cos(np.pi/2))
        self._turn_radius = Value('d',0.0)
        
        #Arrived direction, added for better trajectory control
        self._arrived_dir_lat = Value('d',0.0)
        self._arrived_dir_lon = Value('d',0.0)
        self._arrived_dir_flag = Value(c_bool,False)

        #Auxiliary variables for meters to latlon
        self._dirx = np.array([1.0,0.0])
        self._diry = np.array([0.0,1.0])

        #Auxiliary variables for sharedmem
        self._tj = planner_util_trajectory.Trajectory()
        self.scale_x = Value('d',0.0)
        self.scale_y = Value('d',0.0)
        self.tree_b = Value('i',0)
        self.tree_depth = Value('i',0)

        #Logging
        self._do_logging = Value(c_bool,False)

        #Load clib
        path = expanduser("~/.cache/mavproxy/lib/")
        self._tj_c = cdll.LoadLibrary(path + "planner_clib_trajectory.so")
        self._tj_c.set_parameters.argtypes = (POINTER(c_double),POINTER(c_double),c_double,POINTER(c_double),c_bool,c_bool,c_bool)
        self._tj_c.set_overwp_dist.argtype = (c_double)
        self._tj_c.parallel_dir_trajectory.argtypes = (POINTER(c_double),POINTER(c_double),POINTER(c_double),POINTER(c_int),POINTER(c_double))
        self._tj_c.parallel_dir_trajectory.restype = (c_double)
        self._tj_c.multi_dir_trajectory.argtypes = (POINTER(c_double),POINTER(c_double),POINTER(c_int),POINTER(c_double))
        self._tj_c.multi_dir_trajectory.restype = (c_double)
        self._tj_c.pond_trajectory.argtypes = (POINTER(c_double),POINTER(c_double),c_double,POINTER(c_double),c_int,c_bool)
        self._tj_c.set_pond_params.argtype = (POINTER(c_double))

        self._rrt_c = cdll.LoadLibrary(path + "planner_clib_rrtsolver.so")
        self._rrt_c.add_obstacle.argtypes = (POINTER(c_double),POINTER(c_double),c_int)
        self._rrt_c.rrt_solver.argtypes = (POINTER(c_double),POINTER(c_double),POINTER(c_double),c_int,c_double,c_double,c_double)
        self._rrt_c.rrt_solver.restype = (c_int)
        self._rrt_c.get_solution.argtype = (POINTER(c_double))

        self._CUDA = CUDA


    def set_monitor(self,monitor):
        #Monitor, to be able to access it from the different processes
        self.monitor = monitor

    def set_uav(self,uav):
        self.uav = uav

    def set_logging(self,value):
        self._do_logging.value = value

    def get_logging(self):
        return self._do_logging.value
    
    def compile_cuda(self):
        import pycuda.autoinit
        path = expanduser("~/.cache/mavproxy/lib/")
        with open(path + "planner_gpu_trajectory.cu") as file:
            data = file.read()
        mod = SourceModule(data)
        self._gpu_pond_trajectory = mod.get_function("pond_trajectory")
    

    def set_battery(self,per,vol):
        self._battery_per.value = per
        self._battery_vol.value = vol

    def get_battery(self):
        return self._battery_per.value,self._battery_vol.value
    
    def set_dheight(self,value):
        self._dheight.value = value

    def get_dheight(self):
        return self._dheight.value
    
    def set_flight_height(self,value):
        self._flight_height.value = value

    def get_flight_height(self):
        return self._flight_height.value
    
    def set_grid_param(self,value):
        self._grid_param.value = value

    def get_grid_param(self):
        return self._grid_param.value
    
    def set_scale(self):
        retx,rety = self.meters2latlon(50.0)
        #print rety/retx
        self.scale_x.value = rety/retx
        self.scale_y.value = 1.0

    def get_scale(self):
        return np.array([self.scale_x.value,self.scale_y.value])
    
    def set_arrived_dir_flag(self,value):
        self._arrived_dir_flag.value = value
    
    def get_arrived_dir_flag(self):
        return self._arrived_dir_flag.value
    
    def get_turn_radius(self):
        return self._turn_radius.value
    
    def set_turn_radius(self,radius):
        radius = self.meters2latlon(radius)[1]
        self._turn_radius.value = radius
        
    def get_wind_dir(self):
        return np.array([self._wind_dir_lon.value,self._wind_dir_lat.value])
    
    def set_wind_dir(self,wind_dir):
        self._wind_dir_lon.value = wind_dir[0]
        self._wind_dir_lat.value = wind_dir[1]
    
    def set_arrived_dir(self,dir):
        self._arrived_dir_lon.value = dir[0]
        self._arrived_dir_lat.value = dir[1]
        
    def get_arrived_dir(self):
        return np.array([self._arrived_dir_lon.value,self._arrived_dir_lat.value])
        
    def get_control_mode(self):
        return self._control_mode.value
    
    def set_control_mode(self,mode):
        self._control_mode.value = mode

    def get_attitude(self):
        return self._roll.value,self._pitch.value,self._yaw.value

    def set_attitude(self,roll,pitch,yaw):
        self._roll.value = roll
        self._pitch.value = pitch
        self._yaw.value = yaw
    
    def get_discretizer(self):
        return self._discretizer
    
    def get_altitude(self):
        return self._altitude.value
        
    def set_altitude(self,altitude):
        self._altitude.value = altitude
    
    def get_relative_altitude(self):
        return self._relative_altitude.value
        
    def set_relative_altitude(self,altitude):
        self._relative_altitude.value = altitude
        
    def get_position(self):
        return np.array([self._pos_lon.value,self._pos_lat.value])
    
    def set_position(self,position):
        self._pos_lat.value = position[1]
        self._pos_lon.value = position[0]
    
    def get_direction(self):
        return np.array([self._dir_lon.value,self._dir_lat.value])
    
    def set_direction(self,direction):
        self._dir_lat.value = direction[1]
        self._dir_lon.value = direction[0]
        
    def get_next_location(self):
        return self._next_loc.value
    
    def set_next_location(self,next_loc):
        self._next_loc.value = next_loc
        
    def get_current_location(self):
        return self._current_loc.value
    
    def set_current_location(self,current_loc):
        self._current_loc.value = current_loc
    
    def set_iteration_status(self,status):
        self._iteration_status.value = status
    
    def get_iteration_status(self):
        return self._iteration_status.value

    def meters2latlon(self,distance):
        position = self.get_position()
        (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
        wp_utm_x = np.array([UTMEasting,UTMNorthing])
        wp_utm_y = np.array([UTMEasting,UTMNorthing])
        wp_utm_x += self._dirx*distance
        wp_utm_y += self._diry*distance
        (Lat_x,Long_x) = llutm.UTMtoLL(wp_utm_x[1],wp_utm_x[0],ZoneNumber,isSouthernHemisphere=True)
        (Lat_y,Long_y) = llutm.UTMtoLL(wp_utm_y[1],wp_utm_y[0],ZoneNumber,isSouthernHemisphere=True)
        wp_ll_x = np.array([Long_x,Lat_x])
        wp_ll_y = np.array([Long_y,Lat_y])

        vecx = wp_ll_x - position
        vecy = wp_ll_y - position
        dist_ll_x= np.linalg.norm(vecx,axis=0)
        dist_ll_y= np.linalg.norm(vecy,axis=0)

        #alpha = (vecy[1]/vecx[0] - vecx[1]/vecy[0])/2
        return np.array([dist_ll_x,dist_ll_y])
    
    def set_trajectory_parameters(self,loc_current_pos,grid=None,auxwp=False,overwp=False,check_ang=False,pond=False):
        if (self.get_arrived_dir_flag() == False):
            self.set_arrived_dir_flag(True)
            arrived_dir = self.get_direction()
            self.set_arrived_dir(arrived_dir)
        else:
            arrived_dir = self.get_arrived_dir()

        self._auxwp = auxwp
        self._pond = pond
        turn_radius = self.get_turn_radius()
        scale = self.get_scale()
        #Por ahora dejamos el set_parameters de python porque setea el pos_inicial necesario para set_grid
        self._tj.set_parameters(loc_current_pos,arrived_dir,turn_radius,scale,auxwp,overwp,check_ang)
        self._tj_c.set_parameters(loc_current_pos.ctypes.data_as(POINTER(c_double)),arrived_dir.ctypes.data_as(POINTER(c_double)),c_double(turn_radius),scale.ctypes.data_as(POINTER(c_double)),c_bool(auxwp),c_bool(overwp),c_bool(check_ang))

        #Overwaypoint distance
        overwp_dist = 0.0
        if (overwp):
            overwp_dist = self.meters2latlon(10)[1]
            self._tj.set_overwp_dist(overwp_dist)
            self._tj_c.set_overwp_dist(c_double(overwp_dist))

        #Grid to calculate ponderation
        self._pond = pond
        if (pond):
            #Para calcular suma_actual es necesario el valor de pos_inicial
            self._tj.set_grid(grid)
            suma_pond = self._tj.suma_actual
            self._tj_c.set_pond_params(suma_pond.ctypes.data_as(POINTER(c_double)))

        if (CUDA):
            self._gpu_pos_inicial = loc_current_pos
            self._gpu_dir_inicial = arrived_dir
            self._gpu_scale = scale
            self._gpu_c1_h = self._tj.centros_C1_min[0]
            self._gpu_c1_ah = self._tj.centros_C1_min[1]
            self._gpu_turn_radius = turn_radius
            self._gpu_overwp = overwp
            self._gpu_auxwp = auxwp
            self._gpu_check_ang = check_ang
            self._gpu_overwp_dist = overwp_dist
            if (pond):
                self._gpu_suma_pond = suma_pond
            

    def batch_calculate_trajectory(self,pos_array):
        dir_final = self.get_wind_dir()
        pos_array_flat = pos_array.flatten()
        mode = self.get_control_mode()

        if (CUDA):
            dist_array_gpu = np.zeros(len(pos_array))
            radio = self._gpu_turn_radius
            overwp = self._gpu_overwp
            auxwp = self._gpu_auxwp
            check_ang = self._gpu_check_ang
            overwp_dist = self._gpu_overwp_dist
            #Inicializacion de arrays de device
            d_pos_final_array = cuda.mem_alloc(pos_array_flat.nbytes)
            d_dir_final = cuda.mem_alloc(dir_final.nbytes)
            d_dist_array = cuda.mem_alloc(dist_array_gpu.nbytes)
            d_pos_inicial = cuda.mem_alloc(self._gpu_pos_inicial.nbytes)
            d_dir_inicial = cuda.mem_alloc(self._gpu_dir_inicial.nbytes)
            d_scale = cuda.mem_alloc(self._gpu_scale.nbytes)
            d_c1_h = cuda.mem_alloc(self._gpu_c1_h.nbytes)
            d_c1_ah = cuda.mem_alloc(self._gpu_c1_ah.nbytes)
            d_suma_pond = cuda.mem_alloc(self._gpu_suma_pond.nbytes)
            #Copy to device
            cuda.memcpy_htod(d_pos_final_array, pos_array_flat)
            cuda.memcpy_htod(d_dir_final, dir_final)
            cuda.memcpy_htod(d_pos_inicial,self._gpu_pos_inicial)
            cuda.memcpy_htod(d_dir_inicial,self._gpu_dir_inicial)
            cuda.memcpy_htod(d_scale,self._gpu_scale)
            cuda.memcpy_htod(d_c1_h,self._gpu_c1_h)
            cuda.memcpy_htod(d_c1_ah,self._gpu_c1_ah)
            cuda.memcpy_htod(d_suma_pond,self._gpu_suma_pond)
            #Kernel call
            N = 128
            M = 1 + (len(pos_array)//N)
            
            self._gpu_pond_trajectory(d_pos_final_array,d_dir_final,np.array(len(pos_array)), d_dist_array, d_pos_inicial, d_dir_inicial, np.array(radio), d_scale, np.array(auxwp), np.array(overwp), np.array(check_ang), d_c1_h, d_c1_ah, np.array(overwp_dist),d_suma_pond,np.array(mode), block=(N,1,1), grid=(M,1,1))
            #Copy to host
            cuda.memcpy_dtoh(dist_array_gpu,d_dist_array)
            dist_array = dist_array_gpu
        else:
            pos_final_array = pos_array_flat.ctypes.data_as(POINTER(c_double))
            dist_array_c = np.zeros(len(pos_array),np.float_)
            self._tj_c.pond_trajectory(pos_final_array,dir_final.ctypes.data_as(POINTER(c_double)),c_double(len(pos_array)),dist_array_c.ctypes.data_as(POINTER(c_double)),c_int(mode),c_bool(self._pond))
            dist_array = dist_array_c

        return dist_array

    def calculate_dir_trajectory(self,loc,angle):
        dir_final = np.array([np.cos(angle),np.sin(angle)])
        wp_list,distance = self._tj.calcular_trayectoria_direccionada_ang_check(loc,dir_final)

        self.set_arrived_dir_flag(False)
        return wp_list,distance,dir_final
        
    def calculate_trajectory(self,loc_next_pos,update_dir=False):
        if(self.get_control_mode() == 0):
            wind_dir = self.get_wind_dir()
            #if (self._pond):
                #wp_list,distance,dir = self._tj.calcular_trayectoria_paralela_direccion_ponderada(loc_next_pos,wind_dir)
            #print "START TRAJECTORY"
            dir_final = wind_dir
            delta_angle_c = c_double.in_dll(self._tj_c, "DELTA_ANG")
            cant = int(math.floor((2.1*math.pi)/(delta_angle_c.value))) + 6
            cant_c = c_int(cant)
            wp_list_c = np.zeros(cant*2)
            #print "Lent LIST ", len(wp_list_c)
            wp_list_c_arg = wp_list_c.ctypes.data_as(POINTER(c_double))
            cant_array = np.array([0],np.int32)
            cant_array_c = cant_array.ctypes.data_as(POINTER(c_int))
            dir_min = np.zeros(1)
            dir_min_c = dir_min.ctypes.data_as(POINTER(c_double))
            distance = self._tj_c.parallel_dir_trajectory(loc_next_pos.ctypes.data_as(POINTER(c_double)),dir_final.ctypes.data_as(POINTER(c_double)),wp_list_c_arg,cant_array_c,dir_min_c)

            #Change name to return values
            wp_list = wp_list_c.reshape(cant,2)[0:cant_array[0],:]
            dir = dir_final * dir_min

            #wp_list,distance,dir = self._tj.calcular_trayectoria_paralela_direccion(loc_next_pos,wind_dir)

        else:
            delta_angle_c = c_double.in_dll(self._tj_c, "DELTA_ANG")
            cant = int(math.floor((2.1*math.pi)/(delta_angle_c.value))) + 4
            cant_c = c_int(cant)
            wp_list_c = np.zeros(cant*2)

            wp_list_c_arg = wp_list_c.ctypes.data_as(POINTER(c_double))
            cant_array = np.array([0],np.int32)
            cant_array_c = cant_array.ctypes.data_as(POINTER(c_int))
            dir_min = np.zeros(2)
            dir_min_c = dir_min.ctypes.data_as(POINTER(c_double))
            distance = self._tj_c.multi_dir_trajectory(loc_next_pos.ctypes.data_as(POINTER(c_double)),wp_list_c_arg,cant_array_c,dir_min_c)

            #Change name to return values
            wp_list = wp_list_c.reshape(cant,2)[0:cant_array[0],:]
            dir = dir_min

            #wp_list,distance,dir = self._tj.calcular_trayectoria_minima(loc_next_pos)
        
        if(update_dir):
            self.set_arrived_dir(dir)
        
        if(self._auxwp == False):
            return distance
        
        return wp_list,distance,dir

    def add_obstacles(self,polygons):
        scale = self.get_scale()
        self.limits = np.zeros(4,np.float)
        self._polygon = polygons
        if (len(polygons) > 0):
            set_lim = False
            self._has_obstacles = True
        else:
            self._has_obstacles = False
        for poly in polygons:
            poly = np.array(poly,np.float)
            vecx = poly[:,0]*scale[0]
            vecy = poly[:,1]*scale[1]
            if (not set_lim):
                set_lim = True
                self.limits[0] = min(vecx)
                self.limits[1] = max(vecx)
                self.limits[2] = min(vecy)
                self.limits[3] = max(vecy)
            else:
                self.limits = expand_limits(self.limits,vecx,vecy)
        
            vecx_c = vecx.ctypes.data_as(POINTER(c_double))
            vecy_c = vecy.ctypes.data_as(POINTER(c_double))
            self._rrt_c.add_obstacle(vecx_c,vecy_c,c_int(len(poly)))
            
        self.max_iter = 10000
        grid_param = self.get_grid_param()
        self.tol = self.meters2latlon(grid_param/100)[1]
        self.step = self.meters2latlon(grid_param/10)[1]
        self.radius = self.meters2latlon(grid_param/2)[1]

    def rrt_solver(self,loc_current_pos,loc_next_pos):
        t1 = time.time()
        scale = self.get_scale()
        x_init = loc_current_pos*scale
        x_goal = loc_next_pos*scale
        
        limits = self.limits.copy()
        limits = expand_limits(limits,[x_init[0],x_goal[0]],[x_init[1],x_goal[1]])

        extra_coef = 0.15
        extra_x = extra_coef*(limits[1] - limits[0])
        extra_y = extra_coef*(limits[3] - limits[2])
        limits[0] = limits[0] - extra_x
        limits[1] = limits[1] + extra_x
        limits[2] = limits[2] - extra_y
        limits[3] = limits[3] + extra_y

        limits_c = limits.ctypes.data_as(POINTER(c_double))
        x_init_c = x_init.ctypes.data_as(POINTER(c_double))
        x_goal_c = x_goal.ctypes.data_as(POINTER(c_double))
 
        #print("Data:")
        #print(x_init)
        #print(x_goal)
        #print(limits)
        #print(self._polygon)
        #print(self.radius)
        #print(self.tol)
        #print(self.step)
        sol_size = self._rrt_c.rrt_solver(x_init_c,x_goal_c,limits_c,c_int(self.max_iter),c_double(self.radius),c_double(self.step),c_double(self.tol))
        print "Solution size: " + str(sol_size)
        solution = np.zeros((sol_size*2),np.float)
        solution_c = solution.ctypes.data_as(POINTER(c_double))
        self._rrt_c.get_solution(solution_c)
        solution = solution.reshape(sol_size,2)
        solution = solution/scale
        print "Solution calculation time: " + str(time.time() - t1)
        
        return solution
            
        
        
def expand_limits(limits,vecx,vecy):
    if (min(vecx) < limits[0]):
        limits[0] = min(vecx)
    if (max(vecx) > limits[1]):
        limits[1] = max(vecx)
    if (min(vecy) < limits[2]):
        limits[2] = min(vecy)
    if (max(vecy) > limits[3]):
        limits[3] = max(vecy)

    return limits
